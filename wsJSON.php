<?php
error_reporting(E_ERROR | E_PARSE);
ini_set('display_errors',1);
class wsJSON {
    private $connection;
	private $json_array = array("status" => array("responsecode"=> "1", "entity"=> "1"), "data" => array());
    function __construct($con) {
        $this->connection = $con;
    }
    function executeQuery($query) {
        $result = $this->connection->query($query) or die(mysql_error());
        return $result;
    }
    function addslashes_to_string($str) {
        $tempStr = addslashes(trim($str));
        return str_replace("\'", "", $tempStr);
        // return str_replace("\r\n","",str_replace("\'","'",$tempStr));
        //return str_replace("\r\n","",str_replace("\'","'",$tempStr));
    }	
    function GetLatitudeLongitude($geoaddress) {
        $geoaddress=urlencode($geoaddress);
        $geoaddress=str_replace("#", "", $geoaddress);
        $geoaddress=str_replace(" ", "+", $geoaddress);
        $geoaddress=str_replace("++", "+", $geoaddress);
            //echo "http://maps.googleapis.com/maps/api/geocode/json?address=".$geoaddress."&sensor=true";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://maps.googleapis.com/maps/api/geocode/json?address=".$geoaddress."&sensor=true");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $geocode = curl_exec($ch);
        $output2= json_decode($geocode);
        curl_close($ch);
        $latitude = $output2->results[0]->geometry->location->lat;
        $longitude = $output2->results[0]->geometry->location->lng;
        $coordinate=array($latitude,$longitude);
        return $coordinate;
    } 
	function fnSendInvitation($senderid,$receiverid){
		//check invitation already exists:	 
		$sqluser = "SELECT * FROM users WHERE uid='".$receiverid."'"; 
		$proRowuser = $this->executeQuery($sqluser);
		$rowuser = mysql_fetch_array($proRowuser);
		//contactssource
		$sql = "SELECT * from contactssource WHERE phone='".$rowuser['phone']."' AND feloze_uid='".$senderid."'";
		$result = $this->executeQuery($sql);
		$rowcount = mysql_num_rows($result);
		if ($rowcount > 0) { 
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['invitation']['msg'] = "Contact already exist in contacts.";
		}
		else 
		{
			$sql = "SELECT *  FROM tbl_invitation WHERE senderid='$senderid' AND receiverid='$receiverid' AND status!='4'";
			$proRow = $this->executeQuery($sql);
			if (mysql_num_rows($proRow) != 0) {
				$this->json_array['status']['responsecode'] = '1';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['invitation']['msg'] = "Invitation already sent.";
			}
			else
			{
				$sql = "INSERT INTO tbl_invitation(senderid,receiverid)VALUES('$senderid', '$receiverid') ";
				$proRow = $this->executeQuery($sql);
				$user_id = mysql_insert_id();
				if ($user_id) {
					$this->json_array['status']['responsecode'] = '0';
					$this->json_array['status']['entity'] = '1';
					$this->json_array['data']['invitation']['msg'] = "Invitation sent successfully.";
				}
				else
				{
					$this->json_array['status']['responsecode'] = '0';
					$this->json_array['status']['entity'] = '1';
					$this->json_array['data']['invitation']['msg'] = "Unsuccessful.";
				}
			}
		}
		return json_encode($this->json_array);
	}
	function fnplaceorder($total_order_cost,$total_order_gst_cost,$sale_count,$free_count,$total_count){
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['invitation']['msg'] = "Order Placed Successfully.";
		$this->json_array['data']['total_cost'] = "$total_order_cost";
		$this->json_array['data']['total_gst_cost'] = "$total_order_gst_cost";
		$this->json_array['data']['total_quantity'] = "$sale_count";
		$this->json_array['data']['sale_quantity'] = "$free_count";
		$this->json_array['data']['free_quantity'] = "$total_count";
		return json_encode($this->json_array);
	}
	function get_userdata($userid) {
		 $sql = " SELECT 
		`id`, 
		`external_id`, 
		`surname`, 
		`firstname`, 
		`username`, 
		`pwd`, 
		`user_type`, 
		`address`, 
		`city`, 
		`state`, 
		`mobile`, 
		`email`, 
		`reset_key`, 
		`suburbid`,
		`state_ids`, `city_ids`, `suburb_ids`, `subarea_ids`, `sstockist_id`
		FROM  `tbl_user` 
		LEFT JOIN tbl_user_working_area ON tbl_user.id = tbl_user_working_area.user_id
		WHERE id='$userid' AND isdeleted != 1 AND user_status = 'Active'";	
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '6';				
			
			$this->json_array['data']['Login']['id'] = $row['id'];
			$this->json_array['data']['Login']['external_id'] = $row['external_id'];
			
			$this->json_array['data']['Login']['username'] = $row['username'];
			$this->json_array['data']['Login']['firstname'] = $row['firstname'];
			$this->json_array['data']['Login']['surname'] = $row['surname'];
			$this->json_array['data']['Login']['address'] = $row['address'];
			$this->json_array['data']['Login']['email'] = $row['email'];
			$this->json_array['data']['Login']['mobile'] = $row['mobile'];
			/********* get city, state, suburbid************/ 		
			$state=$this->get_stateid($row['state']);
			$this->json_array['data']['Login']['stateid'] = $state['id'];
			$this->json_array['data']['Login']['statenm'] = $state['name'];
			$city=$this->get_cityid($row['city']);
			$this->json_array['data']['Login']['cityid'] = $city['id'];
			$this->json_array['data']['Login']['citynm'] = $city['name'];
			$suburb_ids = "";
			$suburbnms = "";
			if($row['suburb_ids']!="") {
				$sql = "SELECT id , suburbnm FROM tbl_surb WHERE id in(".$row['suburb_ids'].") ";
				$proRow = $this->executeQuery($sql);
				while($row1 = mysql_fetch_array($proRow))
				{
					if($suburb_ids=="")
						$suburb_ids = $row1["id"];
					else 
						$suburb_ids .= ",". $row1["id"];
					if($suburbnms=="")
						$suburbnms = $row1["suburbnm"];
					else 
						$suburbnms .= ",". $row1["suburbnm"];
				}
			}
			$this->json_array['data']['Login']['suburbids'] = $suburb_ids;
			$this->json_array['data']['Login']['suburbnms'] = $suburbnms;
			$subareaids = "";
			$subareaname = "";
		}
		else {
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['Login']['message'] = "Invalid userid.";
		}
		return json_encode($this->json_array);
	}
	function fngetOwners($id,$role){
		$sql = "SELECT *  FROM `tbl_user` WHERE id='$id' AND user_type='".$role."'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			return $row;
		}
		else{
			return 0;
		}
	}
	function get_login($uname, $password) {
		$password = md5($password);
		$sql = "SELECT *  FROM `tbl_user` WHERE username='$uname' AND pwd='$password'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			 $this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '6';
				$this->json_array['data']['Login']['id'] = $row['id'];
				$this->json_array['data']['Login']['username'] = $row['username'];
				$this->json_array['data']['Login']['firstname'] = $row['firstname'];
				$this->json_array['data']['Login']['surname'] = $row['surname'];
				$this->json_array['data']['Login']['address'] = $row['address'];
				$this->json_array['data']['Login']['email'] = $row['email'];
		}
		else {
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['Login']['message'] = "Invalid username or password.";
		}
		return json_encode($this->json_array);
	}
	function get_login_m($uname, $password) {
		$password = md5($password);
		$sql = "SELECT *  FROM `tbl_users` WHERE username='$uname' AND passwd='$password' AND level='SalesPerson'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$sqlcom = "SELECT *  FROM tbl_user_company WHERE userid='".$row['id']."'";
			$proRowcom = $this->executeQuery($sqlcom);
			$rowcom = mysql_fetch_array($proRowcom);
			 $this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '6';
				$this->json_array['data']['Login']['id'] = $row['id'];
				$this->json_array['data']['Login']['username'] = $row['username'];
				$this->json_array['data']['Login']['firstname'] = $row['firstname'];
				$this->json_array['data']['Login']['surname'] = $row['surname'];
				$this->json_array['data']['Login']['address'] = $row['address'];
				$this->json_array['data']['Login']['email'] = $row['email'];
		}
		else {
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['Login']['message'] = "Invalid username or password.";
		}
		return json_encode($this->json_array);
	}
	
function fnGetProductCat($userid){
		$sql = "SELECT * FROM pcatbrandvariant1";
		$proRow = $this->executeQuery($sql);
		
		 $sql1="SELECT `id`, `userid`, `sptqdate`, `productid`, `prod_var_id`, `quantity`,`delivered_quantity` FROM `tbl_sp_todays_quantity` 
				WHERE userid='$userid' AND DATE_FORMAT(sptqdate,'%Y-%m-%d')=DATE(NOW())";				
		$proRow1 = $this->executeQuery($sql1);
		
		while($row1= mysql_fetch_array($proRow1))
		{
			$temp=$row1['productid']."-".$row1['prod_var_id'];
			$p_array_qnty[$temp]['quantity']=$row1['quantity'];
			$p_array_qnty[$temp]['delivered_quantity']=$row1['delivered_quantity'];
			$p_array_qnty[$temp]['spstock_quantity']=$row1['quantity']-$row1['delivered_quantity'];
		}
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		while($row = mysql_fetch_array($proRow))
		{
			$p_array_temp['brand_name'] = $row['brand_name'];
			$p_array_temp['category_name'] = $row['category_name'];
			
			if($row['category_image']!=''){
				$p_array_temp['category_image'] = SITEURL."templates/admin/upload/".$row['category_image'];
			} else {
				$p_array_temp['category_image'] = "";
			}
			
			$p_array_temp['product_name'] = $row['product_name'];
			$p_array_temp['product_price'] = $row['product_price'];
			$p_array_temp['product_price1'] = $row['product_price1'];
			$p_array_temp['product_price2'] = $row['product_price2'];
			$p_array_temp['product_price3'] = $row['product_price3'];
			$p_array_temp['product_price4'] = $row['product_price4'];
			$p_array_temp['product_price5'] = $row['product_price5'];
			$p_array_temp['product_hsn'] = $row['product_hsn'];
			$p_array_temp['product_cgst'] = $row['product_cgst'];
			$p_array_temp['product_sgst'] = $row['product_sgst'];
			if($row['product_barcode_img']!=''){
				$p_array_temp['product_barcode_img'] = SITEURL."templates/admin/upload/".$row['product_barcode_img'];
			} else {
				$p_array_temp['product_barcode_img'] = "";
			}
			$p_array_temp['brand_id'] = $row['brand_id'];
			$p_array_temp['category_id'] = $row['category_id'];
			$p_array_temp['product_id'] = $row['product_id'];//variant row
			$p_array_temp['product_variant_rowcnt'] = $row['product_variant_id'];
			
			$temp2=$row['product_id'].'-'.$row['product_variant_id'];
			$temp3="'".$row['product_id'].'-'.$row['product_variant_id']."'";
				
			if(array_key_exists($temp2,$p_array_qnty)){
				$p_array_temp['quantity'] = $p_array_qnty[$temp2]['quantity'];
				$p_array_temp['delivered_quantity'] = $p_array_qnty[$temp2]['delivered_quantity'];
				$p_array_temp['spstock_quantity']=$p_array_qnty[$temp2]['spstock_quantity'];
			}else{
				$p_array_temp['quantity'] = '0';
				$p_array_temp['delivered_quantity'] = '0';
				$p_array_temp['spstock_quantity']  = '0';
			}
			
			$variant1arr=explode(",",$row['product_variant1']);
			
			$weightunit=$this->fnGetUnit($variant1arr[1]);
			$p_array_temp['weight'] = $variant1arr[0];
			$p_array_temp['weightunit'] = $weightunit;

			$variant2arr=explode(",",$row['product_variant2']);
			$sizeunit=$this->fnGetUnit($variant2arr[1]);
			$p_array_temp['size'] = $variant2arr[0];
			$p_array_temp['sizeunit'] = $sizeunit;

			$this->json_array['data']['procats'][]=$p_array_temp;
		}
		$this->json_array['data']['campaigns'] = $this->get_campaigndata();
		return json_encode($this->json_array);
	}
	//new for chetrans
	function fnGetProductCatlist(){
		$sql = "SELECT * FROM pcatbrandvariant1";
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		while($row = mysql_fetch_array($proRow))
		{
			$p_array_temp['product_name'] = $row['product_name'];
			$p_array_temp['product_id'] = $row['product_id'];//variant row
			$p_array_temp['product_variant_rowcnt'] = $row['product_variant_id'];
			$variant1arr=explode(",",$row['product_variant1']);
			
			$weightunit=$this->fnGetUnit($variant1arr[1]);
			$p_array_temp['weight'] = $variant1arr[0];
			$p_array_temp['weightunit'] = $weightunit;

			$variant2arr=explode(",",$row['product_variant2']);
			$sizeunit=$this->fnGetUnit($variant2arr[1]);
			$p_array_temp['size'] = $variant2arr[0];
			$p_array_temp['sizeunit'] = $sizeunit;
			$this->json_array['data']['productlist'][]=$p_array_temp;
			
		}
		return json_encode($this->json_array);
	}
	//new for chetrans
	function fnUpdateTodaysSPQuantity($data){
		//echo "<pre>";print_r($data);//die();
		$userid=$data->userid;
		 $sql="SELECT `id`, `userid`, `sptqdate`, `productid`, `prod_var_id`, `quantity` FROM `tbl_sp_todays_quantity` 
				WHERE userid='$userid' AND DATE_FORMAT(sptqdate,'%Y-%m-%d')=DATE(NOW())";				
		$proRow = $this->executeQuery($sql);
		//echo mysql_num_rows($proRow);
		$counter=0;
		if (mysql_num_rows($proRow) == 0) {
			if(count($data->productdetails)){
				foreach($data->productdetails as $key=>$value){
					//echo "asda ";print_r($value->productid);
					$sql="INSERT INTO tbl_sp_todays_quantity(userid,sptqdate,productid,prod_var_id,quantity)VALUES('".$userid."',now(),'".$value->productid."','".$value->prod_var_id."','".$value->quantity."')";
					$proRow = $this->executeQuery($sql);
					$counter++;	
				}
			}
			//die();
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Todays delivery quantity updated successfully.";
		}else{
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Todays delivery quantity already exists.";
		}
		return json_encode($this->json_array);
	}
	
	function fnGetUnit($id){
		$sql="SELECT unitname FROM tbl_units WHERE id='".$id."'";
		$proRow = $this->executeQuery($sql);
		$row = mysql_fetch_array($proRow);
		return $row['unitname'];
	}
	function fnProfileUpdate($profileid,$name,$address,$state,$city,$suburbids,$subareaids,$mobile,$phone_no,$external_id=null){
		//echo "sdfjh";
		  $sql="UPDATE tbl_user SET
		firstname='$name',address='$address',mobile='$mobile'
		where id='$profileid'";		
		$proRow = $this->executeQuery($sql);
		
		 $sql1="SELECT user_id FROM tbl_user_working_area WHERE user_id='".$profileid."'";	 
		$result1 = $this->executeQuery($sql1);
		$rowcount = mysql_num_rows($result1);
		if($rowcount  >0){
			  $sql2 = "UPDATE tbl_user_working_area SET 
				state_ids = '$state' , 
				city_ids = '$city' , 
				suburb_ids='$suburbids' 
			WHERE user_id = '$profileid'";
		} else {
			  $sql2="INSERT INTO `tbl_user_working_area`( `user_id`, `state_ids`, `city_ids`, `suburb_ids`) 
			VALUES  ( '$profileid', '$state', '$city', '$suburbids' )";
		}
		// echo $sql;
		$proRow2 = $this->executeQuery($sql2);
		
		$sql_user_details="SELECT userid FROM tbl_user_details WHERE userid='".$profileid."'";	 
		$result_user_details = $this->executeQuery($sql_user_details);
		$rowcount_user_details = mysql_num_rows($result_user_details);
		if($rowcount_user_details  >0){
			$update_user_details = "UPDATE tbl_user_details SET 
				phone_no = '$phone_no' 
			WHERE userid = '$profileid'";
		} else {
			 $update_user_details="INSERT INTO `tbl_user_details`( `userid`, `phone_no`) 
			VALUES  ( '$profileid', '$phone_no')";
		}
		// echo $sql;
		$proRow_user_details = $this->executeQuery($update_user_details);
		
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '7';
		$this->json_array['data']['Profile']['id'] = $profileid;		
		$this->json_array['data']['Profile']['firstname'] = $name;
		$this->json_array['data']['Profile']['address'] = $address;
		$this->json_array['data']['Profile']['state'] = $state;
		$this->json_array['data']['Profile']['city'] = $city;
		$this->json_array['data']['Profile']['suburbids'] = $suburbids;	
		$this->json_array['data']['Profile']['mobile'] = $mobile;
		$this->json_array['data']['Profile']['phone_no'] = $phone_no;
		
		return json_encode($this->json_array);
	}
	function fnProfileUpdatemain($profileid,$name,$address,$state,$city,$mobile){
		//common db update
		$sqlcdb="UPDATE tbl_users SET firstname='$name',address='$address',state='$state',city='$city' , mobile='$mobile' where id='$profileid'";
		$proRow = $this->executeQuery($sqlcdb);
	}
	function fnApplyLeave($id,$reasonleave,$description,$leavedt){
		//tbl_salesperson_leave
		$sql="INSERT INTO tbl_salesperson_leave(salespersonid,reason,description,leavedt)VALUES('".$id."','".$reasonleave."','".$description."','".$leavedt."')";
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['leave']['msg'] = "Leave application sent successfully.";
		return json_encode($this->json_array);
	}
	function get_state(){
	 $sql = "SELECT id, name FROM tbl_state WHERE country_id='101'";
	  $proRow = $this->executeQuery($sql);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '9';
	  while($row = mysql_fetch_array($proRow)){
		  $s_array_temp['name'] = $row['name'];
		  $s_array_temp['id'] = $row['id'];
		  $this->json_array['data']['state'][]=$s_array_temp;
	  }
	  return json_encode($this->json_array);  
	}
	function get_city($stateid){
		$sqlcity = "SELECT id, name,state_id FROM  tbl_city WHERE state_id='".$stateid."'";
	  $proRowcity = $this->executeQuery($sqlcity);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '3';
	   while($rowcity = mysql_fetch_array($proRowcity)){
		  $c_array_temp['name'] = $rowcity['name'];
		  $c_array_temp['id'] = $rowcity['id'];
		  $c_array_temp['state_id'] = $rowcity['state_id'];
		  $this->json_array['data']['city'][]=$c_array_temp;
	  }
	  return json_encode($this->json_array);
	}
	function get_suburb($cityid){
	$sqlcity = "SELECT id,suburbnm FROM tbl_surb WHERE cityid='".$cityid."' AND isdeleted!='1'";
	  $proRowcity = $this->executeQuery($sqlcity);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '2';
	   while($rowcity = mysql_fetch_array($proRowcity)){
		  $c_array_temp['suburbnm'] = $rowcity['suburbnm'];
		  $c_array_temp['id'] = $rowcity['id'];
		  $this->json_array['data']['suburb'][]=$c_array_temp;
	  }
	  return json_encode($this->json_array);
	}
	/********************* City, state and suburb by id ***************************/
	function get_stateid($id){
	  $sql = "SELECT id, name FROM tbl_state WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '2';
	  $row = mysql_fetch_array($proRow);
	  return $row;  
	}
	function get_cityid($cityid){
	$sqlcity = "SELECT id, name FROM  tbl_city WHERE id='".$cityid."'";
	$proRowcity = $this->executeQuery($sqlcity);
	$rowcity = mysql_fetch_array($proRowcity);
	 return $rowcity;
	}
	function get_suburbid($suburbid){
	$sqlcity = "SELECT id,suburbnm FROM tbl_surb WHERE id='".$suburbid."'";
	$proRowcity = $this->executeQuery($sqlcity);
	$rowsuburb = mysql_fetch_array($proRowcity);
	return $rowsuburb;
	}
	/*******************************************************************************/
	//$salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$distributorid,$suburbid,$subarea_id
	function fnAddNewStore($salespersonid,$store_name,$owner_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$suburbid,$shop_type_id, $bank_acc_name,$bank_acc_no,$bank_b_name,$bank_ifsc) {
		 $sql="INSERT INTO tbl_shops(name, address, city, state, contact_person, mobile,shop_added_by,latitude,longitude,suburbid,shop_type_id,bank_acc_name,bank_acc_no,bank_b_name,bank_ifsc)
		VALUES('".$store_name."','".$store_Address."','".$city."','".$state."','".$owner_name."','".$contact_no."','".$salespersonid."','".$lat."','".$lng."','".$suburbid."','".$shop_type_id."','".$bank_acc_name."','".$bank_acc_no."','".$bank_b_name."','".$bank_ifsc."')";
		$proRow = $this->executeQuery($sql);
		$last_id = mysql_insert_id();
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Store']['msg'] = "Store added successfully.";
		$this->json_array['data']['Store']['id'] = $last_id;
		return json_encode($this->json_array);
	}
		
	function get_shoplist($userid, $suburbid=null) {//shop list by Reginal head's regions
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '9';
	  if($suburbid != null){
		  $sql = "SELECT suburb_ids 
		  FROM tbl_user_working_area WHERE user_id = '".$userid."' AND suburb_ids IN (".$suburbid.")";
		  $extIdResult 	= $this->executeQuery($sql);
		  $rowcount = mysql_num_rows($extIdResult);
		  $extIdRow 	= mysql_fetch_array($extIdResult);
		  $extIdRow['suburb_ids'] = $suburbid;
	  }else{
		   $sql = "SELECT suburb_ids 
		  FROM tbl_user_working_area WHERE user_id = '".$userid."'";
		  $extIdResult 	= $this->executeQuery($sql);
		  $rowcount = mysql_num_rows($extIdResult);
		  $extIdRow 	= mysql_fetch_array($extIdResult);
	  }
		if($rowcount  >0 && $extIdRow['suburb_ids']!=""){	
		  $sql = " SELECT 
			tbl_shops.id,  tbl_shops.name,  tbl_shops.address,  
			tbl_shops.city,  tbl_shops.state,  tbl_shops.suburbid, 
			tbl_shops.contact_person,  tbl_shops.mobile,  tbl_shops.shop_added_by, 
			tbl_shops.latitude,  tbl_shops.longitude,  closedday,  
			opentime,  closetime,  
			tbl_shops.gst_number, tbl_shops.subarea_id, subarea_name, 
			tbl_shops.state, tbl_state.name as state_name, 
			tbl_shops.city, tbl_city.name as city_name, 
			tbl_surb.suburbnm as suburb  ,
			tbl_shops.shop_type_id, tbl_shops.bank_acc_name, tbl_shops.bank_acc_no, 
			tbl_shops.bank_b_name, tbl_shops.bank_ifsc
		FROM tbl_shops     
		LEFT JOIN tbl_subarea ON tbl_shops.subarea_id = tbl_subarea.subarea_id     
		LEFT JOIN tbl_state ON tbl_state.id = tbl_shops.state     
		LEFT JOIN tbl_city ON tbl_city.id = tbl_shops.city    
		LEFT JOIN tbl_surb ON tbl_surb.id = tbl_shops.suburbid  
		WHERE tbl_shops.suburbid IN (".$extIdRow['suburb_ids'].") 
		AND tbl_shops.isdeleted!='1' 
		AND tbl_shops.shop_status = 'Active'";
		$proRow = $this->executeQuery($sql);
		 while($row = mysql_fetch_array($proRow))
		  {	  
			$shop_array_temp['id'] = $row['id'];
			$shop_array_temp['name'] = $row['name'];
			$shop_array_temp['address'] = $row['address'];
			$shop_array_temp['contact_person'] = $uname;
			$shop_array_temp['lat'] = $row['latitude'];
			$shop_array_temp['lon'] = $row['longitude'];
			$shop_array_temp['closedday'] = $this->fnGetday($row['closedday']);
			$shop_array_temp['opentime'] = $row['opentime'];
			$shop_array_temp['closetime'] = $row['closetime'];
			$shop_array_temp['mobile'] = $row['mobile'];
			$shop_array_temp['gst_number'] = $row['gst_number'];	
			$shop_array_temp['city_id'] = $row['city'];
			$shop_array_temp['city'] = $row['city_name'];
			$shop_array_temp['state_id'] = $row['state'];
			$shop_array_temp['state'] = $row['state_name'];	
			$shop_array_temp['suburb_id'] = $row['suburbid'];
			$shop_array_temp['suburb'] = $row['suburb'];	
			$shop_array_temp['subarea_id'] = $row['subarea_id'];
			$shop_array_temp['subarea_name'] = $row['subarea_name'];			
			$this->json_array['data']['shops'][]=$shop_array_temp;	
		  }
		}else{
			 $this->json_array['data']['shops'][]="Shops not available";	
		}
	  return json_encode($this->json_array);
	}
	function fnGetday($dayid) {
		$str="";
		switch($dayid){
			case '1':
				$str="Monday";
			break;
			case '2':
				$str="Tuesday";
			break;
			case '3':
				$str="Wednesday";
			break;
			case '4':
				$str="Thursday";
			break;
			case '5':
				$str="Friday";
			break;
			case '6':
				$str="Saturday";
			break;
			case '7':
				$str="Sunday";
			break;
		}
		return $str;
	}
	function fngetStatenm($id){
	  $sql = "SELECT `id`,`name` FROM tbl_state WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $row = mysql_fetch_array($proRow);
	  return $row['name'];
	}
	function fngetCitynm($id){
	  $sql = "SELECT `id`,`name` FROM tbl_city WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $row = mysql_fetch_array($proRow);
	  return $row['name'];
	}
	function fngetSuburbnm($id){
	  $sql = "SELECT `suburbnm` FROM  tbl_surb WHERE id='".$id."'";
	  $proRow = $this->executeQuery($sql);
	  $row = mysql_fetch_array($proRow);
	  return $row['suburbnm'];
	}
	function get_orderdata_old($userid,$pageid){
		$npageid=$pageid*10;
		if($pageid=='1')
			$limit=0;
		else{
			$limit=($pageid-1)*10;
		} 
	$sql = "select shops.name as shopname,VO.shopid, OA.order_date,  VO.orderid, VO.variantweight,
	SUM(VO.totalcost*VO.variantunit) as totalcost,
	count(VO.orderappid) as variantunit, 
	IF(OA.offer_provided = 0,'Offer Applied','Offer Not Applied') AS offer_provided_val, OA.catid
	FROM `tbl_variant_order` VO
	left join tbl_order_app OA on OA.id=VO.orderappid
	LEFT JOIN
		tbl_shops shops ON shops.id= VO.shopid
	WHERE 
	order_by='".$userid."' AND DATE_FORMAT(OA.order_date,'%Y-%m-%d')=DATE(NOW())
	GROUP BY VO.orderid limit $limit,$npageid ";//
	$proRow = $this->executeQuery($sql);
	if($rowcount = mysql_num_rows($proRow)>0){
		$this->json_array['status']['responsecode'] = '0';
	$this->json_array['status']['entity'] = '9';
	while($row = mysql_fetch_array($proRow)){
		$tcost=$row['totalcost']; 
			$sql_free_pro = "SELECT COUNT(tbl_variant_order.id) as quantity_free FROM `tbl_variant_order` INNER JOIN `tbl_order_app` ON tbl_order_app.id = tbl_variant_order.orderappid WHERE campaign_sale_type='free' AND catid = ".$row['catid'];
			$result_free_pro = mysqli_query($con,$sql_free_pro);
			$row_free_pro = mysqli_fetch_assoc($result_free_pro);
			$free_quantity = 0;
			$sale_quantity = $row['variantunit'];
			if($row_free_pro['quantity_free'] > 0)
			{
				$free_quantity = $row_free_pro['quantity_free'];
				$sale_quantity = $row['variantunit'] - $free_quantity;
			}
	$ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', '');//$rowtc['tcost'];
	$ohistory_array_temp['free_quantity'] = $free_quantity;
	$ohistory_array_temp['sale_quantity'] = $sale_quantity;
	$ohistory_array_temp['quantity'] = $row['variantunit'];
	$ohistory_array_temp['orderid'] = $row['orderid'];
	$ohistory_array_temp['order_date'] = $row['order_date'];
	$ohistory_array_temp['shopid'] = $row['shopid'];
	$ohistory_array_temp['shopnme'] = $row['shopname'];//$rowh['shopnme'];
	$ohistory_array_temp['offer_applied'] = $row['offer_provided_val'];
	$this->json_array['data']['ohistory'][]=$ohistory_array_temp;
	}
	}
	else{
		$this->json_array['status']['responsecode'] = '1';
	$this->json_array['status']['entity'] = '1';
	$this->json_array['data']['ohistory'][]="No records found";
	}
	return json_encode($this->json_array);
	}
	function get_campaigndata()
	{ 
		$sql_campaign = "SELECT `id` AS campaign_id, `campaign_name`, `campaign_description`, `campaign_start_date`, `campaign_end_date`, `campaign_type`, `status` FROM `tbl_campaign` WHERE isdeleted != 1 AND status =0 AND campaign_end_date >= '".date('Y-m-d')."'";
		
		$result_campaign = $this->executeQuery($sql_campaign);
	   
		while($row_campaign = mysql_fetch_array($result_campaign)) 
		{
			$campaign_array_temp['campaign_id'] = $row_campaign['campaign_id'];	
			$campaign_array_temp['campaign_name'] = $row_campaign['campaign_name'];
			$campaign_array_temp['campaign_description'] = $row_campaign['campaign_description'];
			$campaign_array_temp['campaign_start_date'] = $row_campaign['campaign_start_date'];
			$campaign_array_temp['campaign_end_date'] = $row_campaign['campaign_end_date'];
			$campaign_array_temp['campaign_type'] = $row_campaign['campaign_type'];
			
			$sql_campaign_area="SELECT `level`, `state_id`, `city_id`, `suburb_id`, `shop_id`, subarea_id FROM `tbl_campaign_area` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
			
			$result_campaign_area = $this->executeQuery($sql_campaign_area);
			$campaign_array_temp['campaign_area'] = array();
			$campaign_array_temp['campaign_product'] = array();
			$campaign_array_temp['campaign_product_weight'] = array();
			while($row_campaign_area = mysql_fetch_array($result_campaign_area)) {
					$campaign_array_temp['campaign_area']['level']  = $row_campaign_area['level'];	
								
				$sql_state="SELECT `name` FROM `tbl_state` WHERE id IN (".$row_campaign_area['state_id'].")";
				$result_state = $this->executeQuery($sql_state);
				$state_name_arr = array();
				
				while($row_state = mysql_fetch_array($result_state)){
					$state_name_arr[] = $row_state['name'];									
				}
				$state_name = implode(',',$state_name_arr);
				$campaign_array_temp['campaign_area']['state_id']  = $row_campaign_area['state_id'];
				$campaign_array_temp['campaign_area']['state_name']  = $state_name;					
								
				if($row_campaign_area['city_id'] != '')
				{
					$sql_city="SELECT `name` FROM `tbl_city` WHERE id IN (".$row_campaign_area['city_id'].")";
					$result_city = $this->executeQuery($sql_city);
					$city_name_arr = array();
					while($row_city = mysql_fetch_array($result_city)){
						$city_name_arr[] = $row_city['name'];									
					}
					$city_name = implode(',',$city_name_arr);
					$campaign_array_temp['campaign_area']['city_id']  = $row_campaign_area['city_id'];
					$campaign_array_temp['campaign_area']['city_name']  = $city_name;	
				} else {
					$campaign_array_temp['campaign_area']['city_id']  = "";
					$campaign_array_temp['campaign_area']['city_name']  = "";	
				}
				if($row_campaign_area['suburb_id'] != '')
				{
					$sql_suburb="SELECT `suburbnm` FROM `tbl_surb` WHERE id IN (".$row_campaign_area['suburb_id'].")";
					$result_suburb = $this->executeQuery($sql_suburb);
					$suburb_name_arr = array();
					while($row_suburb = mysql_fetch_array($result_suburb)){
						$suburb_name_arr[] = $row_suburb['suburbnm'];									
					}
					$suburb_name = implode(',',$suburb_name_arr);
					$campaign_array_temp['campaign_area']['suburb_id']  = $row_campaign_area['suburb_id'];
					$campaign_array_temp['campaign_area']['suburb_name']  = $suburb_name;	
				} else {
					$campaign_array_temp['campaign_area']['suburb_id']  = "";
					$campaign_array_temp['campaign_area']['suburb_name']  = "";	
				}
				
				// 
				if($row_campaign_area['subarea_id'] != '')
				{
					$sql_subarea="SELECT subarea_name,subarea_id FROM tbl_subarea WHERE subarea_id IN (".$row_campaign_area['subarea_id'].")";
					
					$result_subarea = $this->executeQuery($sql_subarea);
					$subarea_name_arr = array();
					
					while($row_subarea = mysql_fetch_array($result_subarea)) {
						$subarea_name_arr[] = $row_subarea['subarea_name'];									
					}
					
					$subarea_name = implode(',',$subarea_name_arr);
					
					$campaign_array_temp['campaign_area']['subarea_id']  = $row_campaign_area['subarea_id'];
					$campaign_array_temp['campaign_area']['subarea_name']  = $subarea_name;
					
				} else {
					$campaign_array_temp['campaign_area']['subarea_id']  = "";
					$campaign_array_temp['campaign_area']['subarea_name']  = "";	
				}
				
				if($row_campaign_area['shop_id'] != '')
				{
					$sql_shop="SELECT `name` FROM `tbl_shops` WHERE id IN (".$row_campaign_area['shop_id'].")";
					$result_shop = $this->executeQuery($sql_shop);
					$shop_name_arr = array();
					while($row_shop = mysql_fetch_array($result_shop)){
						$shop_name_arr[] = $row_shop['name'];									
					}
					$shop_name = implode(',',$shop_name_arr);
					$campaign_array_temp['campaign_area']['shop_id']  = $row_campaign_area['shop_id'];
					$campaign_array_temp['campaign_area']['shop_name']  = $shop_name;	
				}else{
					$campaign_array_temp['campaign_area']['shop_id']  = "";
					$campaign_array_temp['campaign_area']['shop_name']  = "";	
				}
			}
			
			switch($row_campaign['campaign_type']){
				case 'discount':				
						$sql_campaign_area_price="SELECT `product_price`, `discount_percent` FROM `tbl_campaign_area_price` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
						$result_campaign_area_price = $this->executeQuery($sql_campaign_area_price);	
						$i=1;
						$campaign_array_temp['area_price'] = array();
						while($row_campaign_area_price = mysql_fetch_array($result_campaign_area_price)){
							$record_discount = array();
							$record_discount['record_counter'] = $i;
							$record_discount['product_price'] = $row_campaign_area_price['product_price'];
							$record_discount['discount_percent'] = $row_campaign_area_price['discount_percent'];
							$campaign_array_temp['area_price'][]  = $record_discount;						
							$i++;
						}
					break;
				case 'price_weight':				
						$sql_campaign_price_weight="SELECT `w_p_brand_id`, `w_p_category_id`, `w_p_product_id`, `w_p_variant_id`, `weight`, `price_per_unit`, `wt_unit_id`, `wt_unit` FROM `tbl_campaign_wt_price` WHERE campaign_id=".$row_campaign['campaign_id'];
						$result_campaign_price_weight = $this->executeQuery($sql_campaign_price_weight);	
						$i=1;
						$campaign_array_temp['price_weight'] = array();
						while($row_campaign_price_weight = mysql_fetch_array($result_campaign_price_weight)){
							$record_price_weight = array();
							$record_price_weight['record_counter'] = $i;
							$record_price_weight['brand_id'] = $row_campaign_price_weight['w_p_brand_id'];
							$record_price_weight['brand_name']  = $this->getBrand($row_campaign_price_weight['w_p_brand_id']);
							$record_price_weight['category_id'] = $row_campaign_price_weight['w_p_category_id'];
							$record_price_weight['category_name']  = $this->getCategory($row_campaign_price_weight['w_p_category_id']);
							$record_price_weight['product_id'] = $row_campaign_price_weight['w_p_product_id'];
							$record_price_weight['product_name']  = $this->getProduct($row_campaign_price_weight['w_p_product_id']);
							$record_price_weight['variant_id'] = $row_campaign_price_weight['w_p_variant_id'];
							
							
							$sql="SELECT * from `tbl_product_variant` where id = ".$row_campaign_price_weight['w_p_variant_id'];
							$result1 = $this->executeQuery($sql);
							$row_variant = mysql_fetch_array($result1);	
							$exp_variant1 = $row_variant['variant_1'];
							$imp_variant1= explode(',',$exp_variant1);
							$weightunit1=$this->fnGetUnit($imp_variant1[1]);
							$exp_variant2 = $row_variant['variant_2'];
							$imp_variant2= explode(',',$exp_variant2);
							$weightunit2=$this->fnGetUnit($imp_variant2[1]);
							$record_price_weight['variant_wt1'] = $imp_variant1[0];
							$record_price_weight['variant_unit1'] = $weightunit1;
							$record_price_weight['variant_wt2'] = $imp_variant2[0];
							$record_price_weight['variant_unit2'] = $weightunit2;
							$record_price_weight['weight'] = $row_campaign_price_weight['weight'];
							$record_price_weight['wt_unit_id'] = $row_campaign_price_weight['wt_unit_id'];
							$record_price_weight['wt_unit'] = $row_campaign_price_weight['wt_unit'];
							$record_price_weight['price_per_unit'] = $row_campaign_price_weight['price_per_unit'];
						
							$campaign_array_temp['price_weight'][]  = $record_price_weight;						
							$i++;
						}
					break;
				case 'free_product':
				
					$sql_campaign_free_product="SELECT `c_p_brand_id`, `c_p_category_id`, `c_product_id`, `c_p_quantity`, `c_p_measure`, `c_p_measure_id`,`c_p_quantity_measure`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`,`f_p_quantity_measure` FROM `tbl_campaign_product` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
					
					$result_campaign_free_product = $this->executeQuery($sql_campaign_free_product);
					$campaign_array_temp['area_price'] = array();
					$campaign_array_temp['campaign_product'] = array();
					$campaign_array_temp['campaign_product_weight'] = array();
					
					$j = 1;
					while($row_campaign_free_product = mysql_fetch_array($result_campaign_free_product)){
						$record_product = array();
						$record_product['record_counter'] = $j;
						$record_product['c_p_brand_id']  = $row_campaign_free_product['c_p_brand_id'];
						$record_product['c_p_brand_name']  = $this->getBrand($row_campaign_free_product['c_p_brand_id']);
						$record_product['c_p_category_id']  = $row_campaign_free_product['c_p_category_id'];
						$record_product['c_p_category_name']  = $this->getCategory($row_campaign_free_product['c_p_category_id']);
						$record_product['c_product_id']  = $row_campaign_free_product['c_product_id'];
						$record_product['c_product_name']  = $this->getProduct($row_campaign_free_product['c_product_id']);
						if($row_campaign_free_product['c_p_measure_id'] !='')
						{
							$record_product['c_product_variant_wt_quantity']  = $row_campaign_free_product['c_p_quantity'];
							$record_product['c_product_variant_unit']  = $row_campaign_free_product['c_p_measure'];
							$record_product['c_product_variant']  = $row_campaign_free_product['c_p_quantity']." ".$row_campaign_free_product['c_p_measure'];
							$record_product['c_product_variant_unit_id']  = $row_campaign_free_product['c_p_measure_id'];
						}
						else
							$campaign_array_temp['campaign_product']['c_product_variant']  = "";
						
						$record_product['f_p_brand_id']  = $row_campaign_free_product['f_p_brand_id'];
						$record_product['f_p_brand_name']  = $this->getBrand($row_campaign_free_product['f_p_brand_id']);
						$record_product['f_p_category_id']  = $row_campaign_free_product['f_p_category_id'];
						$record_product['f_p_category_name']  = $this->getCategory($row_campaign_free_product['f_p_category_id']);
						$record_product['f_product_id']  = $row_campaign_free_product['f_product_id'];	
						$record_product['f_product_name']  = $this->getProduct($row_campaign_free_product['f_product_id']);
						if($row_campaign_free_product['f_p_measure_id'] !='')
						{
							$record_product['f_product_variant_wt_quantity']  = $row_campaign_free_product['f_p_quantity'];
							$record_product['f_product_variant_unit']  = $row_campaign_free_product['f_p_measure'];
							$record_product['f_product_variant']  = $row_campaign_free_product['f_p_quantity']." ".$row_campaign_free_product['f_p_measure'];
							$record_product['f_product_variant_unit_id']  = $row_campaign_free_product['f_p_measure_id'];
						}
						else
							$campaign_array_temp['campaign_product']['f_product_variant']  = "";		

						$campaign_array_temp['campaign_product'][] = $record_product;
						$j++;
					}
					break;
				case 'by_weight':				
					$sql_campaign_product_weight="SELECT `id`, `campaign_id`, `campaign_area_id`, `c_weight`, `c_unit`, `f_p_brand_id`, `f_p_category_id`, `f_product_id`, `f_p_quantity`, `f_p_measure`, `f_p_measure_id`, `f_p_quantity_measure` FROM `tbl_campaign_product_weight` WHERE deleted = 0 AND campaign_id=".$row_campaign['campaign_id'];
					
					$result_campaign_product_weight = $this->executeQuery($sql_campaign_product_weight);
					$campaign_array_temp['area_price'] = array();
					$campaign_array_temp['campaign_product'] = array();
					$campaign_array_temp['campaign_product_weight'] = array();
					$j = 1;
					while($row_campaign_product_weight = mysql_fetch_array($result_campaign_product_weight)){
						$record_product = array();
						$record_product['record_counter'] = $j;						
						$record_product['c_weight']  = $row_campaign_product_weight['c_weight'];	
						$record_product['c_unit']  = $row_campaign_product_weight['c_unit'];							
						
						$record_product['f_p_brand_id']  = $row_campaign_product_weight['f_p_brand_id'];
						$record_product['f_p_brand_name']  = $this->getBrand($row_campaign_product_weight['f_p_brand_id']);
						$record_product['f_p_category_id']  = $row_campaign_product_weight['f_p_category_id'];
						$record_product['f_p_category_name']  = $this->getCategory($row_campaign_product_weight['f_p_category_id']);
						$record_product['f_product_id']  = $row_campaign_product_weight['f_product_id'];
						$record_product['f_product_name']  = $this->getProduct($row_campaign_product_weight['f_product_id']);
						if($row_campaign_product_weight['f_p_measure_id'] !='')
						{
							$record_product['f_product_variant_wt_quantity']  = $row_campaign_product_weight['f_p_quantity'];
							$record_product['f_product_variant_unit']  = $row_campaign_product_weight['f_p_measure'];
							$record_product['f_product_variant']  = $row_campaign_product_weight['f_p_quantity']." ".$row_campaign_product_weight['f_p_measure'];
							$record_product['f_product_variant_unit_id']  = $row_campaign_product_weight['f_p_measure_id'];
						}
						else
							$campaign_array_temp['campaign_product_weight']['f_product_variant']  = "";									

						
						$campaign_array_temp['campaign_product_weight'][] = $record_product;
						$j++;
					}
					break;
			}	
			$data[]=$campaign_array_temp;
		}
		if(count($data) == 0)
			$data = [];
		
		return $data;
	}
	function getBrand($id){
		$sql_brand="SELECT `name` FROM `tbl_brand` WHERE isdeleted != 1 AND id =".$id;
		$result_brand = $this->executeQuery($sql_brand);
		$row_brand = mysql_fetch_array($result_brand);
		return $row_brand['name'];
	}
	function getCategory($id){
		$sql_category="SELECT `categorynm` FROM `tbl_category` WHERE isdeleted != 1 AND id =".$id;
		$result_category = $this->executeQuery($sql_category);
		$row_category = mysql_fetch_array($result_category);
		return $row_category['categorynm'];
	}
	function getProduct($id){
		$sql_product="SELECT `productname` FROM `tbl_product` WHERE isdeleted != 1 AND id =".$id;
		$result_product = $this->executeQuery($sql_product);	
		$row_product = mysql_fetch_array($result_product);
		return $row_product['productname'];
	}
	function getVariantUnit($id){
		$sql_variant="SELECT `unitname` FROM `tbl_units` WHERE id =".$id;
		$result_variant = $this->executeQuery($sql_variant);
		$row_variant = mysql_fetch_array($result_variant);	
		return $row_variant['unitname'];
	}
	function get_subarea($suburbids){
	$sqlcity = "SELECT subarea_id,subarea_name,suburb_id FROM tbl_subarea WHERE suburb_id in ($suburbids) AND isdeleted !='1'";
		$proRowcity = $this->executeQuery($sqlcity);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '2';
		while($rowcity = mysql_fetch_array($proRowcity)) {
			$c_array_temp['subarea_name']	= $rowcity['subarea_name'];
			$c_array_temp['subarea_id'] 	= $rowcity['subarea_id'];
			$c_array_temp['suburb_id'] 	= $rowcity['suburb_id'];
			$this->json_array['data']['subarea'][]=$c_array_temp;
		}
		return json_encode($this->json_array);
	}
	function get_distributorlist($salespersonid) {
		  $sql = "SELECT external_id 
		  FROM tbl_user WHERE id = '".$salespersonid."'";
		  $proRow 		= $this->executeQuery($sql);
		  $row 			= mysql_fetch_array($proRow);//print_r($row );
		  /*$suburb_ids 	= $row["suburb_ids"];
		  $subarea_ids 	= $row["subarea_ids"];
		  $state 		= $row["state"];*/
		  $city 		= $row["city"];
		  $external_id 		= $row["external_id"];
		/*$sqlcity = "SELECT 
			`id`, 	 
			`firstname`	 	
		FROM 
			`tbl_user` 
		WHERE 
			user_type = 'Distributor' AND city= '$city'";
		$proRowcity = $this->executeQuery($sqlcity);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '2';
		if (mysql_num_rows($proRowcity) != 0) {
			while($rowcity = mysql_fetch_array($proRowcity)) {
				$c_array_temp['id']	= $rowcity['id'];
				$c_array_temp['firstname'] 	= $rowcity['firstname'];
				$this->json_array['data']['distributor'][]=$c_array_temp;
			}
		}else{*/
			$sqlstockist = "SELECT 
			`id`, 	 
			`firstname`	 	
			FROM 
				`tbl_user` 
			WHERE 
				user_type = 'Distributor' AND id IN ($external_id) AND isdeleted!='1' ";
			$proRowstockist = $this->executeQuery($sqlstockist);
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
			while($rowstockist = mysql_fetch_array($proRowstockist)) {
				$c_array_temp['id']	= $rowstockist['id'];
				$c_array_temp['firstname'] 	= $rowstockist['firstname'];
				$this->json_array['data']['distributor'][]=$c_array_temp;
			}
		//}
		return json_encode($this->json_array);
	}
	function fnsaveNoordertakenHistory($shop_id,$salesperson_id,$shop_close_reason_type,$shop_close_reason_details,$shop_visit_date_time ) {//,$no_order_lat,$no_order_lon
		$sql="INSERT INTO tbl_shop_visit(shop_id,salesperson_id,shop_close_reason_type, shop_close_reason_details, shop_visit_date_time)VALUES('".$shop_id."' , '".$salesperson_id."' , '".$shop_close_reason_type."' , '".$shop_close_reason_details."' , '".$shop_visit_date_time."')";
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['leave']['msg'] = "No order taken history has been stored successfully.";
		$salespersonname = "";
		$sql = "SELECT firstname FROM  `tbl_user` WHERE id='".$salesperson_id ."'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$salespersonname = $row['firstname'];
		}
		$shopname = "";
		$sql = "SELECT name FROM  tbl_shops WHERE id='".$shop_id ."'";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) != 0) {
			$row = mysql_fetch_array($proRow);
			$shopname = $row['name'];
		}
		$subject = "No Order Recieved";
		$message = "Hi Admin,<br><br> Following shop not taked order.<br>";
		$message .= "<br><b>Salesperson name:</b> ". $salespersonname;
		$message .= "<br><b>Shop name:</b> ". $shopname;
		$message .= "<br><b>Reason:</b> ".$shop_close_reason_type;
		$message .= "<br><b>Reason details:</b> ". $shop_close_reason_details;
		$message .= "<br><br>BR, </br>".COMPANYNM." Team";
		//$FROMMAILID = FROMMAILID;
		$FROMMAILID = "farmharvest@admin.com";
		//$headers = 'From: '. $FROMMAILID  .'\r\n Reply-To: ' . $FROMMAILID;
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		// More headers
		$headers .= 'From: <'.$FROMMAILID.'>' . "\r\n";
		//$headers .= 'Cc: myboss@example.com' . "\r\n";
		$to = "farmharvest@admin.com";
		@mail($to , $subject , $message , $headers);
		return json_encode($this->json_array);
	}
	function save_userlocation($userid,$latt,$longg) {
		$last_id=0;
		 $sql_point_check="select id from `tbl_user_location` where userid='$userid' and lattitude='$latt' and longitude='$longg' and DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW()) ORDER BY id DESC LIMIT 1";
		$proRowp = $this->executeQuery($sql_point_check);
		if(mysql_num_rows($proRowp)!=0){	
			$this->json_array['data']['Location']['id'] = $last_id;
		}else{
			if($latt!='0.0'&&$longg!='0.0'){
				$sql="INSERT INTO tbl_user_location(userid, lattitude, longitude,tdate)VALUES('".$userid."','".$latt."','".$longg."',now())";
				$proRow = $this->executeQuery($sql);
				$last_id = mysql_insert_id();
				$this->json_array['data']['Location']['id'] = $last_id;
			}
		}
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Location']['msg'] = "Location added successfully.";
		
		return json_encode($this->json_array);
	}
	function GetDrivingDistance($lat1, $lat2, $long1, $long2)
	{
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response, true);
		$dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
		$time = $response_a['rows'][0]['elements'][0]['duration']['text'];
		return array('distance' => $dist, 'time' => $time);
	}
	//count for images TADA bill
	function get_sptravel_count($data) {
		$userid =$data['userid'];
		$sqlfordistance = "SELECT max(id) as maxid FROM  tbl_sp_tadabill WHERE userid='".$userid ."' and date_format(date_tada,'%d-%m-%Y')= date_format(NOW(), '%d-%m-%Y')";
		$proRowdist = $this->executeQuery($sqlfordistance);
		$rowAttendance = mysql_fetch_assoc($proRowdist);
		return $rowAttendance['maxid'];
	}
	function save_sptravel($data) {
		$userid =$data['userid'];
		$distance_covered =$data['distance_covered'];
		$mode_of_transe =$data['mode_of_transe'];
		$food =$data['food'];
		$other =$data['other'];
		$sqlfordistance = "SELECT 	id,lattitude,longitude FROM  tbl_user_location WHERE userid='".$userid ."' and date_format(tdate,'%d-%m-%Y')= date_format(NOW(), '%d-%m-%Y')";
		$proRowdist = $this->executeQuery($sqlfordistance);
		$countpoints=mysql_num_rows($proRowdist);
		$countrec=0;
		if (mysql_num_rows($proRowdist) > 0) {
			while($rowc = mysql_fetch_array($proRowdist)) {
				$c_array_temp[$countrec]['id'] = $rowc['id'];
				$c_array_temp[$countrec]['lattitude'] = $rowc['lattitude'];
				$c_array_temp[$countrec]['longitude'] = $rowc['longitude'];
				$countrec++;
			}
		}
		//echo "".$countpoints."<pre>";print_r($c_array_temp);
		 $google_distance=0;
		for($i=0;$i<$countpoints-1;$i++){
			$google_d=$this->GetDrivingDistance($c_array_temp[$i]['lattitude'],$c_array_temp[$i+1]['lattitude'],$c_array_temp[$i]['longitude'],$c_array_temp[$i+1]['longitude']);
			//echo "<pre>";print_r($google_d);
			$google_distance=$google_distance+$google_d['distance'];
		} 
		$google_distance=bcdiv($google_distance, 1000, 3);
		
		/* $sqlforrate="SELECT rupees_per_km from tbl_mode_transe where id='$mode_of_transe'";
		$prorateRow = $this->executeQuery($sqlforrate);$rupees_per_km=0;
		while($rowc = mysql_fetch_array($prorateRow)) {
			$rupees_per_km = $rowc['rupees_per_km'];
		}*/
		 $sql="INSERT INTO tbl_sp_tadabill(userid, distance_covered,google_distance, mode_of_transe,food,other,date_tada,Current_rate_mot)VALUES
		('".$userid."','".$distance_covered."','".$google_distance."','".$mode_of_transe."','".$food."','".$other."',now(),'".$rupees_per_km."')";
		$proRow = $this->executeQuery($sql);
		$last_id = mysql_insert_id();
		$this->json_array['data']['Travel']['msg'] = "Expense bill submitted successfully.";
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Travel']['id'] = $last_id;
		return json_encode($this->json_array);
	}
	//backup above
	function save_sptravel_old_for_repeat_update($data) {
		$userid =$data['userid'];
		$distance_covered =$data['distance_covered'];
		$mode_of_transe =$data['mode_of_transe'];
		$food =$data['food'];
		$other =$data['other'];
		$sqlfordistance = "SELECT 	id,lattitude,longitude FROM  tbl_user_location WHERE userid='".$userid ."' and date_format(tdate,'%d-%m-%Y')= date_format(NOW(), '%d-%m-%Y')";
		$proRowdist = $this->executeQuery($sqlfordistance);
		$countpoints=mysql_num_rows($proRowdist);
		$countrec=0;
		if (mysql_num_rows($proRowdist) > 0) {
			while($rowc = mysql_fetch_array($proRowdist)) {
				$c_array_temp[$countrec]['id'] = $rowc['id'];
				$c_array_temp[$countrec]['lattitude'] = $rowc['lattitude'];
				$c_array_temp[$countrec]['longitude'] = $rowc['longitude'];
				$countrec++;
			}
		}
		//echo "".$countpoints."<pre>";print_r($c_array_temp);
		 $google_distance=0;
		for($i=0;$i<$countpoints-1;$i++){
			$google_d=$this->GetDrivingDistance($c_array_temp[$i]['lattitude'],$c_array_temp[$i+1]['lattitude'],$c_array_temp[$i]['longitude'],$c_array_temp[$i+1]['longitude']);
			//echo "<pre>";print_r($google_d);
			$google_distance=$google_distance+$google_d['distance'];
		} 
		$google_distance=bcdiv($google_distance, 1000, 3);
		$sql = "SELECT id,userid,date_tada FROM  tbl_sp_tadabill WHERE userid='".$userid ."' 
		AND DATE_FORMAT(date_tada,'%Y-%m-%d')=DATE(NOW())";
		$proRow = $this->executeQuery($sql);
		if (mysql_num_rows($proRow) > 0) {
			$row = mysql_fetch_array($proRow);
			$last_id = $row['id'];
			$sql="update  tbl_sp_tadabill set 
			userid='$userid', distance_covered='$distance_covered',google_distance='$google_distance',
			mode_of_transe='$mode_of_transe',food='$food',other='$other',date_tada=now()
			where id='$last_id'";
			$proRow = $this->executeQuery($sql);
			$this->json_array['data']['Travel']['msg'] = "Expense bill updated successfully.";
		}else{
			$sql="INSERT INTO tbl_sp_tadabill(userid, distance_covered,google_distance, mode_of_transe,food,other,date_tada)VALUES
			('".$userid."','".$distance_covered."','".$google_distance."','".$mode_of_transe."','".$food."','".$other."',now())";
			$proRow = $this->executeQuery($sql);
			$last_id = mysql_insert_id();
			$this->json_array['data']['Travel']['msg'] = "Expense bill submitted successfully.";
		}
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Travel']['id'] = $last_id;
		return json_encode($this->json_array);
	}
	function save_sp_todaystravel($data) {
		$userid =$data['userid'];
		$flag =$data['flag'];
		$comments =$data['comments'];
		$user_type =$data['user_type'];
		$visit_to_id =$data['visit_to_id'];//shop/ Trasnport office
		
		$sql = "SELECT * from tbl_sp_attendance WHERE sp_id='".$userid ."' 
		AND DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW())";
		$proRow = $this->executeQuery($sql);
		$c_array_temp=array();
		while($rowc = mysql_fetch_array($proRow)) {
			$c_array_temp['dayendtime']   = $rowc['dayendtime'];
			$c_array_temp['daystarttime'] = $rowc['daystarttime'];			
		}
		if (mysql_num_rows($proRow) == 0) {
				$this->json_array['data']['Track']['msg'] = "Day not started yet.";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = '';
				return json_encode($this->json_array);
		}else if($c_array_temp['dayendtime']!== NULL){
				$this->json_array['data']['Track']['msg'] = "Day already ended.";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = '';
				return json_encode($this->json_array);
		}else{
			if($user_type == 'SalesPerson' || $user_type == ''){
				$sql="INSERT INTO tbl_shop_visit(salesperson_id, shop_visit_date_time, flag, comments, is_shop_location)VALUES
				('".$userid."',now(),'".$flag."','".$comments."','1')";
				$proRow = $this->executeQuery($sql);
				$last_id = mysql_insert_id();
				$this->json_array['data']['Track']['msg'] = "Location track ".$flag.".";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = $last_id;
			}else if($user_type == 'DeliveryPerson'){
				$sql="INSERT INTO tbl_transport_off_visit(`transport_off_id`, `deliveryperson_id`, `transport_off_visit_date_time`, `flag`, `comments`)
				VALUES
				('".$visit_to_id."', '".$userid."',now(),'".$flag."','".$comments."','1')";
				$proRow = $this->executeQuery($sql);
				$last_id = mysql_insert_id();
				$this->json_array['data']['Track']['msg'] = "Location track ".$flag.".";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				$this->json_array['data']['Track']['id'] = $last_id;
			}
			return json_encode($this->json_array);
		}
		
		
		
	}
	function get_userlocation($userid){
		$sql="select id,userid,lattitude,longitude from tbl_user_location where userid='$userid'";
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '2';
		$countt=0;
		while($rowc = mysql_fetch_array($proRow)) {
						$c_array_temp['id']        = $rowc['id'];
						$c_array_temp['userid'] = $rowc['userid'];
						$c_array_temp['lattitude']           = $rowc['lattitude'];
						$c_array_temp['longitude']         = $rowc['longitude'];
						$this->json_array['data']['location'][]=$c_array_temp;                    
						$countt++;
		}
		$this->json_array['data']['sdpoint'][]=$this->json_array['data']['location'][0];
		$this->json_array['data']['sdpoint'][]=$this->json_array['data']['location'][$countt-1];
		for($i=0;$i<$countt;$i++){
						$this->json_array['data']['location'][$i]['latd']=$this->json_array['data']['location'][$i+1]['lattitude'];
						$this->json_array['data']['location'][$i]['longgd']=$this->json_array['data']['location'][$i+1]['longitude'];
		}
		$this->json_array['data']['location'][$countt-1]['latd']=$this->json_array['data']['location'][$countt-1]['lattitude'];
		$this->json_array['data']['location'][$countt-1]['longgd']=$this->json_array['data']['location'][$countt-1]['longitude'];
		return json_encode($this->json_array);                 
	}
	//new service for shop edit
	function fnEditStore($salespersonid,$store_name,$contact_no,$store_Address,$lat,$lng,$city,$state,$suburbid,$shopid,$shop_type_id, $bank_acc_name,$bank_acc_no,$bank_b_name,$bank_ifsc) {
		/*
			bank_acc_no:""
			bank_b_name:""
			bank_ifsc:""
			bank_acc_name:"" 
			shop_type_id:"" 
			*/
		
		$sql ="UPDATE tbl_shops SET 
		name='$store_name',
		address='$store_Address',
		state='$state',
		city='$city',
		mobile='$contact_no',
		shop_added_by='$salespersonid',
		latitude='$lat',                   
		longitude='$lng',
		suburbid='$suburbid'
		where id='$shopid'";      
		//bank_acc_no='$bank_acc_no',  bank_b_name='$bank_b_name',bank_ifsc='$bank_ifsc' ,bank_acc_name='$bank_acc_name', 
		//shop_type_id='$shop_type_id', 		
		$proRow = $this->executeQuery($sql);
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Store']['msg'] = "Store updated successfully.";
		return json_encode($this->json_array);
	}
	function checkAttendance($id){
		$sql = "SELECT 
			count(sp_id) AS count, 	dayendtime
			FROM 
				`tbl_sp_attendance` 
			WHERE 
				sp_id = $id AND date_format(tdate, '%d-%m-%Y') = '".date('d-m-Y')."'";
			$resultAttendance = $this->executeQuery($sql);
			if (mysql_num_rows($resultAttendance) != 0) {
				$rowAttendance = mysql_fetch_assoc($resultAttendance);
				return $rowAttendance;
			}else{
				return 0;
			}
	}
	function addAttendance($id,$latt,$longg){
		$attendance_count = $this->checkAttendance($id);
		if($attendance_count['count'] == 0){
			$sql="INSERT INTO tbl_sp_attendance(sp_id, tdate,presenty,daystarttime,latt,longg) VALUES ( '".$id."',now(),'1',now(),'".$latt."','".$longg."' )";
			$proRow = $this->executeQuery($sql);
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day Start Time added successfully.";
		}else{
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day Start Time already exist.";
		}
		return json_encode($this->json_array);
	}
	function addDayEndTime($id,$latt,$longg){
		$attendance_count = $this->checkAttendance($id);
		if($attendance_count['count'] != 0 && $attendance_count['dayendtime'] == ''){
			$sql="UPDATE tbl_sp_attendance SET dayendtime = now(),endlatt='".$latt."',endlongg='".$longg."' WHERE sp_id = $id AND date_format(tdate, '%d-%m-%Y') = '".date('d-m-Y')."'";
			$proRow = $this->executeQuery($sql);
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day End Time added successfully.";			
		}else if($attendance_count['dayendtime'] != ''){
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Day End Time already exist.";
		}else{
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['msg'] = "Please add day start time.";
		}
		return json_encode($this->json_array);
	}
	public function getOrders($order_no){		
		
		$where_clause_outer ='';
		
		if($order_no != ''){
			$where_clause_outer .= " AND o.order_no = '".$order_no."' ";
		}
		
		 $order_sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status 
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE 1=1 $where_clause_outer
		ORDER BY shop_name, o.order_date DESC";//o.shop_order_status = ".$order_status."
		
		$orders_result = $this->executeQuery($order_sql);
		$row_orders_count = mysql_num_rows($orders_result);
		$i = 1;
		$all_orders = array();
		$row_orders_det_count = 0;
		if($row_orders_count > 0){
			while($row_orders = mysql_fetch_assoc($orders_result))
			{
				$all_orders[$i] = $row_orders;
				 $order_details_sql = "SELECT od.id, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
				odis.discount_amount
				FROM `tbl_order_details` AS od
				LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
				WHERE od.order_id = ".$row_orders['id']." ";//AND od.order_status = ".$order_status." 
				$orders_det_result = $this->executeQuery($order_details_sql);
				//$row_orders_det_count = mysqli_num_rows($orders_det_result);
				if(mysql_num_rows($orders_det_result) > 0)
				{
					$j = 1;
					while($row_orders_det = mysql_fetch_assoc($orders_det_result))
					{
						$all_orders[$i]['order_details'][$j] = $row_orders_det;
						$j++;
					}
					$i++;
				}
				else
				{
					unset ($all_orders[$i]); 
				}
			}
		}
		//print"<pre>";
		//print_r($all_orders);
		
		if(count($all_orders) == 0){
			$all_orders = array();
			return $all_orders;
		}
		else
			return $all_orders;
	}
	function getorderdetailsbyordernumber($ordernum){
	//echo "<pre>";print_r($ordernum);die();
	$this->json_array['status']['responsecode'] = '0';
	$this->json_array['status']['entity'] = '9';
	$result1 = $this->getOrders($ordernum);
		//echo "<pre>";print_r(count($result1));die();
		$ohistory_array_temp1=array();
		//$result1 = mysqli_query($con,$sql);
			if(count($result1)>0){                                  
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '9';
				foreach($result1 as $key=>$value){
					foreach($value['order_details'] as $key1=>$value1){
						$ohistory_array_temp['product_varient_id']= $value1['product_variant_id'];
						$ohistory_array_temp['distributorid']='';
						$ohistory_array_temp['distributornm']='';
						$ohistory_array_temp['salespersonnm']=$value['order_by_name'];
						
						$ohistory_array_temp['orderid']=$value['id'];
						$ohistory_array_temp['ordernum']=$value['order_no'];
						
						$ohistory_array_temp['variant_oder_id']='';
						$ohistory_array_temp['productname']=$value1['product_name']." ".$value1['product_variant_weight1']."-".$value1['product_variant_unit1'];
						if(!empty($value1['product_variant_weight2'])){
							$ohistory_array_temp['productname'].=" ".$value1['product_variant_weight2']."-".$value1['product_variant_unit2'];
						}
						
						$ohistory_array_temp['order_date']=date('d-m-Y H:i:s',strtotime($value['order_date']));
						$ohistory_array_temp['unitprice']=number_format($value1['product_unit_cost'],2, '.', '');
						$ohistory_array_temp['quantity']=$value1['product_quantity'];
						$total_cost=$ohistory_array_temp['unitprice']*$ohistory_array_temp['quantity'];
						$ohistory_array_temp['totalcost']=number_format($total_cost,2, '.', '');
						$ohistory_array_temp['shop_id']=$value['shop_id'];
						$ohistory_array_temp['shopnm']=$value['shop_name'];
						
						$ohistory_array_temp['catnm']=$value1['cat_name'];
						$ohistory_array_temp['brandnm']=$value1['brand_name'];
						
						$ohistory_array_temp['totalproductcount']='';
						$ohistory_array_temp1[] = $ohistory_array_temp;
					}
					
				}
				if(!empty($ohistory_array_temp1))                                                                         
				$this->json_array['data']['orderdetails']=$ohistory_array_temp1;
											
			}else{
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['ohistory'][]="No records found";
		}
		return json_encode($this->json_array);
	}
	function get_distributorlist_by_ssid($ssid) {  
			$sqlstockist = "SELECT 
			`id`, 	 
			`firstname`	 	
			FROM 
				`tbl_user` 
			WHERE 
				user_type = 'Distributor' AND external_id =$ssid and isdeleted!='1' ";
			$proRowstockist = $this->executeQuery($sqlstockist);
			$this->json_array['status']['responsecode'] = '0';
            $this->json_array['status']['entity'] = '9';
			while($rowstockist = mysql_fetch_array($proRowstockist)) {
				$c_array_temp['id']	= $rowstockist['id'];
				$c_array_temp['firstname'] 	= $rowstockist['firstname'];
				$this->json_array['data']['distributor'][]=$c_array_temp;
			}
		return json_encode($this->json_array);
	}
	function get_mode_of_transe(){
		$sqlcity = "SELECT id, van_type FROM  tbl_mode_transe";
	  $proRowcity = $this->executeQuery($sqlcity);
	  $this->json_array['status']['responsecode'] = '0';
	  $this->json_array['status']['entity'] = '3';
	   while($rowcity = mysql_fetch_array($proRowcity)){
		  $c_array_temp['id'] = $rowcity['id'];
		  $c_array_temp['van_type'] = $rowcity['van_type'];
		  $this->json_array['data']['mot'][]=$c_array_temp;
	  }
	  return json_encode($this->json_array);
	}
	//Sales person pause resume status
	function get_sp_pause_resume($userid, $user_type){
		$sql = "SELECT * from tbl_sp_attendance WHERE sp_id='".$userid ."' 
		AND DATE_FORMAT(tdate,'%Y-%m-%d')=DATE(NOW())";
		$proRow = $this->executeQuery($sql);
		$c_array_temp=array();
		while($rowc = mysql_fetch_array($proRow)) {
			$c_array_temp['dayendtime']   = $rowc['dayendtime'];
			$c_array_temp['daystarttime'] = $rowc['daystarttime'];			
		}
		if (mysql_num_rows($proRow) == 0||$c_array_temp['dayendtime']!== NULL) {
				$this->json_array['data']['flag'] = "2";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				return json_encode($this->json_array);
		}else{
			if($user_type == 'SalesPerson' || $user_type == ''){
				$sql="SELECT salesperson_id, shop_visit_date_time, flag, comments, is_shop_location 
				FROM tbl_shop_visit where salesperson_id='".$userid."' and
				is_shop_location = '1' 
				and date_format(shop_visit_date_time, '%d-%m-%Y') = '".date('d-m-Y')."'";
				$proRow = $this->executeQuery($sql);
				$count_resume=mysql_num_rows($proRow);
				$newcount=$count_resume%2;
				$this->json_array['data']['flag'] = (string)$newcount;
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
			}else if($user_type == 'DeliveryPerson'){
				$sql="SELECT `transport_off_id`, `deliveryperson_id`, `transport_off_visit_date_time`, `flag`
				FROM tbl_transport_off_visit where deliveryperson_id='".$userid."'
				and date_format(transport_off_visit_date_time, '%d-%m-%Y') = '".date('d-m-Y')."'";
				$proRow = $this->executeQuery($sql);
				$count_resume=mysql_num_rows($proRow);
				$newcount=$count_resume%2;
				$this->json_array['data']['flag'] = (string)$newcount;
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
			}
			return json_encode($this->json_array);
		}
	}
	function get_shop_type(){
		$sql = "SELECT * FROM tbl_shop_type WHERE status='0'";
		$proRow = $this->executeQuery($sql);
		$shop_type_array=array();
		if (mysql_num_rows($proRow) == 0) {
				$this->json_array['data']['flag'] = "2";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				return json_encode($this->json_array);
		}else{
			while($rowshoptype = mysql_fetch_array($proRow)) {
				$shop_type_array['id']				= $rowshoptype['id'];
				$shop_type_array['shop_type'] 		= $rowshoptype['shop_type'];
				$this->json_array['data']['shop_type'][]=$shop_type_array;
			}
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			return json_encode($this->json_array);
		}
	}
	function get_transport_offices(){
		$sql = "SELECT `id`, `trans_off_name`, `address`, `state_id`, `city_id`, `suburb_id`,
		(SELECT transport_name FROM tbl_transport_type WHERE id = transport_type) AS transport_name
		FROM tbl_transport_offices WHERE status='Active' AND isdeleted !=1";
		$proRow = $this->executeQuery($sql);
		$array=array();
		if (mysql_num_rows($proRow) == 0) {
				$this->json_array['data']['flag'] = "2";
				$this->json_array['status']['responsecode'] = '0';
				$this->json_array['status']['entity'] = '1';
				return json_encode($this->json_array);
		}else{
			while($rowsoffice = mysql_fetch_array($proRow)) {
				$array['id']					= $rowsoffice['id'];
				$array['trans_off_name'] 		= $rowsoffice['trans_off_name'];
				$array['transport_name'] 		= $rowsoffice['transport_name'];
				$array['address'] 				= $rowsoffice['address'];
				$array['state_id'] 				= $rowsoffice['state_id'];
				$array['city_id'] 				= $rowsoffice['city_id'];
				$array['suburb_id'] 			= $rowsoffice['suburb_id'];
				$this->json_array['data']['transport_offices'][]=$array;
			}
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '1';
			return json_encode($this->json_array);
		}
	}
	//original get order data old
	function get_orderdata_old_new($userid,$pageid){
		$npageid=$pageid*10;
		if($pageid=='1')
			$limit=0;
		else{
			$limit=($pageid-1)*10;
		} 
		 $sql = "select O.shop_id, O.order_date,  O.order_no, OD.product_variant_weight1,
		OD.product_variant_unit1, OD.product_variant_weight2, OD.product_variant_unit2,
		SUM(OD.product_total_cost) as totalcost, SUM(OD.p_cost_cgst_sgst) as gst_totalcost,
		SUM(OD.product_quantity) as total_quantity, 
		IF(O.offer_provided = 0,'Offer Applied','Offer Not Applied') AS offer_provided_val,
		(SELECT name FROM  tbl_shops WHERE id = O.shop_id) AS shop_name
		FROM `tbl_order_details` OD
		LEFT JOIN tbl_orders O on O.id=OD.order_id
		WHERE 
		ordered_by='".$userid."' AND DATE_FORMAT(O.order_date,'%Y-%m-%d')=DATE(NOW())
		GROUP BY OD.order_id limit $limit,$npageid ";
		$proRow = $this->executeQuery($sql);
		if($rowcount = mysql_num_rows($proRow)>0){
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '9';
			while($row = mysql_fetch_array($proRow)){
				$tcost = $row['totalcost']; 				
				$free_quantity = 0;
				$sale_quantity = $row['total_quantity'];
				$ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', '');//$rowtc['tcost'];
				$ohistory_array_temp['free_quantity'] = $free_quantity;
				$ohistory_array_temp['sale_quantity'] = $sale_quantity;
				$ohistory_array_temp['quantity'] = $row['total_quantity'];
				$ohistory_array_temp['orderid'] = $row['order_no'];
				$ohistory_array_temp['order_date'] = $row['order_date'];
				$ohistory_array_temp['shopid'] = $row['shop_id'];
				$ohistory_array_temp['shopnme'] = $row['shop_name'];
				$ohistory_array_temp['offer_applied'] = $row['offer_provided_val'];
				$this->json_array['data']['ohistory'][]=$ohistory_array_temp;
			}
		}
		else{
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';
			$this->json_array['data']['ohistory'][]="No records found";
		}
		return json_encode($this->json_array);
	}
	function get_orderdata($userid,$pageid){
		$npageid=$pageid*10;
		if($pageid=='1')
			$limit=0;
		else{
			$limit=($pageid-1)*10;
		} 
		  $sql = "select shops.name as shopname,OA.shop_id, OA.order_date,  
						OA.order_no, VO.product_variant_weight1,
						VO.product_variant_unit1,
						SUM(VO.p_cost_cgst_sgst) as totalcost,
						count(VO.order_id) as variantunit, 
		IF(OA.offer_provided = 0,'Offer Applied','Offer Not Applied') AS offer_provided_val, VO.cat_id

		FROM `tbl_order_details` VO
		LEFT JOIN tbl_orders OA on OA.id=VO.order_id
		LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
		WHERE 
		OA.ordered_by='".$userid."' AND DATE_FORMAT(OA.order_date,'%Y-%m-%d')=DATE(NOW())
		GROUP BY VO.order_id limit $limit,$npageid ";//
		$proRow = $this->executeQuery($sql);

		if($rowcount = mysql_num_rows($proRow)>0){
			$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '9';
		while($row = mysql_fetch_array($proRow)){
			$tcost=$row['totalcost']; 
	 
				$sql_free_pro = "SELECT COUNT(tbl_variant_order.id) as quantity_free FROM `tbl_variant_order` INNER JOIN `tbl_order_app` ON tbl_order_app.id = tbl_variant_order.orderappid WHERE campaign_sale_type='free' AND OA.cat_id = ".$row['cat_id'];
				$result_free_pro = mysqli_query($con,$sql_free_pro);
				$row_free_pro = mysqli_fetch_assoc($result_free_pro);
				$free_quantity = 0;
				$sale_quantity = $row['variantunit'];
				if($row_free_pro['quantity_free'] > 0)
				{
					$free_quantity = $row_free_pro['quantity_free'];
					$sale_quantity = $row['variantunit'] - $free_quantity;
				}
		$ohistory_array_temp['tcost'] = number_format($tcost, 2, '.', '');//$rowtc['tcost'];
		$ohistory_array_temp['free_quantity'] = $free_quantity;
		$ohistory_array_temp['sale_quantity'] = $sale_quantity;
		$ohistory_array_temp['quantity'] = $row['variantunit'];
		$ohistory_array_temp['orderid'] = $row['order_no'];
		$ohistory_array_temp['order_date'] = $row['order_date'];
		$ohistory_array_temp['shopid'] = $row['shop_id'];
		$ohistory_array_temp['shopnme'] = $row['shopname'];//$rowh['shopnme'];
		$ohistory_array_temp['offer_applied'] = $row['offer_provided_val'];
		$this->json_array['data']['ohistory'][]=$ohistory_array_temp;
		}
		}
		else{
			$this->json_array['status']['responsecode'] = '1';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['ohistory'][]="No records found";
		}
		return json_encode($this->json_array);
	}
	
	function get_order_and_orderdetails($userid, $order_status){
		  $order_sql = "SELECT o.id as oid, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status 
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE o.shop_order_status = '1' and  o.delivery_person_id=".$userid."
		ORDER BY shop_name, o.order_date DESC";
		
		$orders_result = $this->executeQuery($order_sql);
		$row_orders_count = mysql_num_rows($orders_result);
		//$i = 1;
		$all_orders = array();
		$row_orders_det_count = 0;
		if($row_orders_count > 0){
			while($row_orders = mysql_fetch_assoc($orders_result))
			{
				//$all_orders[$i] = $row_orders;
				   $order_details_sql = "SELECT od.id as odid, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date 
				FROM `tbl_order_details` AS od
				WHERE od.order_id = ".$row_orders['oid']." AND od.order_status = '".$order_status."'";
				//die();
				
				$orders_det_result = $this->executeQuery($order_details_sql);
				$row_orders_det_count = $row_orders_det_count+ mysql_num_rows($orders_det_result);
				//echo $row_orders_det_count ;
				if(mysql_num_rows($orders_det_result) > 0){
					//$j = 1;
					while($row_orders_det = mysql_fetch_assoc($orders_det_result))
					{
						$newarr[]=array_merge ($row_orders_det, $row_orders);
						//$all_orders[$i]['order_details'][] = $newarr;
					}
				}
			}
		}
		//echo $row_orders_det_count ;
		//echo "<pre>";print_r($newarr);
		 foreach ($newarr as $key => $value) {			
			foreach ($value as $key1 => $value1) {
				if (is_null($value1)) {
					 $newarr[$key][$key1] = "";
				}
			}
		} 
		
		
		if($row_orders_det_count == 0){
			$all_orders = array();
			$this->json_array['status']['responsecode'] = '1';
			$this->json_array['status']['entity'] = '1';	
			$this->json_array['data']['orderdetails'][]="No records";
		}
		else{
			$this->json_array['status']['responsecode'] = '0';
			$this->json_array['status']['entity'] = '9';
			$this->json_array['data']['orderdetails']=$newarr;
		}
		return json_encode($this->json_array);
		
	}
	function save_delivery_to_trnaseoffice($data) {
		foreach($data->order_details as $key=>$value){
			//echo "<pre>";print_r($value1);
			
			$sql="update  tbl_order_details set 
							trans_office_id='".$data->transport_office_id."', 
							challan_no='".$data->challan_no."', 
							vehicle_no='".$data->vehicle_no."',
							transport_mode='".$data->transport_mode."',
							place_of_supply='".$data->place_of_supply."',
							order_status = '3',
							transport_date=now()
							where id='".$value->odid."'";
			//echo "<pre>";print_r($sql);die();
			$proRow = $this->executeQuery($sql);
		}
		
		$this->json_array['status']['responsecode'] = '0';
		$this->json_array['status']['entity'] = '1';
		$this->json_array['data']['Delivery']['msg'] = "Delivery assigned to transport office successfully.";
		return json_encode($this->json_array);
	}	
	function get_region_list($userid)
	{		
		 $sql_assigned_region = "SELECT GROUP_CONCAT(suburb_ids) AS assigned_regions		
							FROM tbl_user_working_area 		
							WHERE suburb_ids != '' AND 	
		user_id IN (SELECT id FROM tbl_user WHERE user_type = 'salesperson' AND isdeleted != 1 AND id !='$userid')";		
		$proRow = $this->executeQuery($sql_assigned_region);		
		$row_assigned_region = mysql_fetch_assoc($proRow);		
		if($row_assigned_region['assigned_regions'] != '')			
			$sql = "SELECT id,suburbnm FROM tbl_surb WHERE id NOT IN (".$row_assigned_region['assigned_regions'].") AND isdeleted!='1'";		
		else			
			$sql = "SELECT id,suburbnm FROM tbl_surb WHERE isdeleted!='1'";						
		$proRowSuburb = $this->executeQuery($sql);			
		$this->json_array['status']['responsecode'] = '0';		
		$this->json_array['status']['entity'] = '2';		
		while($row = mysql_fetch_array($proRowSuburb)){			
			$c_array_temp['suburbnm'] = $row['suburbnm'];			
			$c_array_temp['id'] = $row['id'];			
			$this->json_array['data']['suburb'][]=$c_array_temp;		
		}		
		return json_encode($this->json_array);	
	}
	
}