<?php
/***********************************************************
 * File Name	: productManage.php
 ************************************************************/	
include_once "../includes/commonManage.php";	
class productManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);		
	}
	public function deleteBrand($brand_id) {
		$deleted_on = date("Y-m-d H:i:s");
			
		$update_sql="UPDATE  tbl_brand SET isdeleted=1,deleted_on='".$deleted_on."' where id='$brand_id'";		
		mysqli_query($this->local_connection,$update_sql);	
		$this->commonObj->log_delete_record('tbl_brand',$brand_id,$update_sql);
		$this->deleteCategory($brand_id);
		$record['brand_id'] = $brand_id;
		//$this->deleteCampaign($record);
	}
	public function deleteCategory($brand_id, $cat_id=null) {
		$deleted_on = date("Y-m-d H:i:s");
		$where_clause = "";
		if($cat_id != ''){
			$where_clause = " AND id = ".$cat_id;
		}			
		$update_sql="UPDATE tbl_category SET isdeleted=1,deleted_on='".$deleted_on."' where brandid='$brand_id'".$where_clause;		
		mysqli_query($this->local_connection,$update_sql);
		$this->commonObj->log_delete_record('tbl_category',$brand_id,$update_sql);
		if($cat_id != ''){
			$this->deleteProduct($cat_id);
			$record['cat_id'] = $cat_id;			
			//$this->deleteCampaign($record);
		}else{
			$sql_cat = "SELECT GROUP_CONCAT(id) AS cat_ids FROM tbl_category WHERE brandid='$brand_id'";
			$result = mysqli_query($this->local_connection,$sql_cat);
			$row_count = mysqli_num_rows($result);
			if($row_count > 0){					
				$row = mysqli_fetch_assoc($result);	
				
				if(isset($row['cat_ids']) && $row['cat_ids'] !=''){
					$this->deleteProduct($row['cat_ids']);
					$record['cat_id'] = $cat_id;
					//$this->deleteCampaign($record);
				}
			}
		}
	}
	public function deleteProduct($cat_id, $product_id=null) {
		$deleted_on = date("Y-m-d H:i:s");
		$where_clause = "";
		if($product_id != ''){
			$where_clause = " AND id = ".$product_id;
		}			
		$update_sql="UPDATE tbl_product SET isdeleted=1,deleted_on='".$deleted_on."' where catid IN ($cat_id) ".$where_clause;		
		mysqli_query($this->local_connection,$update_sql);
		$this->commonObj->log_delete_record('tbl_product',$cat_id,$update_sql);
		$record['cat_id'] = $cat_id;
		//$this->deleteCampaign($record);
	}
	public function deleteCampaign($record) {
		$deleted_on = date("Y-m-d H:i:s");
		if($record['brand_id']!=''){
			$sql_camp = "SELECT GROUP_CONCAT(campaign_id) AS campaign_id FROM tbl_campaign_product WHERE  (c_p_brand_id='".$record['brand_id']."' OR f_p_brand_id='".$record['brand_id']."' )";
			$result = mysqli_query($this->local_connection,$sql_camp);
			$row_count = mysqli_num_rows($result);
			if($row_count > 0){					
				$row = mysqli_fetch_assoc($result);	
				$id = $row['campaign_id'];
			}	
				
		}elseif($record['cat_id']!=''){
			$sql_camp = "SELECT GROUP_CONCAT(campaign_id) AS campaign_id FROM tbl_campaign_product WHERE  (c_p_category_id IN (".$record['cat_id'].") OR f_p_category_id IN (".$record['cat_id'].") )";
			$result = mysqli_query($this->local_connection,$sql_camp);
			$row_count = mysqli_num_rows($result);
			if($row_count > 0){					
				$row = mysqli_fetch_assoc($result);	
				$id = $row['campaign_id'];
			}
		}
		if(isset($id) && $id != ''){
			$update_sql="UPDATE tbl_campaign SET isdeleted=1,deleted_on='".$deleted_on."' where id IN ($id) ";
			mysqli_query($this->local_connection,$update_sql);
			$this->commonObj->log_delete_record('tbl_campaign',$id,$update_sql);
		}
	}
	public function getAllBrands(){
		$sql1="SELECT `id`, `name`, `description`, `type`, `isdeleted`, `deleted_on` 
		FROM `tbl_brand`
		ORDER BY name ASC";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getAllCategory(){
		$sql1="SELECT `id`, `brandid`, `categorynm`, `categoryimage`, `isdeleted`, `deleted_on` 
		FROM `tbl_category` 
		ORDER BY categorynm ASC";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}
	public function getAllProducts(){
		$sql1="SELECT `id`, `catid`, `productname`, `isdeleted`, `deleted_on`
		FROM tbl_product 
		ORDER BY productname ASC";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;	
	}	
	public function check_product_available(){
		$sql = "SELECT count(p.id) AS pcount, 
		GROUP_CONCAT(p.catid) AS cat_id, 
		GROUP_CONCAT(b.id) AS brand_id 
		FROM tbl_product AS p, tbl_category AS c, tbl_brand AS b 
		WHERE p.catid = c.id AND c.brandid = b.id 
		AND p.isdeleted != 1 AND c.isdeleted != 1 AND b.isdeleted != 1 ";
		$result = mysqli_query($this->local_connection,$sql);
		$row = mysqli_fetch_assoc($result);	//print_r($row);
		if($row['pcount'] == 1){
			echo $row['pcount']."####".$row['cat_id']."####".$row['brand_id'];
		}else if($row['pcount'] > 1){
			echo "more_than_one_product";
		}else{
			echo "no_product";
		}
	} 
}
?>