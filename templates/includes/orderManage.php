<?php
/***********************************************************
 * File Name	: orderManage.php
 ************************************************************/	
class orderManage
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
	}
	public function getReportTitle($frmdate=null,$todate=null){
		extract($_POST);
		$report_title = '';
		switch($report_type){
			case "superstockist":
					$report_title = 'Super Stockist';
				break;
			case "stockist":
					$report_title = 'Stockist';
				break;
			case "salesperson":	
					//$report_title = 'Sales Person';
					$report_title = 'Sales Person';
				break;
			case "shop":	
					$report_title = 'Shops';
				break;
			case "product":	
					$report_title = 'Products';
				break;
		}
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	//title for shop
	public function getReportTitleForShop($frmdate=null,$todate=null){
		extract($_POST);
		$report_title = 'Sale Report For Shop : ';
		$sql="SELECT id , name FROM tbl_shops WHERE id= '$selshop' ";
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$report_title.= $row['name'];
			}
		}else{
			$report_title ='No shop found';
		}
		
		return $report_title;
	}
	
	//new method for sp sales report productwise //ganeshfoods
	public function getAllOrders1() {
		
		extract($_POST);
		$selprod= $_POST['selprod'];
		$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(OA.order_date) = yearweek(curdate())";
					//$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(OA.order_date, '%m')=date_format(now(), '%m')";
					//$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(OA.order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				//	$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(OA.order_date, '%d-%m-%Y') = '".$date."' ";
				//	$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		$where="";
		if($selprod!=''){
			$where= " AND VO.product_id = $selprod ";
		}
		if($user_type=='Admin'){
			$external_id = '';
		}
		
		
		if($external_id != '')
		{
			$where = " AND salesman.external_id IN (". $external_id.")";
		}
		 $sql = "SELECT
					salesman.id,	
					salesman.firstname,
					sum(VO.product_quantity) as totalunit, 
					sum(p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user salesman 
				LEFT JOIN tbl_orders OA ON OA.ordered_by = salesman.id
				LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id
				LEFT JOIN tbl_product TP ON VO.product_id = TP.id				
				WHERE salesman.user_type='salesperson'
					$where $condnsearch 
					 group by salesman.id ";	//date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //tbl_orders
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	//new method for shop sales report  //ganeshfoods
	public function getAllOrders2() {
		extract($_POST);
		$selshop= $_POST['selshop'];
		
		$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$luser_id = $_SESSION[SESSION_PREFIX.'user_id'];
		$where="";		
		if($user_type=='Superstockist'){
			$where=" AND OA.superstockistid ='$luser_id' ";
		}
		if($user_type=='Distributor'){
			$where=" AND OA.distributorid='$luser_id' ";
		}
		
		 $sql = "SELECT					
					TP.id as id,	
					TP.productname as productname,
					date_format(OA.order_date, '%M') as month,
					VO.product_variant_weight1,
					VO.product_variant_unit1 as unit, 
					VO.product_quantity as variantunit, 
					VO.product_unit_cost as unitcost,
					VO.product_variant_id
				FROM tbl_shops AS TS
				LEFT JOIN tbl_orders OA ON OA.shop_id = TS.id							
				LEFT JOIN tbl_order_details VO ON OA.id = VO.order_id
				LEFT JOIN tbl_product TP ON VO.product_id = TP.id 
				WHERE OA.shop_id = $selshop
				$where ORDER BY `TP`.`productname`";	//date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //FROM tbl_product TP 
			
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	
		public function getAllOrders() {
		extract($_POST);
		//print_r($_POST);	
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($report_type){
			case "superstockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							 $sql="SELECT u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' $search_name
							GROUP BY u.firstname  ".$order_by.$limit;
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.superstockistid = u.id ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Superstockist' AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name
							GROUP BY u.firstname ".$order_by.$limit;
						break;
						 
						break;
					}
				break;
			case "stockist":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.distributorid = u.id ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Superstockist":						
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.distributorid = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='Distributor' AND u.external_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $search_name 
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						 
						break;
					}
				break;
			case "salesperson":	
					if($search != '')
						$search_name = " AND u.firstname LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id, u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.ordered_by = u.id ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE u.user_type='SalesPerson' $search_name GROUP BY u.firstname ".$order_by.$limit;	
							
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv WHERE u.id = sv.salesperson_id $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.ordered_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (  '".$_SESSION[SESSION_PREFIX.'user_id']."') OR u.external_id IN (SELECT id FROM tbl_user where external_id='".$_SESSION[SESSION_PREFIX.'user_id']."') ) 
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_orders oa 
							LEFT JOIN tbl_user u  ON oa.ordered_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY u.firstname ASC ';
							$sql="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv, tbl_shops AS ss WHERE ss.id=sv.shop_id AND u.id = sv.salesperson_id AND (ss.stockist_id = ".$_SESSION[SESSION_PREFIX.'user_id'].") $condnsearch_sp) AS shop_no_order
							FROM tbl_user u 
							LEFT JOIN tbl_orders oa ON oa.ordered_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name AND (u.external_id IN (".$_SESSION[SESSION_PREFIX.'user_id'].")  OR external_id LIKE ('%,". $_SESSION[SESSION_PREFIX.'user_id']."%'))
							GROUP BY u.firstname ".$order_by.$limit;	
							$sql2="SELECT u.id as sp_id,  u.firstname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales,
							count(DISTINCT oa.shop_id) AS shop_order,
							(select count(shop_id) from tbl_shop_visit AS sv INNER JOIN tbl_shops as s ON s.id = sv.shop_id WHERE u.id = sv.salesperson_id AND s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' $condnsearch_sp) AS shop_no_order
							FROM tbl_orders oa 
							LEFT JOIN tbl_user u  ON oa.ordered_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE u.user_type='SalesPerson' $search_name   AND u.external_id !=  '".$_SESSION[SESSION_PREFIX.'user_id']."'  
							GROUP BY u.firstname ".$order_by.$limit;	
						break;
					}
				break;
			case "shop":	
					if($search != '')
						$search_name = " AND s.name LIKE '%".$search."%'";
					switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":						
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_orders oa ON shop_id = s.id
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) ".$condnsearch." 
							WHERE s.id!=0 AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Superstockist":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_orders oa ON shop_id = s.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
							WHERE s.id!=0 AND s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !=''  $search_name 
							GROUP BY s.name ".$order_by.$limit;	
						break;
						case "Distributor":							
							if($order_by == '')
								$order_by = ' ORDER BY s.name ASC ';
							$sql="SELECT s.name as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_shops AS s
							LEFT JOIN tbl_orders oa ON shop_id = s.id  AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
							WHERE s.id!=0  AND  s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."' AND s.name !='' $search_name 
							GROUP BY s.name ".$order_by.$limit;	
							
						break;
					}	
				break;
			case "product":	
					if($search != '')
						$search_name = " AND p.productname LIKE '%".$search."%'";
				switch($_SESSION[SESSION_PREFIX.'user_type']){
						case "Admin":		
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							/*$sql="SELECT p.productname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_orders oa ON oa.catid   = p.catid  ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))  AND vo.productid = p.id 
							WHERE p.id!=0 AND p.productname !=''  $search_name 
							GROUP BY p.productname ".$order_by.$limit;	*/

							$sql="SELECT p.productname as Name,
	                        sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
                            LEFT JOIN tbl_order_details vo ON vo.product_id = p.id 
							LEFT JOIN tbl_orders oa ON oa.id   = vo.Order_id 
							WHERE p.id!=0 AND p.productname !='' ".$condnsearch." $search_name 
							GROUP BY p.productname ".$order_by.$limit;



						break;
						case "Superstockist":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							/*$sql="SELECT p.productname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_orders oa ON oa.catid   = p.catid AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name 
							GROUP BY p.productname ".$order_by.$limit;	*/
							$sql="SELECT p.productname as Name,
	                        sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
                            LEFT JOIN tbl_order_details vo ON vo.product_id = p.id 
							LEFT JOIN tbl_orders oa ON oa.id   = vo.Order_id AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'
							WHERE p.id!=0 AND p.productname !='' ".$condnsearch." $search_name 
							GROUP BY p.productname ".$order_by.$limit;	
						break;
						case "Distributor":	
							if($order_by == '')
								$order_by = ' ORDER BY p.productname ASC ';
							/*$sql="SELECT p.productname as Name,sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
							LEFT JOIN tbl_orders oa ON oa.catid   = p.catid AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."'  ".$condnsearch." 
							LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) AND vo.productid = p.id
							WHERE p.id!=0 AND p.productname !='' $search_name  
							GROUP BY p.productname ".$order_by.$limit;	*/
							$sql="SELECT p.productname as Name,
	                        sum(vo.p_cost_cgst_sgst) as Total_Sales 
							FROM tbl_product AS p
                            LEFT JOIN tbl_order_details vo ON vo.product_id = p.id 
							LEFT JOIN tbl_orders oa ON oa.id   = vo.Order_id AND  oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."'
							WHERE p.id!=0 AND p.productname !='' ".$condnsearch." $search_name 
							GROUP BY p.productname ".$order_by.$limit;						
						break;
					}	
				break;
			default:
				break;
		}
		
		
		
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			if($sql2 != ''){
				$result2 = mysqli_query($this->local_connection,$sql2);
				$j = 1;
				$row_count2 = mysqli_num_rows($result2);
				if($row_count2 > 0){
					$name = array_column($records,'Name') ;
					
					while($row2 = mysqli_fetch_assoc($result2))
					{
						if(!in_array($row2['Name'],$name))
						{
							$records2[$j] = $row2;
							$j++;
						}
					}				
				}else{
					$records2 =0;
				}
			}
			if($records2 != 0)
			{
				$records = array_merge($records,$records2);				
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function getReportTitleForSP($selTest=null,$frmdate=null,$todate=null){	
		$report_title = '';		
		switch($selTest){
				case 1:
					$monday = strtotime("last sunday");
					$monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
					 
					$sunday = strtotime(date("d-m-Y",$monday)." +6 days");
					 
					$this_week_sd = date("d-m-Y",$monday);
					$this_week_ed = date("d-m-Y",$sunday);
					$date=" Weekly (". $this_week_sd." TO ".$this_week_ed.") Report";
				break;
				case 2:
					$date=" Current Month (".date('F').") Report";
				break;
				case 3:	
					$frmdate = date('d-m-Y',$frmdate);
					$todate = date('d-m-Y',$todate);
					$date=" Report During ($frmdate To $todate)";
				break;
				case 4:
					$date=" Today's (".date('d-m-Y').") Report";
				break;				
				case 5:
					$date="";
				break;
			}
		$report_title = $report_title.$date;
		return $report_title;
	}
	function getSPShopOrdersSummary($sp_id, $filter_date,$date1=null,$date2=null){

		if($filter_date!=0){

			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				 $sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user u 				
				LEFT JOIN tbl_orders oa ON oa.ordered_by = u.id ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale'))
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Superstockist":	
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user u 
				LEFT JOIN tbl_orders oa ON oa.ordered_by = u.id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND vo.status != 1 AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
			case "Distributor":		
				$sql="SELECT s.name as shop_name,s.id as shop_id,sum(vo.p_cost_cgst_sgst) as Total_Sales
				FROM tbl_user u  
				LEFT JOIN tbl_orders oa ON oa.ordered_by = u.id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch."
				LEFT JOIN tbl_shops AS s ON s.id = oa.shop_id
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id AND (vo.campaign_sale_type = '' OR vo.campaign_sale_type IS NULL OR vo.campaign_sale_type = 'sale')) 
				WHERE u.id = $sp_id GROUP BY s.name ORDER BY s.name";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	function getSPShopOrders($sp_id, $s_id, $filter_date,$date1=null,$date2=null){
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch=" AND yearweek(order_date) = yearweek(curdate())";					
				break;
				case 2:
					$condnsearch=" AND date_format(order_date, '%m')=date_format(now(), '%m')";					
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(order_date, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(order_date, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					$condnsearch=" AND date_format(order_date, '%d-%m-%Y') = '".$date."' ";					
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
				 $sql="SELECT b.name AS brandnm, c.categorynm, vo.product_variant_id, vo.product_quantity, vo.p_cost_cgst_sgst, vo.id as variant_oder_id, oa.order_date,vo.product_id, vo.campaign_sale_type,vo.product_variant_weight1,vo.product_variant_unit1,p.productname
				FROM tbl_orders oa 				
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id)
                LEFT JOIN tbl_category c ON (c.id = vo.cat_id)
                LEFT JOIN tbl_brand b ON (b.id = vo.brand_id)
                LEFT JOIN tbl_product p ON (p.id = vo.product_id)
				WHERE oa.ordered_by = $sp_id AND oa.shop_id = $s_id ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Superstockist":
				$sql="SELECT b.name AS brandnm, c.categorynm, vo.product_variant_id, vo.product_quantity, vo.p_cost_cgst_sgst, vo.id as variant_oder_id, oa.order_date,vo.product_id, vo.campaign_sale_type,vo.product_variant_weight1,vo.product_variant_unit1,p.productname
				FROM tbl_orders oa 				
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id)
                LEFT JOIN tbl_category c ON (c.id = vo.cat_id)
                LEFT JOIN tbl_brand b ON (b.id = vo.brand_id)
                LEFT JOIN tbl_product p ON (p.id = vo.product_id)
				WHERE oa.ordered_by = $sp_id AND vo.status != 1 AND oa.shop_id = $s_id AND oa.superstockistid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
			case "Distributor":	
				$sql="SELECT b.name AS brandnm, c.categorynm, vo.product_variant_id, vo.product_quantity, vo.p_cost_cgst_sgst, vo.id as variant_oder_id, oa.order_date,vo.product_id, vo.campaign_sale_type,vo.product_variant_weight1,vo.product_variant_unit1,p.productname
				FROM tbl_orders oa 				
				LEFT JOIN tbl_order_details vo ON (oa.id = vo.order_id)
                LEFT JOIN tbl_category c ON (c.id = vo.cat_id)
                LEFT JOIN tbl_brand b ON (b.id = vo.brand_id)
                LEFT JOIN tbl_product p ON (p.id = vo.product_id)
				WHERE oa.ordered_by = $sp_id AND oa.shop_id = $s_id AND oa.distributorid='".$_SESSION[SESSION_PREFIX.'user_id']."' ".$condnsearch." ORDER BY vo.id DESC";
			break;
		}
		$result1 = mysqli_query($this->local_connection,$sql);
		$i = 1;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){
			while($row = mysqli_fetch_array($result1))
			{
				$sql_product = "SELECT  productname FROM `tbl_product` WHERE id = '".$row['productid'] ."'";										
				$product_result = mysqli_query($this->local_connection,$sql_product);
				$obj_product = mysqli_fetch_object($product_result);
				$product_variant_id = $row['product_variant_id'];
				$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_variant_id' ";
				$resultprd = mysqli_query($this->local_connection,$sqlprd);
				$rowprd = mysqli_fetch_array($resultprd);
				$exp_variant1 = $rowprd['variant_1'];
				$imp_variant1= explode(',',$exp_variant1);
				$exp_variant2 = $rowprd['variant_2']; $imp_variant2= explode(',',$exp_variant2);
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit1 = $rowunit['unitname'];
				$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
				$resultunit = mysqli_query($this->local_connection,$sql);
				$rowunit = mysqli_fetch_array($resultunit);
				$variant_unit2 = $rowunit['unitname'];
				$dimentionDetails = "";
				if($variant_unit1!="") {
					$dimentionDetails = " - ".$imp_variant1[0] . " " . $variant_unit1;//" .  $imp_variant1[0] . " 
				}
				//$dimentionDetails .= " - Weight: " .  $imp_variant2[0] . " " . $variant_unit2;
				$row['prodname_variant'] = $obj_product->productname . $dimentionDetails;
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}else{
			return 0;
		}
	}
	public function no_order_history_details($sp_id=null,$filter_date=null,$date1=null,$date2=null) {
		if($filter_date!=0){
			switch($filter_date){
				case 1:
					$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					$frmdate = date('d-m-Y',$date1);
					$todate = date('d-m-Y',$date2);
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');					
					$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;				
				default:
					$condnsearch_sp="";
				break;

			}
		}
		if($sp_id!=''){
			$where = " AND salesperson_id = $sp_id";
		}
		switch($_SESSION[SESSION_PREFIX.'user_type']){
			case "Admin":	
					$sql1="SELECT `id`, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit 
					WHERE shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Superstockist":
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.sstockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
			case "Distributor":	
					$sql1="SELECT tbl_shop_visit.id, `shop_id`, `salesperson_id`, `shop_visit_date_time`, `shop_visit_reason`, `shop_close_reason_type`, `shop_close_reason_details`
					FROM tbl_shop_visit INNER JOIN tbl_shops as s ON s.id = shop_id 
					WHERE s.stockist_id = '".$_SESSION[SESSION_PREFIX.'user_id']."'
					AND shop_id != 0 AND salesperson_id !=0 $where
					$condnsearch_sp
					ORDER BY id DESC";
			break;
		}
		//echo $sql1; exit;
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}
	function getSalesPOrders_old($date,$salespid){
		 $sql = "SELECT	DISTINCT(VO.orderid), OA.shop_id, shops.name as shopname, shops.address, shops.mobile,
				OA.order_date , OA.categorynm, oplacelat, oplacelon, VO.orderappid,
				(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
				(SELECT name FROM tbl_state WHERE id = shops.state) AS statename
				FROM tbl_order_app OA 
				LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
				LEFT JOIN tbl_variant_order VO ON VO.orderappid= OA.id				
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				GROUP BY VO.orderid,
				OA.order_date
				ORDER BY OA.order_date ASC";
//GROUP BY OA.shop_id
				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
function getSalesPOrders($date,$salespid){
			 $sql = "(
						SELECT  DISTINCT(VO.order_id), OA.shop_id, shops.name as shopname, 
						shops.address, shops.mobile, OA.order_date , c.categorynm, 
						VO.order_id,VO.cat_id,OA.lat AS oplacelat, OA.long AS oplacelon,SV.no_o_lat,SV.no_o_long,
						(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
						(SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
						OA.order_date AS date,
						SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details
						FROM tbl_orders OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id
						LEFT JOIN tbl_category c ON c.id= VO.cat_id 		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = $salespid AND VO.order_id!=''
						GROUP BY VO.order_id,
						OA.order_date
						ORDER BY OA.order_date ASC
					)
					UNION  ALL
					(
						SELECT DISTINCT(VO.order_id), OA.shop_id,VO.cat_id, 
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname,
						shops.address, shops.mobile,OA.lat AS oplacelat, OA.long AS oplacelon,SV.no_o_lat,SV.no_o_long,
						OA.order_date , c.categorynm, VO.order_id,
						(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
						(SELECT name FROM tbl_state WHERE id = shops.state) AS statename, 
						SV.shop_visit_date_time AS date,						
						SV.shop_visit_reason, SV.shop_close_reason_type, SV.shop_close_reason_details
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id AND OA.order_date =SV.shop_visit_date_time					
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id
						LEFT JOIN tbl_category c ON c.id= VO.cat_id 	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = $salespid AND VO.order_id!=''
						ORDER BY SV.shop_visit_date_time ASC
					)
					ORDER BY date ASC";
//GROUP BY OA.shop_id
				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}

	function getSPLocationPoints($date,$salespid){
		 $sql = "SELECT * FROM `tbl_user_location` AS OA WHERE
		date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = " . $salespid;	
		$result = mysqli_query($this->local_connection,$sql);
		return $result;
	}
	function getSalesPOrdersProducts($date,$salespid)
	{
		 $sql = "SELECT DISTINCT(OV.product_variant_id),OV.product_id, b.name, c.categorynm,OV.brand_id,OV.cat_id,OA.ordered_by,OA.order_date,
		(SELECT  productname FROM `tbl_product` WHERE id = OV.product_id) AS productname
		FROM `tbl_orders` AS OA 
		LEFT JOIN `tbl_order_details` AS OV ON OV.order_id = OA.id 
		LEFT JOIN `tbl_brand` AS b ON b.id = OV.brand_id
		LEFT JOIN `tbl_category` AS c ON c.id= OV.cat_id 
		WHERE
		date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.ordered_by = " . $salespid;
									
		$result = mysqli_query($this->local_connection,$sql);
		return $result;		
	}
	function getSalesPProductQuantity($date,$salespid,$product_id,$product_variant_id)
	{
		 $sql = "SELECT quantity from tbl_sp_todays_quantity
		WHERE date_format(sptqdate, '%d-%m-%Y') = '".$date."' AND userid = ".$salespid." AND productid = ".$product_id." AND prod_var_id = ".$product_variant_id;
		$result = mysqli_query($this->local_connection,$sql);
		$row = mysqli_fetch_assoc($result);
		return $row;	
	}
	function getSalesPOrdersPQuantity($shop_id,$product_variant_id,$order_date)
	{
	    $sql = "SELECT OV.product_quantity AS quantity, OV.p_cost_cgst_sgst AS totalcost, OV.product_quantity AS variantunit FROM `tbl_orders` AS OA LEFT JOIN `tbl_order_details` AS OV ON OV.order_id = OA.id
		WHERE
		OV.product_variant_id = ".$product_variant_id." AND OA.shop_id = " . $shop_id." AND OA.order_date = '".$order_date."'";
									
		$result = mysqli_query($this->local_connection,$sql);
		$row = mysqli_fetch_assoc($result);
		return $row;		
	}
	function getSalesPOrdersDetails($orderRecId){
		$sql = "SELECT	shops.name as shopname, shops.address, shops.mobile,
				OA.order_date , OA.categorynm,
				(SELECT name FROM tbl_city WHERE id = shops.city) AS cityname,
				(SELECT name FROM tbl_state WHERE id = shops.state) AS statename
				FROM tbl_order_app OA 
				LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				ORDER BY OA.order_date ASC";

				$result = mysqli_query($this->local_connection,$sql); 				
				return $result;		
	}
	function getSalesPOrdersStart_old($date,$salespid){
		$sql = "SELECT	OA.order_date , VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
				FROM tbl_order_app OA 
				LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
				LEFT JOIN tbl_shops shops ON shops.id= VO.shopid			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				ORDER BY OA.order_date ASC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 	
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersEnd_old($date,$salespid){
		$sql = "SELECT	OA.order_date, VO.id, shops.name as shopname, shops.address, OA.oplacelat, OA.oplacelon
				FROM tbl_order_app OA 
				LEFT JOIN tbl_variant_order VO ON OA.id = VO.orderappid 
				LEFT JOIN tbl_shops shops ON shops.id= VO.shopid			
				WHERE
					date_format(OA.order_date, '%d-%m-%Y') = '".$date."' AND OA.order_by = " . $salespid."
				ORDER BY OA.order_date DESC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 				
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersStart($date,$salespid){				
				 $sql = "(
						SELECT  VO.id, shops.name as shopname, shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,SV.no_o_lat,SV.no_o_long,SV.shop_id AS v_shop_id,OA.shop_id AS o_shop_id,
						OA.order_date AS date
						FROM tbl_orders OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = " . $salespid."
						GROUP BY VO.order_id,
						OA.order_date
						ORDER BY OA.order_date ASC
					)
					UNION  ALL
					(
						SELECT VO.id,
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
						shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,SV.no_o_lat,SV.no_o_long,SV.shop_id AS v_shop_id,OA.shop_id AS o_shop_id,
						SV.shop_visit_date_time AS date		
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time						
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = " . $salespid."
						ORDER BY SV.shop_visit_date_time ASC
					)
					ORDER BY date ASC LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 	
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPOrdersEnd($date,$salespid){
					$sql = "(
						SELECT  OA.id AS oid, VO.id, shops.name as shopname, shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,SV.no_o_lat,SV.no_o_long,SV.shop_id AS v_shop_id,OA.shop_id AS o_shop_id,
						OA.order_date AS date
						FROM tbl_orders OA 
						LEFT JOIN tbl_shops shops ON shops.id= OA.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id		
						LEFT OUTER JOIN  tbl_shop_visit AS SV ON SV.shop_id = OA.shop_id  AND OA.order_date =SV.shop_visit_date_time
						WHERE date_format(OA.order_date, '%d-%m-%Y') = '$date' AND OA.ordered_by = $salespid
						GROUP BY VO.order_id,
						OA.order_date
						ORDER BY OA.id DESC, OA.order_date DESC
					)
					UNION  ALL
					(
						SELECT OA.id AS oid, VO.id,
						(SELECT name FROM tbl_shops WHERE id = SV.shop_id) AS shopname, 
						shops.address, OA.lat AS oplacelat, OA.long AS oplacelon,SV.no_o_lat,SV.no_o_long,SV.shop_id AS v_shop_id,OA.shop_id AS o_shop_id,
						SV.shop_visit_date_time AS date		
						FROM tbl_shop_visit AS SV
						LEFT OUTER JOIN  tbl_orders OA  ON OA.shop_id = SV.shop_id  AND OA.order_date =SV.shop_visit_date_time						
						LEFT JOIN tbl_shops shops ON shops.id= SV.shop_id
						LEFT JOIN tbl_order_details VO ON VO.order_id= OA.id	
						WHERE date_format(SV.shop_visit_date_time, '%d-%m-%Y') = '$date' AND SV.salesperson_id = $salespid
						ORDER BY SV.id DESC, SV.shop_visit_date_time DESC
					)
					ORDER BY date DESC, oid DESC  LIMIT 1";

				$result = mysqli_query($this->local_connection,$sql); 				
				$record = mysqli_fetch_assoc($result);				
				return $record;		
	}
	function getSalesPDayStartEndPoints($date,$salespid){	
		$sql = "SELECT * FROM `tbl_sp_attendance` 
		WHERE date_format(tdate, '%d-%m-%Y')='$date' AND sp_id= $salespid";
		$result = mysqli_query($this->local_connection,$sql); 				
		$record = mysqli_fetch_assoc($result);				
		return $record;	
	}
	function getSalesPStartEndDay($date,$salespid){
		$sql_day_time = "SELECT	`sp_id`, `tdate`, `presenty`, `dayendtime`
				FROM tbl_sp_attendance
				WHERE
				date_format(tdate, '%d-%m-%Y') = '".$date."' AND sp_id = " . $salespid;

		$result = mysqli_query($this->local_connection,$sql_day_time); 
		$totalRecords=mysqli_num_rows($result);
		if($totalRecords > 0)
					return mysqli_fetch_array($result);
				else
					return $totalRecords;	
	}
		function getSProductVariant($product_variant_id){
		$sqlprd="SELECT variant_1, variant_2 FROM `tbl_product_variant` WHERE id = '$product_variant_id' ";
		$resultprd = mysqli_query($this->local_connection,$sqlprd);
		$rowprd = mysqli_fetch_array($resultprd);
		$exp_variant1 = $rowprd['variant_1'];
		$imp_variant1= explode(',',$exp_variant1);
		$exp_variant2 = $rowprd['variant_2']; $imp_variant2= explode(',',$exp_variant2);
		$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant1[1]'";
		$resultunit = mysqli_query($this->local_connection,$sql);
		$rowunit = mysqli_fetch_array($resultunit);
		$variant_unit1 = $rowunit['unitname'];
		$sql="SELECT unitname , id FROM `tbl_units` WHERE id='$imp_variant2[1]'";
		$resultunit = mysqli_query($this->local_connection,$sql);
		$rowunit = mysqli_fetch_array($resultunit);
		$variant_unit2 = $rowunit['unitname'];
		$dimentionDetails = "";
		/*if($imp_variant1[0]!="") {
			$dimentionDetails = " Pcs:  " .$imp_variant1[0] . " " . $variant_unit1;;//  $imp_variant1[0] //$variant_unit1;
		}*/
		if($variant_unit1!="") 
		{
			$dimentionDetails = " " .$imp_variant1[0]." ". $variant_unit1;//  $imp_variant1[0] //$variant_unit1;
		}
		//	$dimentionDetails .= " - Weight: " .  $imp_variant2[0] . " " . $variant_unit2;
		return $dimentionDetails;
	}
	function getSPStartLocationPoints($date,$salespid){
		/*echo $sql = "SELECT *,MIN(id) FROM `tbl_user_location` AS OA WHERE
		date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = " . $salespid;*/
		 /*  $sql = "SELECT * FROM `tbl_user_location` AS OA WHERE date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = '".$salespid."' ORDER BY OA.id ASC LIMIT 0, 1";*/

		 $sql ="SELECT OA.lattitude,OA.longitude,SA.tdate AS start_date_time 
		 FROM `tbl_user_location` AS OA 
LEFT JOIN `tbl_sp_attendance` AS SA ON SA.sp_id = OA.userid  
WHERE date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND date_format(SA.tdate, '%d-%m-%Y') = '".$date."' AND  OA.userid = '".$salespid."' ORDER BY SA.id ASC LIMIT 0, 1";

		$result = mysqli_query($this->local_connection,$sql);
		return $result;
	}
	function getSPEndLocationPoints($date,$salespid){
		/* $sql = "SELECT *,MAX(id) FROM `tbl_user_location` AS OA WHERE
		date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = " . $salespid;*/
		/* $sql = "SELECT * FROM `tbl_user_location` AS OA WHERE date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND OA.userid = '".$salespid."' ORDER BY OA.id DESC LIMIT 0, 1";	*/
		  $sql ="SELECT OA.lattitude,OA.longitude,SA.dayendtime AS end_date_time FROM `tbl_user_location` AS OA 
LEFT JOIN `tbl_sp_attendance` AS SA ON SA.sp_id = OA.userid  
WHERE date_format(OA.tdate, '%d-%m-%Y') = '".$date."' AND date_format(SA.dayendtime, '%d-%m-%Y') = '".$date."' AND  OA.userid = '".$salespid."' ORDER BY SA.id DESC LIMIT 0, 1";
		$result = mysqli_query($this->local_connection,$sql);
		return $result;
	}
	public function getLocation($latitude, $longitude){

		$geolocation = $latitude.",".$longitude;//"18.4773911,73.9025829";//
		$request = "http://maps.googleapis.com/maps/api/geocode/json?latlng=$geolocation&sensor=false";
		
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $request); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		$output = curl_exec($ch);   

		$json_decode = json_decode($output);
		
		$address = "";
		if(isset($json_decode->results[0])) {
			$response = array();
			$address = $json_decode->results[0]->formatted_address;
		}
		return $address;
	}
	function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km')
	 {
	 	/*$latitude1 ='18.520430';
	 	$longitude1 ='73.856744';

	 	$latitude2 ='19.075984';
	 	$longitude2 ='72.877656'; Mumabi pune co-ordinates*/ 

     $theta = $longitude1 - $longitude2;
     $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
     $distance = acos($distance);
     $distance = rad2deg($distance);
     $distance = $distance * 60 * 1.1515; 
     switch($unit) {
          case 'Mi': break; case 'Km' : $distance = $distance * 1.609344;
     }
     return (round($distance,2));
}


function getDistanceBetweenPoints($lat1, $lat2, $long1, $long2)
	{
		//echo $lat1;
		//exit();
		$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		$response_a = json_decode($response, true);
		$dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
		$time = $response_a['rows'][0]['elements'][0]['duration']['text'];
		return array('distance' => $dist, 'time' => $time);
	}
	public function getOrders($order_status, $order_id = null){		
		extract($_POST);
		$where_clause_outer ='';
		$where_clause_inner ='';
		
		if($order_id != ''){
			$where_clause_outer .= " AND o.id = '".$order_id."' ";
		}
		if($frmdate != ''){
			$where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '".$frmdate."' ";
		}
		if($dropdownSalesPerson!="")
		{
			$where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
		}
		if($divShopdropdown !="")
		{
			$where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
		}
		if($subarea !="") {
			$where_clause_outer .= " AND s.subarea_id = " . $subarea;
		}
		if($dropdownCity !="")
		{
			$where_clause_outer .= " AND s.city = " . $dropdownCity;
		}
		if($dropdownState !="")
		{
			$where_clause_outer .= " AND s.state = " . $dropdownState;
		}
		if($dropdownCategory !="")
		{
			$where_clause_inner .= " AND od.cat_id = " . $dropdownCategory;
		}
		if($dropdownProducts !="")
		{
			$where_clause_inner .= " AND od.product_id = " . $dropdownProducts;
		}
		  $order_sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status 
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		WHERE 1=1 $where_clause_outer
		ORDER BY shop_name, o.order_date DESC";//o.shop_order_status = ".$order_status."
		//product_sgst
		$orders_result = mysqli_query($this->local_connection,$order_sql);
		$row_orders_count = mysqli_num_rows($orders_result);
		$i = 1;
		$all_orders = array();
		$row_orders_det_count = 0;
		if($row_orders_count > 0){
			while($row_orders = mysqli_fetch_assoc($orders_result))
			{
				$all_orders[$i] = $row_orders;
				 $order_details_sql = "SELECT od.id, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
				odis.discount_amount
				FROM `tbl_order_details` AS od
				LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
				WHERE od.order_id = ".$row_orders['id']." AND od.order_status = ".$order_status." $where_clause_inner
				";
				$orders_det_result = mysqli_query($this->local_connection,$order_details_sql);
				$row_orders_det_count = mysqli_num_rows($orders_det_result);
				if($row_orders_det_count > 0){
					$j = 1;
					while($row_orders_det = mysqli_fetch_assoc($orders_det_result))
					{
						$all_orders[$i]['order_details'][$j] = $row_orders_det;
						$j++;
					}
					$i++;
				}else{
					unset ($all_orders[$i]); 
				}
			}
		}
		//print"<pre>";
		//print_r($all_orders);
		
		if(count($all_orders) == 0){
			$all_orders = array();
			return $all_orders;
		}
		else
			return $all_orders;
	}
	public function getOrders1(){	//for orders page search
		extract($_POST);
		$where_clause_outer ='';
		$where_clause_inner ='';
		
		if($order_id != ''){
			$where_clause_outer .= " AND o.id = '".$order_id."' ";
		}
		if($frmdate != ''){
			$where_clause_outer .= " AND date_format(o.order_date, '%d-%m-%Y') = '".$frmdate."' ";
		}
		if($dropdownSalesPerson!="")
		{
			$where_clause_outer .= " AND o.ordered_by = " . $dropdownSalesPerson;
		}
		if($divShopdropdown !="")
		{
			$where_clause_outer .= " AND o.shop_id = " . $divShopdropdown;
		}
		if($subarea !="") {
			$where_clause_outer .= " AND s.subarea_id = " . $subarea;
		}
		if($dropdownCity !="")
		{
			$where_clause_outer .= " AND s.city = " . $dropdownCity;
		}
		if($dropdownState !="")
		{
			$where_clause_outer .= " AND s.state = " . $dropdownState;
		}
		if($dropdownCategory !="")
		{
			$where_clause_outer .= " AND od.cat_id = " . $dropdownCategory;
		}
		if($dropdownProducts !="")
		{
			$where_clause_outer .= " AND od.product_id = " . $dropdownProducts;
		}
		   $order_sql = "SELECT o.id as oid, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  s.name AS shop_name,
		s.suburbid, (SELECT sb.suburbnm FROM tbl_surb AS sb WHERE sb.id = s.suburbid) AS region_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status 
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_shops AS s ON s.id = o.shop_id 
		LEFT JOIN tbl_order_details AS od ON od.order_id = o.id 
		WHERE 1=1 $where_clause_outer
		ORDER BY o.order_date DESC ";//o.shop_order_status = ".$order_status."
		$orders_result = mysqli_query($this->local_connection,$order_sql);
		$row_orders_count = mysqli_num_rows($orders_result);
		$i = 1;
		$all_orders = array();
		if($row_orders_count > 0){
			while($row_orders = mysqli_fetch_assoc($orders_result))
			{
				$all_orders[$i] = $row_orders;
				$i++;
			}
		}
		
		if(count($all_orders) == 0){
			$all_orders = array();
			return $all_orders;
		}
		else
			return $all_orders;
	}
	public function getOrdersDetails1($order_id){	
	
			if($order_id !=""){
				$where_clause_outer .= " AND od.order_id = " . $order_id;
			}
				 $order_details_sql = "SELECT od.id, od.order_id, 
				od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
				od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
				od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
				od.producthsn,
				od.product_variant_id, od.product_quantity, 
				od.product_variant_weight1, od.product_variant_unit1, 
				od.product_variant_weight2, od.product_variant_unit2, 
				od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
				od.campaign_applied, od.campaign_type, od.campaign_sale_type, od.order_status, 
				od.delivery_assing_date, od.delivery_assign_to, od.transport_date, 
				od.challan_no, od.vehicle_no, od.transport_mode, od.date_time_supply, od.place_of_supply, 
				od.quantity_delivered, od.delivery_date, od.amount_paid, od.payment_date,
				odis.discount_amount,odpw.discounted_totalcost
				FROM `tbl_order_details` AS od
				LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
				LEFT JOIN tbl_campaign_price_weight AS odpw ON odpw.odid = od.id
				WHERE 1=1 $where_clause_outer";//AND od.order_status = ".$order_status." $where_clause_inner
				$orders_det_result = mysqli_query($this->local_connection,$order_details_sql);
				//$row_orders_det_count = mysqli_num_rows($orders_det_result);
				if(mysqli_num_rows($orders_det_result) > 0)
				{
					while($row_orders_det = mysqli_fetch_assoc($orders_det_result))
					{
						$all_orders['order_details'][] = $row_orders_det;
					}
				}
			return $all_orders;
	}
	public function getOrderDetailsById($order_details_id){
		$sql = "SELECT o.id, o.order_no, o.invoice_no, 
		o.ordered_by, (SELECT u.firstname FROM tbl_user AS u WHERE u.id = o.ordered_by) AS order_by_name,
		o.order_date, 
		o.shop_id,  (SELECT s.name  FROM tbl_shops AS s WHERE s.id = o.shop_id) AS shop_name,
		o.total_items, 
		o.total_cost, o.total_order_gst_cost, o.lat, 
		o.long, o.offer_provided, o.shop_order_status, 
		od.id, od.order_id, 
		od.brand_id, (SELECT b.name FROM tbl_brand AS b WHERE b.id = od.brand_id) AS brand_name,
		od.cat_id, (SELECT c.categorynm FROM tbl_category AS c WHERE c.id = od.cat_id) AS cat_name,
		od.product_id, (SELECT p.productname FROM tbl_product AS p WHERE p.id = od.product_id) AS product_name,
		od.producthsn, od.product_variant_id, od.product_quantity, 
		od.product_variant_weight1, od.product_variant_unit1, 
		od.product_variant_weight2, od.product_variant_unit2, 
		od.product_unit_cost, od.product_total_cost, od.product_cgst, od.product_sgst, od.p_cost_cgst_sgst, 
		od.campaign_applied, od.campaign_type, od.campaign_sale_type,
		od.order_status, od.delivery_assing_date, od.delivery_assign_to,
		od.transport_date, od.challan_no,od.vehicle_no, od.transport_mode, 
		od.date_time_supply, od.place_of_supply, od.quantity_delivered, od.delivery_date, 
		od.amount_paid, od.payment_date,
		odis.discount_amount,odpw.discounted_totalcost
		FROM `tbl_orders` AS o 		
		LEFT JOIN tbl_order_details AS od ON od.order_id = o.id
		LEFT JOIN tbl_order_cp_discount AS odis ON odis.order_variant_id = od.id
		LEFT JOIN tbl_campaign_price_weight AS odpw ON odpw.odid = od.id
		WHERE od.id = ".$order_details_id;
		$orders_det_result = mysqli_query($this->local_connection,$sql);
		$row_orders_det_count = mysqli_num_rows($orders_det_result);
		return $row = mysqli_fetch_assoc($orders_det_result);
	}
	public function order_payment_update(){
		extract ($_POST);
		$j=0;
		//print"<pre>";
		//print_r($_POST); 
		for($i=1; $i<= $data_count; $i++){			
			 /*[order_details_id_1] => 2
			[order_price_1] => 2835
			[payment_partial_1] => on
			[payment_amount_1] => 2335
			[payment_date_1] => 25-03-2018
			[hidbtnsubmit_order_pay] => */
	
			$shop_id = $_POST['shop_id_'.$i];
			$order_id = $_POST['order_id_'.$i];
			$order_detail_id = $_POST['order_details_id_'.$i];
			$order_price = $_POST['order_price_'.$i];
			$amount_paid = $_POST['payment_amount_'.$i];
			$payment_date = date('Y-m-d',strtotime($_POST['payment_date_'.$i]));
			if(isset($_POST['payment_partial_'.$i])){
				if($order_price < $amount_paid){
					$status = 6;//Complete Payment Done
				}else
					$status = 8;//Partial Payment Done
			}else
				$status = 6;//Complete Payment Done
			
			
			
			/*$update_order_sql = "UPDATE `tbl_orders` 
			SET `shop_order_status`=".$status." 
			WHERE id = ".$order_id;
			mysqli_query($this->local_connection,$update_order_sql);
			*/
			$update_order_det_sql = "UPDATE `tbl_order_details` 
			SET 
			`order_status` = ".$status.",`amount_paid`=".$amount_paid.",`payment_date`= '".$payment_date."'
			WHERE id = ".$order_detail_id;
			mysqli_query($this->local_connection,$update_order_det_sql);
			
			$sql = "INSERT INTO 
			`tbl_shop_payment`(`shop_id`, `order_details_id`, `paid_amount`, `payment_date`) 
			VALUES (".$shop_id.",".$order_detail_id.",".$amount_paid.",'".$payment_date."')";
			mysqli_query($this->local_connection,$sql);
			$j++;
		}
		if($j == $data_count)
			return true;
		else
			return false;
	}
	public function get_opening_balance($shop_id){
		/*$sql_amount = "SELECT ROUND(SUM(p_cost_cgst_sgst),2) AS paid_amount
		FROM `tbl_shop_payment`
		WHERE shop_id = ".$shop_id."
		GROUP BY shop_id";
		$orders_paid_result = mysqli_query($this->local_connection,$sql_amount);
		$row_paid_count = mysqli_num_rows($orders_paid_result);
		$row_paid = mysqli_fetch_assoc($orders_paid_result);*/
		
		$sql_pending_amount = "SELECT ROUND(SUM(p_cost_cgst_sgst),2) AS amount_to_pay
		FROM `tbl_order_details` AS od
		LEFT JOIN `tbl_orders` AS o ON o.id = od.order_id
		WHERE o.shop_id = ".$shop_id." AND od.order_status = 4
		GROUP BY o.shop_id";
		$orders_payment_result = mysqli_query($this->local_connection,$sql_pending_amount);
		$row_payment_count = mysqli_num_rows($orders_payment_result);
		$row_payment = mysqli_fetch_assoc($orders_payment_result);
	}
	public function generate_invoice_no(){
		$sql = "SELECT invoice_no FROM `tbl_orders` WHERE invoice_no != '' ORDER BY id DESC LIMIT 1 ";
		$result = mysqli_query($this->local_connection,$sql);
		$row_count = mysqli_num_rows($result);
		$row = mysqli_fetch_assoc($result);
		$part1 = 'SJFP';
		if(date("m") >= 4){
			$part2 = date('y').'-'.date("y",strtotime("+1 year"));
		}else{
			$part2 = date("y",strtotime("-1 year")).'-'.date('y');
		}
		$inc_number = 5000;
		if($row_count == 0){
			$inc_number = 5000;
		}else{
			$invoice_part =  explode('/',$row['invoice_no']);
			$inc_number = $invoice_part[2] + 1;
		}
		$part3 = $inc_number;
		$invoice_no = $part1.'/'.$part2.'/'.$part3;
		return $invoice_no;
	}
	public function getSPattendance() {
		
		extract($_POST);
		$dropdownSalesPerson= $_POST['dropdownSalesPerson'];
		$user_type = $_SESSION[SESSION_PREFIX.'user_type'];
		$limit = '';
		$order_by = '';
		$search_name = '';
		if($actionType=="excel") {
			$start = 0;
			if($page == 0)
				$start = '';
			else
			{
				$start = ($per_page * ($page)).",";
			}
			$sort = explode(',',$sort_complete);
			$sort_by = '';
			if($sort[0] == 0)
				$sort_by = ' Name '.$sort[1];
			else if($sort[0] == 1)
				$sort_by = ' Total_Sales '.$sort[1];
				
			$order_by = ' ORDER BY '.$sort_by;
			if($per_page != -1)	
				$limit = " LIMIT $start ".$per_page; 
		}
		$condnsearch="";
		$selperiod=$selTest;
		if($selperiod!=0){
			switch($selperiod){
				case 1:
					$condnsearch=" AND yearweek(sa.tdate) = yearweek(curdate())";
					//$condnsearch_sp=" AND yearweek(shop_visit_date_time) = yearweek(curdate())";
				break;
				case 2:
					$condnsearch=" AND date_format(sa.tdate, '%m')=date_format(now(), '%m')";
					//$condnsearch_sp=" AND date_format(shop_visit_date_time, '%m')=date_format(now(), '%m')";
				break;
				case 3:
					if($todate!="")
						$todat=$todate;
					else
						$todat=date("d-m-Y");			
					
					$condnsearch=" AND (date_format(sa.tdate, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(sa.tdate, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				//	$condnsearch_sp=" AND (date_format(shop_visit_date_time, '%Y-%m-%d') >= STR_TO_DATE('".$frmdate."','%d-%m-%Y') AND date_format(shop_visit_date_time, '%Y-%m-%d') <= STR_TO_DATE('".$todat."','%d-%m-%Y'))";
				break;
				case 4:
					$date = date('d-m-Y');
					/*$condnsearch=" AND date_format(sa.tdate, '%d-%m-%Y') = '".$date."' AND date_format(sa.dayendtime, '%d-%m-%Y') = '".$date."'";
*/
					$condnsearch=" AND date_format(sa.tdate, '%d-%m-%Y') = '".$date."' AND date_format(sa.dayendtime, '%d-%m-%Y') = '".$date."'";
				//	$condnsearch_sp=" AND date_format(shop_visit_date_time, '%d-%m-%Y') = '".$date."' ";
				break;
				case 0:
					$condnsearch="";
				break;
				case 5:
					$condnsearch="";
				break;
				default:
					$condnsearch="";
				break;

			}
		}
		$where="";
		if($dropdownSalesPerson!=''){
			$where= " AND sa.sp_id = $dropdownSalesPerson ";
		}
		 $sql = "SELECT (UNIX_TIMESTAMP(sa.dayendtime) - UNIX_TIMESTAMP(sa.tdate)) / 60.0 / 60.0 as hours_difference,u.id,sa.id,
					sa.tdate,
					sa.dayendtime,	
					u.firstname
				FROM tbl_user AS u 
				LEFT JOIN tbl_sp_attendance sa ON sa.sp_id = u.id		
				WHERE u.user_type='salesperson'
					$where $condnsearch ORDER BY sa.id DESC";	//date_format(OA.order_date, '%d-%m-%Y') = '".$frmdate."' //tbl_order_app
		$result1 = mysqli_query($this->local_connection,$sql);
		
		$i = 0;
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0)
		{
			while($row = mysqli_fetch_assoc($result1))
			{
				$records[$i] = $row;
				$i++;
			}
			return $records;
		}
		else
		{
			return 0;
		}
	} 
}
?>