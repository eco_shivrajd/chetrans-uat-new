<div class="page-sidebar-wrapper">	 
	<div class="page-sidebar navbar-collapse collapse">
		<ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
		
			<li class="sidebar-toggler-wrapper">
				<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				<div class="sidebar-toggler"></div>
				<!-- END SIDEBAR TOGGLER BUTTON -->
			</li>			
			<br/>			
			<?if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>
			<li class="<?php if($activeMainMenu=="Dashboard"){ echo 'active open'; } ?>">
				<a href="index.php">
				<i class="icon-home"></i>
				<span class="title">Dashboard</span>
				<span class="selected"></span>
				</a>
			</li>
			
			<li class="<?php if($activeMainMenu=="ManageSupplyChain"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Supply Chain</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					
				    <li class="<?php if($activeMenu=="Area"){ echo 'active open'; } ?>">
						<a href="suburb.php">
						 Region</a>
					</li>
					
					<li class="<?php if($activeMenu=="SalesPerson"){ echo 'active open'; } ?>">
						<a href="sales.php">
						 Sales Person </a>
					</li>
					<!-- <li class="<?php if($activeMenu=="DeliveryPerson"){ echo 'active open'; } ?>">
						<a href="delivery-persons.php">
						 Delivery Person</a>
					</li>
					<li class="<?php if($activeMenu=="Accountant"){ echo 'active open'; } ?>">
						<a href="accountant.php">
						 Accountant</a>
					</li> -->
					<li class="<?php if($activeMenu=="Shops"){ echo 'active open'; } ?>">
						<a href="shops.php">Shops</a>
					</li>
				</ul>
			</li>
			<? } ?>
					
			<?if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>
			<li class="<?php if($activeMainMenu=="ManageProducts"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Products</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				   <li class="<?php if($activeMenu=="Brands"){ echo 'active open'; } ?>">
						<a href="brands.php">
						 Brands</a>
					</li>
					<li class="<?php if($activeMenu=="Categories"){ echo 'active open'; } ?>">
						<a href="categories.php">
						 Categories </a>
					</li>
					<li class="<?php if($activeMenu=="Variant"){ echo 'active open'; } ?>">
						<a href="variant.php">
						 Variant</a>
					</li>
					<li class="<?php if($activeMenu=="Product"){ echo 'active open'; } ?>">
						<a href="product.php">
						Product</a>
					</li>
					<li class="<?php if($activeMenu=="Campaign"){ echo 'active open'; } ?>">
						<a href="campaign.php">
						Campaign</a>
					</li> 
				</ul>
			</li>
			<!-- <li class="<?php if($activeMainMenu=="ManageTransport"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Transport</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				   <li class="<?php if($activeMenu=="TransportOffices"){ echo 'active open'; } ?>">
						<a href="transport_offices.php">
						 Transport Offices</a>
					</li>
					
				</ul>
			</li> -->
			<li class="<?php if($activeMainMenu=="ManageTransport"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Manage Transport</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				   <li class="<?php if($activeMenu=="modeoftranse"){ echo 'active open'; } ?>">
						<a href="motlist.php">
						Mode Of Transport</a>
					</li>
					
				</ul>
			</li>
			<?}?>
					
			
			<?if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>			
			<li class="<?php if($activeMainMenu=="Orders"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Orders</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php if($activeMenu=="Orders"){ echo 'active open'; } ?>">
					<a href="orders-new.php">Orders </a>
					</li>
				</ul>
			<li>
		
			<? } else { ?>
			
				<li class="<?php if($activeMainMenu=="Orders"){ echo 'active open'; } ?>">
				<a href="javascript:;">
				<i class="fa fa-share-alt"></i>
				<span class="title">Orders</span>
				<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
					<li class="<?php if($activeMenu=="Orders"){ echo 'active open'; } ?>">
					<a href="orders_accountant.php">Orders</a>
					</li>
				</ul>
				<li>			
			<? } ?>
			<?if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>
			<li class="<?php if($activeMainMenu=="Reports"){ echo 'active open'; } ?>">
				<a href="javascript:;">
					<i class="fa fa-share-alt"></i>
					<span class="title">Reports</span>
					<span class="arrow "></span>
				</a>
				<ul class="sub-menu">
				    <li class="<?php if($activeMenu=="OrderSummary"){ echo 'active open'; } ?>">
					<a href="order_summary.php">Summary</a>
					</li>
					<li class="<?php if($activeMenu=="SalesReport"){ echo 'active open'; } ?>">
						<a href="sales_report.php">Sales Report</a>
					</li> 
					<?if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>
					<li class="<?php if($activeMenu=="OrderSummary1"){ echo 'active open'; } ?>">
					<a href="order_summary1.php">SP Summary</a>
					</li>
						<li class="<?php if($activeMenu=="OrderSummary2"){ echo 'active open'; } ?>">
					<a href="order_summary2.php">Shop Summary</a>
					</li>
					<li class="<?php if($activeMenu=="DailyStatusReport"){ echo 'active open'; } ?>">
						<a href="daily_status_report.php">Daily Status Report</a>
					</li> 
					<li class="<?php if($activeMenu=="SPDailyStatusReport"){ echo 'active open'; } ?>">
						<a href="sp_daily_status_report.php">Sales Person DSR</a>
					</li>
					<li class="<?php if($activeMenu=="geolocation"){ echo 'active open'; } ?>">
						<a href="geolocation-track1.php">Sales Person Location</a>
					</li> 
					<li class="<?php if($activeMenu=="tadareport"){ echo 'active open'; } ?>">
						<a href="tadareport.php">Expense Report</a>
					</li>
					<li class="<?php if($activeMenu=="Sales Person Attendance"){ echo 'active open'; } ?>">		
						<a href="sales_person_attendance.php">Sales Person Attendance</a>
					</li>
					<? } ?>
				</ul>
			</li>
			<? } ?>
			
			<?if($_SESSION[SESSION_PREFIX.'user_type']=="Admin") {?>
				
			<li class="<?php if($activeMainMenu=="Noorder"){ echo 'active open'; } ?>">
				<a href="no_order_history.php">
				<i class="fa fa-share-alt"></i>
				<span class="title">No order accept history</span>
				</a>
			</li>
			<? } ?>	
			 
		</ul>
		<!-- END SIDEBAR MENU -->
	</div>
</div>