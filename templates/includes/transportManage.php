<?php
/***********************************************************
 * File Name	: transportManage.php
 ************************************************************/	

class transportManager
{	
	private $local_connection   	= 	'';
	private $common_connection   	= 	'';
	public function __construct($con,$conmain) {
		$this->local_connection = $con;
		$this->common_connection = $conmain;
		$this->commonObj 	= 	new commonManage($this->local_connection,$this->common_connection);		
	}	
	
	public function getAllTransportOffices() {		
		$sql1="SELECT `id`, `trans_off_name`, `transport_type`, `state_id`, `city_id`, `suburb_id`, `status`, `isdeleted`,
		(SELECT name FROM tbl_state WHERE id = state_id) AS state_name,
		(SELECT name FROM tbl_city WHERE id = city_id) AS city_name,
		(SELECT suburbnm FROM tbl_surb WHERE id = suburb_id) AS region_name,
		(SELECT transport_name FROM tbl_transport_type WHERE id = transport_type) AS transport_name
		FROM tbl_transport_offices  where isdeleted!='1' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $result1;		
		}else
			return $row_count;		
	}	
	
	public function getTransportOfficeDetails($id) {		
		$sql1="SELECT `id`, `trans_off_name`, `transport_type`, `address`, `state_id`, `city_id`, `suburb_id`, `status`, `isdeleted`,
		(SELECT name FROM tbl_state WHERE id = state_id) AS state_name,
		(SELECT name FROM tbl_city WHERE id = city_id) AS city_name,
		(SELECT suburbnm FROM tbl_surb WHERE id = suburb_id) AS region_name,
		(SELECT transport_name FROM tbl_transport_type WHERE id = transport_type) AS transport_name
		FROM tbl_transport_offices WHERE id = '$id' ";
		$result1 = mysqli_query($this->local_connection,$sql1);
		$row_count = mysqli_num_rows($result1);
		if($row_count > 0){	
			return $row = mysqli_fetch_assoc($result1);		
		}else
			return $row_count;		
	}	
	
	public function addTransportOfficeDetails() {
		extract ($_POST);
		$trans_off_name=fnEncodeString($trans_off_name);
		$address=fnEncodeString($address);		
			
		$fields = '';
		$values = ''; 		
		if($state_id != '')
		{
			$fields.= ",`state_id`";
			$values.= ",'".$state_id."'";
		}
		if($city != '')
		{
			$fields.= ",`city_id`";
			$values.= ",'".$city."'";
		}
		if($area != '')
		{
			$fields.= ",`suburb_id`";			
			$values.= ",'".$area."'";
		}
		
		$sql = "INSERT INTO tbl_transport_offices (`trans_off_name`, `transport_type`,`address`, `status` $fields) 
		VALUES('".$trans_off_name."','".$transport_type."','".$address."', '".$status."' $values)";
	
		mysqli_query($this->local_connection,$sql);
		$officeid=mysqli_insert_id($this->local_connection); 
		$this->commonObj->log_add_record('tbl_transport_offices',$officeid,$sql);	
	}
	
	public function updateTransportOfficeDetails($id) {
		extract ($_POST);		
		$trans_off_name=fnEncodeString($trans_off_name);
		$address=fnEncodeString($address);		
		
		$values = ''; 		
		if($state_id != '')
		{
			$values.= ", `state_id` = '".$state_id."'";
		}
		if($city != '')
		{
			$values.= ", `city_id` = '".$city."'";
		}
		if($area != '')
		{		
			$values.= ", `suburb_id` = '".$area."'";
		}
		
		$update_sql="UPDATE tbl_transport_offices SET trans_off_name='$trans_off_name',transport_type='$transport_type',address='$address' $values where id='$id'";		
		mysqli_query($this->local_connection,$update_sql);
		$this->commonObj->log_update_record('tbl_transport_offices',$id,$update_sql);		
	}
	public function deleteTOfficebyid($id){
		$tbl_transport_offices = "UPDATE  tbl_transport_offices SET isdeleted='1'   WHERE id='$id'";
		mysqli_query($this->local_connection,$tbl_transport_offices);
		
	}
}
?>