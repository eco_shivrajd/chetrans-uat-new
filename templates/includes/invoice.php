<? 
include ("../../includes/config.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
include "../includes/shopManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$admin_details_basic = $userObj->getLocalUserDetails($_SESSION[SESSION_PREFIX.'user_id']);
$admin_details = $userObj->getLocalUserOtherDetails($_SESSION[SESSION_PREFIX.'user_id'],'Admin');
$order_id = $_POST['order_id'];
$order_status = $_POST['order_status'];
$order_details = $orderObj->getOrders($order_status, $order_id);
//$product_variant = $orderObj->getSProductVariant($order_details['product_variant_id']);
//print"<pre>";
//print_r($order_details);
$order_detail = $order_details[1];
$shop_details = $shopObj->getShopDetails($order_detail['shop_id']);//Shop details
?>
<style>
.darkgreen{
	background-color:#364622; color:#fff!important; font-size:24px;font-weight:600;
}
.fentgreen1{
	background-color:#b0b29c;
	color:#4a5036;
	font-size:12px;
}
.fentgreen{
	background-color:#b0b29c;
	color:#4a5036;
}
.font-big{
	font-size:20px;
	font-weight:600;
	color:#364622;
}
.font-big1{
	font-size:18px;
	font-weight:600;
	color:#364622;
}
.table-bordered-popup {
    border: 1px solid #364622;
}
.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {
    border: 1px solid #364622;
	color:#4a5036;
}
.blue{
	color:#010057;
}
.blue1{
	color:#574960;
	font-size:16px;
}
.buyer_section{
	color:#574960;
	font-size:14px;
}
.pad-5{
	padding-left:10px;
}
.pad-40{
	padding-left:40px;
}
.np{
	padding-left:0px;
	padding-right:0px;
}
.bg{
	background-image:url(../../assets/global/img/watermark.png); background-repeat:no-repeat;
	 background-size: 200px 200px;
}
</style>
<div class="modal-header">
<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divPrintArea">
<div class="row">
<div class="col-md-12"> 
		<div class="portlet-body">
			<table class="table table-bordered-popup">
				<tbody>
				<tr>
				<td colspan="4" width="70%" class="darkgreen"><img src="../../assets/global/img/logo-fh-invoice.jpg" style="width:60px;"> &nbsp; SRI JAYA SHREE FOOD PRODUCT</td>
				<td colspan="3" class="font-big text-center">Tax Invoice</td>
				</tr>
				
				<tr>
				<td colspan="4" class="fentgreen1"><?=$admin_details_basic['address'];?><br/>
				Tel: <b><?=$admin_details['phone_no'];?></b> 
				<?=$admin_details['website'];?> Tollfree: <b><?=$admin_details['tollfree_no'];?></b>  
				State: <b><?=$admin_details_basic['state_name'];?></b> State Code: <b><?=$admin_details_basic['state'];?></b> GSTIN :<b><?=$admin_details_basic['gst_number_sss'];?></b></td>
				<td colspan="3" rowspan="2">
				<div class="col-md-7 np">Invoice No.: </div><span class="blue"><?=$order_detail['invoice_no'];?></span><br/>
				<div class="col-md-7 np">Dated: </div><span class="blue"></span><br/>
				<div class="col-md-7 np">D. C. No.: </div><br/>
				<div class="col-md-7 np">Vehicle No.:</div><br/>
				<div class="col-md-7 np">Transportation Mode:</div> <br/>
				<div class="col-md-7 np">Date & Time of Supply:</div><br/>
				<div class="col-md-7 np">Place of Supply:</div>
				</td>
				
				</tr>
				
				<tr>
				<td colspan="4">Buyer:
				<span class="buyer_section"><b><?=$shop_details['name'];?></b><br/></span>
				<span class="buyer_section pad-40"><?=$shop_details['address'];?>,<br/></span>
				<span class="buyer_section pad-40"><?=$shop_details['city_name'];?><br/></span>
				<span class="buyer_section pad-40"><?=$shop_details['state_name'];?><br/></span>
				<span class="buyer_section pad-40">GSTIN NO. <?=$shop_details['gst_number'];?></span>
				</td>                        
				</tr>
				<tr class="fentgreen">
				<th width="5%" class="text-center">SI No.</th>
				<th class="text-center">Name of Goods</th>
				<th class="text-center">HSN COde</th>
				<th class="text-center">Qty</th>
				<th class="text-center">Rate</th>
				<th class="text-center">UOM</th>
				<th class="text-center">Value</th>
				</tr>
				<tr style="height:214px;">
				<? 
				$i = 1;
				$final_qty = 0;
				$final_cost = 0;
				foreach($order_detail['order_details'] as $val){
					$sr_no.=$i."<br><br>";
					$product_name.=$val['product_name']."<br><br>";
					$hsn.=$val['producthsn']."<br><br>";
					$qty.=$val['product_quantity']."<br><br>";
					$final_qty = $final_qty + $val['product_quantity'];
					$unit_cost.=$val['product_unit_cost']."<br><br>";
					$nos.=$i."<br><br>";
					$total_cost.=$val['product_total_cost']."<br><br>";
					$final_cost = $final_cost + $val['product_total_cost'];
				$i++; } ?>
				<td class="text-center"><span class="blue"><?=$sr_no;?></span></td>
				<td class="bg"><span class="blue">1.350kgs <?=$product_name;?></span></td>
				<td class="text-center"><span class="blue"><?=$hsn;?></span></td>
				<td class="text-center"><span class="blue"><?=$qty;?></span></td>
				<td class="text-right"><span class="blue"><?=$unit_cost;?></span></td>
				<td class="text-center"><span class="blue"><?=$nos;?></span></td>
				<td class="text-right" align="right"><span class="blue"><?=$total_cost;?></span></td>
				</tr>
				<tr>
				<td></td>
				<td class="text-right"><b>Total</b></td>
				<td class="fentgreen"></td>
				<td class="fentgreen" align="center"><?=$final_qty;?></td>
				<td class="fentgreen"></td>
				<td class="fentgreen"></td>
				<td class="fentgreen" align="right"><?=$final_cost;?></td>
				</tr>
				
				<tr>
				<td colspan="4"><b>Two Thousand Four Hunderd Thirty Two only</b>
				
				<table class="table table-bordered-popup">
				<tbody>
				<tr>
				<td class="text-center"><span class="blue">HSN/SAC</span></td>
				<td class="text-center"><span class="blue">Taxable Value</span></td>
				<td colspan="2" class="text-center"><span class="blue">Central Tax</span> </td>
				<td colspan="2" class="text-center"><span class="blue">State Tax</span></td>
				</tr>
				
				<tr>
				<td></td>
				<td></td>
				<td><span class="blue">Rate</span> </td>
				<td><span class="blue">Amount</span></td>
				<td><span class="blue">Rate</span> </td>
				<td><span class="blue">Amount</span></td>
				</tr>
				<? 
				$total_amount = 0;
				$cgst_amount = 0;
				$sgst_amount = 0;
				$cgst_percent = 0;
				$sgst_percent = 0;
				foreach($order_detail['order_details'] as $val){ 
					$cgst_value = (($val['product_total_cost'] * $val['product_cgst'])/100);
					$sgst_value = (($val['product_total_cost'] * $val['product_sgst'])/100);
					$total_amount = $total_amount + $val['product_total_cost'];
					$cgst_amount = $cgst_amount + $cgst_value;
					$sgst_amount = $sgst_amount + $sgst_value;
					$cgst_percent = $cgst_percent + $val['product_cgst'];
					$sgst_percent = $sgst_percent + $val['product_sgst'];
				?>
				<tr>
				<td><span class="blue"><?=$val['producthsn'];?></span></td>
				<td class="text-right"><?=$val['product_total_cost'];?></span></td>
				<td><span class="blue"><?=$val['product_cgst'];?> %</span></td>
				<td class="text-right"><?=$cgst_value;?></span></td>
				<td><span class="blue"><?=$val['product_sgst'];?> %</span></td>
				<td class="text-right"><?=$sgst_value;?></span></td>
				</tr>
				<? } ?>
				<tr>
				<td class="text-right"><span class="blue">Total</span></td>
				<td class="text-right"><?=$total_amount;?></td>
				<td></td>
				<td class="text-right"><?=$cgst_amount;?></span></td>
				<td> </td>
				<td class="text-right"><?=$sgst_amount;?></span></td>
				</tr>
				
				</tbody>
				</table>
				
				</td>
				<td colspan="2" rowspan="2">
				<span style="display:inline-block; height:40px;">CGST @ <?=$cgst_percent;?> %</span><br/>
				<span style="display:inline-block; height:40px;">SGST @ <?=$sgst_percent;?> %</span><br/>
				<span style="display:inline-block; height:40px;">Rounding Off</span> <br/>
				<span style="display:inline-block; height:40px;">Grand Total </span><br/>
				<span style="display:inline-block; height:40px;">Opening Balance</span> <br/>
				<span style="display:inline-block; height:40px;">Closing Balance </span>
				</td>
				<td rowspan="2" class="text-right">
				<span style="display:inline-block; height:40px;"> <?=$cgst_amount;?></span><br/>
				<span style="display:inline-block; height:40px;"><?=$sgst_amount;?></span><br/>
				<span style="display:inline-block; height:40px;">0.04</span><br/>
				<?
					$gst_total_amount = $total_amount + $cgst_amount + $sgst_amount;
					$opening_balance = 0;
					$closing_balance = $gst_total_amount + $opening_balance ;
				?>
				<span style="display:inline-block; height:40px;"><?=$gst_total_amount;?></span><br/>
				<span style="display:inline-block; height:40px;"><?=$opening_balance;?></span><br/>
				<span style="display:inline-block; height:40px;"><?=$closing_balance;?></span>
				</td>
				</tr>
				
				<tr>
				<td colspan="2" width="30%">
				<u>Declaration:</u><br/>
				<?=$admin_details['declaration'];?>
				</td>
				<td><div class="text-center"><b><u>BANK DETAILS</u></b></div>
				<div class="col-md-5 np">BANK NAME:</div> <?=$admin_details['accbrnm'];?>,<br/>
				<div class="col-md-5 np">BRANCH:</div> SALEM MAIN, FIVE ROADS<br/>
				<div class="col-md-5 np">CC A/C NO.:</div> <?=$admin_details['accno'];?><br/>
				<div class="col-md-5 np">IFSC CODE:</div> <?=$admin_details['accifsc'];?></td>
				<td  class="fentgreen font-big1">For <b>SRI JAYA SHREE FOOD PRODUCTIONS</b><br/><br/><br/>
				Authorised Signature
				</td>
				</tr>
				</tbody>
				
				</table>
</div>

</div>
</div>
</div>