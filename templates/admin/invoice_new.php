<? 
include ("../../includes/config.php");
include ("../includes/common.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
include "../includes/shopManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$admin_details_basic = $userObj->getLocalUserDetails($_SESSION[SESSION_PREFIX.'user_id']);
$admin_details = $userObj->getLocalUserOtherDetails($_SESSION[SESSION_PREFIX.'user_id'],'Admin');
$order_id = $_POST['order_id'];
$order_status = $_POST['order_status'];
$order_details = $orderObj->getOrders($order_status, $order_id);
//$product_variant = $orderObj->getSProductVariant($order_details['product_variant_id']);
//print"<pre>";
//print_r($order_details);
$order_detail = $order_details[1];
$shop_details = $shopObj->getShopDetails($order_detail['shop_id']);//Shop details
$opening_bal_details = $orderObj->get_opening_balance($order_detail['shop_id']);
$opening_balance = 0;
if($opening_bal_details['amount_to_pay'] !=''){
	$opening_balance = $opening_bal_details['amount_to_pay'] ;
}
$colspan3 = '3';
$colspan2 = '2';
if($order_detail['offer_provided'] != null && $order_detail['offer_provided'] == 0){ 
	$colspan3 = '4';
	$colspan2 = '3';
 } 
?>
    <style>
        .table-bordered-popup {
            border: 1px solid #364622;
        }
        
        .table-bordered-popup > tbody > tr > td,
        .table-bordered-popup > tbody > tr > th,
        .table-bordered-popup > thead > tr > td,
        .table-bordered-popup > thead > tr > th {
            border: 1px solid #364622;
            color: #4a5036;
        }
        
        .np {
            padding-left: 0px;
            padding-right: 0px;
        }
        
        .greybg {
            background-color: #eeeeee;
        }
        
        .mb5 {
            margin-bottom: 5px;
        }
        
        .tlt {
            border-bottom: 1px solid #ccc;
        }
        
        .mt0 {
            margin-top: 0px;
        }
        
        .br {
            border: 1px solid #ddd;
        }
        
        .br1 {
            border-bottom: 1px solid #ddd;
            padding-bottom: 8px;
            margin-bottom: 0px;
        }
        
        .br2 {
            border-right: 1px solid #ddd;
            padding-top: 8px;
        }
        
        .rotate {
            /* Safari */
            -webkit-transform: rotate(-90deg);
            /* Firefox */
            -moz-transform: rotate(-90deg);
            /* IE */
            -ms-transform: rotate(-90deg);
            /* Opera */
            -o-transform: rotate(-90deg);
            /* Internet Explorer */
            filter: progid: DXImageTransform.Microsoft.BasicImage(rotation=3);
            width: 8px;
        }
    </style>
    <div class="modal-header">
        <button type="button" name="btnPrint1" id="btnPrint1" onclick="takeprint_invoice('<?=SITEURL;?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
    </div>
    <div class="modal-body" style="padding-bottom: 5px !important;" id="divPrintArea1">
        <div class="row">
            <div class="col-md-12">

                <div class="text-center tlt br">
                    <h2><b>CHETRAN FOODS PRIVATE LIMITED</b></h2>
                    <h5>(Manufactures of soya dairy and non dairy foods)</h5>
					
                </div>

                <div class="br">

                    <div class="col-md-4 np">
                        <h4 class="br1"><b>DETAILS OF SELLER</b></h4>

                        <div class="col-md-12 br2">
                            <div class="col-md-4 np">
                                <label>GSTIN</label>
                            </div>
                            <div class="col-md-8 np">27AABCC9525C1ZV</div>
                            <div class="clearfix"></div>

                            <div class="col-md-4 np">
                                <label>NAME</label>
                            </div>
                            <div class="col-md-8 np">Chetran Foods Pvt Ltd</div>
                            <div class="clearfix"></div>

                            <div class="col-md-4 np">
                                <label>ADDRESS</label>
                            </div>
                            <div class="col-md-8 np">Chintamani Estate, Ramtekdi Indl Area, hadapsar, Pune 411013. TEl 02065221863</div>
                            <div class="clearfix"></div>

                            <div class="col-md-4 np">
                                <label>STATE(CD)</label>
                            </div>
                            <div class="col-md-8 np">MAHARASHTRA (27)</div>
                            <div class="clearfix"></div>

                            <div class="col-md-4 np">
                                <label>TAX INVOICE NO:</label>
                            </div>
                            <div class="col-md-8 np">
                                <h4 class="mt0"><b>010036</b></h4></div>
                            <div class="clearfix"></div>

                        </div>

                    </div>

                    <div class="col-md-8 np">
                        <h4 class="br1"><b>DETAILS OF RECEIVER CUM CONSIGNEE</b></h4>

                        <div class="col-md-12">
                            <!--  <div class="col-md-4 np"><label>Name</label></div>
                <div class="col-md-8 np"><?=$shop_details['name'];?></div>
                <div class="clearfix"></div> -->
                            <div class="col-md-3 np">
                                <label>NAME</label>
                            </div>
                            <div class="col-md-3 np">
                                <?=$shop_details['name'];?>
                            </div>
                            <div class="col-md-3 np"></div>
                            <div class="col-md-3 np"></div>
                            <div class="clearfix"></div>

                            <div class="col-md-3 np">
                                <label>ADDRESS</label>
                            </div>
                            <div class="col-md-3 np">
                                <?=$shop_details['address'];?>
                            </div>
                            <div class="col-md-3 np" style="text-align: center;">PO NO.</div>
                            <div class="col-md-3 np"> - </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3 np">
                                <label>CITY</label>
                            </div>
                            <div class="col-md-3 np">
                                <?=$shop_details['city_name'];?>
                            </div>
                            <div class="col-md-3 np" style="text-align: center;">GSTIN</div>
                            <div class="col-md-3 np">
                                <?=$shop_details['gst_number'];?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-3 np">
                                <label>STATE</label>
                            </div>
                            <div class="col-md-3 np">
                                <?=$shop_details['state_name'];?>
                                    </br>( Code :
                                    <?=$shop_details['state'];?> )</div>
                            <div class="col-md-3 np" style="text-align: center;">DATE</div>
                            <div class="col-md-3 np">
                                <?=date("d/m/Y", strtotime($order_detail['order_date']));?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                </div>

                <div class="clearfix"></div>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="text-center">HSN</th>
                            <th class="text-center">Description of Goods</th>
                            <th class="text-center">EAN</th>
                            <th class="text-center rotate">MRP</th>
                            <th class="text-center">Qty PC</th>
                            <th colspan="2" class="text-center">Rate ₹</th>
                            <th colspan="2" class="text-center">Taxable ₹ Value</th>
                            <th class="text-center rotate">Rate</th>
                            <th class="text-center">CGST Amount ₹</th>
                            <th class="text-center rotate">Rate</th>
                            <th class="text-center">SGCT Amount ₹</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
								$i = 1;
								$final_qty = 0;
								$final_cost = 0;
								$total_amount = 0;
								$cgst_amount = 0;
								$sgst_amount = 0;
								$cgst_percent = 0;
								$sgst_percent = 0;
								foreach($order_detail['order_details'] as $val)
								{
									//echo "<pre>";
									//print_r($val);
								$sr_no.=$i."<br><br>";
								$product_name.=$val['product_name']." ".$val['product_variant_weight1']." ".$val['product_variant_unit1']."<br><br>";
								$hsn.=$val['producthsn']."<br><br>";
								$qty.=$val['product_quantity']."<br><br>";
								$final_qty = $final_qty + $val['product_quantity'];
								$unit_cost.=$val['product_unit_cost']."<br><br>";
								$nos.="nos<br><br>";
								if($val['campaign_applied'] == 0)
								{ 
								$discounted_amt.= ($val['discount_amount'] * $val['product_quantity'])."<br><br>";
								}
								$total_cost.=$val['product_total_cost']."<br><br>";
								$final_cost = $final_cost + $val['product_total_cost'];
                                $product_sgst .= $val['product_sgst']."<br><br>";
                                $product_cgst .= $val['product_cgst']."<br><br>";

								$product_sgst1 = $val['product_sgst'];
                                $product_cgst1 = $val['product_cgst'];
								$cgst_value = (($val['product_total_cost'] * $val['product_cgst'])/100);
								$sgst_value = (($val['product_total_cost'] * $val['product_sgst'])/100);
								$cgst_value1 .= $cgst_value."<br><br>"; 
								$sgst_value1 .= $sgst_value."<br><br>";
								$total_amount = $total_amount + $val['product_total_cost'];
								$cgst_amount = $cgst_amount + $cgst_value;
								$sgst_amount = $sgst_amount + $sgst_value;
								$cgst_percent = $cgst_percent + $val['product_cgst'];
								$sgst_percent = $sgst_percent + $val['product_sgst'];
								$i++; 
							} 

							$gst_total_amount = $total_amount + $cgst_amount + $sgst_amount;
							$gst_total_amount_round = ceil($gst_total_amount);
							$round_val = $gst_total_amount_round - $gst_total_amount;
							$closing_balance = $gst_total_amount_round + $opening_balance ;

							    foreach($order_detail['order_details'] as $val)
								{ 
								$hsn=$val['producthsn'];
								$product_name=$val['product_name']." ".$val['product_variant_weight1']." ".$val['product_variant_unit1'];
								$unit_cost=$val['product_unit_cost'];
								$qty=$val['product_quantity'];
								$product_cgst = $val['product_cgst'];
								$cgst_value = (($val['product_total_cost'] * $val['product_cgst'])/100);
								$cgst_value1 = $cgst_value;
								$product_sgst = $val['product_sgst'];
								 $sgst_value = (($val['product_total_cost'] * $val['product_sgst'])/100);
								 $sgst_value1 = $sgst_value;
								 if ($product_cgst <=5) 
								 {                               
								?>
                            <tr>
                                <td>
                                    <?=$hsn;?>
                                </td>
                                <td>
                                    <?=$product_name;?>
                                </td>
                                <td>-</td>
                                <td>
                                    <?=$unit_cost;?>
                                </td>
                                <td>
                                    <?=$qty;?>
                                </td>

                                <td></td>
                                <td></td>

                                <td></td>
                                <td></td>

                                <td>
                                    <?=$product_cgst;?>
                                </td>
                                <td class="greybg">
                                    <?=$cgst_value1;?>
                                </td>
                                <td>
                                    <?=$product_sgst;?>
                                </td>
                                <td class="greybg">
                                    <?=$sgst_value1;?>
                                </td>
                            </tr>
                            <?php }
						   }                       
                            ?>
                                <tr>
                                    <td colspan="4" class="greybg"></td>
                                    <th colspan="5">SUBTOTAL 5% GST</th>
                                    <td class="greybg"></td>
                                    <td></td>
                                    <td class="greybg"></td>
                                    <td></td>
                                </tr>
                                <?php
							    foreach($order_detail['order_details'] as $val)
								{ 
								$hsn=$val['producthsn'];
								$product_name=$val['product_name']." ".$val['product_variant_weight1']." ".$val['product_variant_unit1'];
								$unit_cost=$val['product_unit_cost'];
								$qty=$val['product_quantity'];
								$product_cgst = $val['product_cgst'];
								$cgst_value = (($val['product_total_cost'] * $val['product_cgst'])/100);
								$cgst_value1 = $cgst_value;
								$product_sgst = $val['product_sgst'];
								 $sgst_value = (($val['product_total_cost'] * $val['product_sgst'])/100);
								 $sgst_value1 = $sgst_value;
								 if ($product_cgst >5) 
								 {                                   
								?>
                                    <tr>
                                        <td>
                                            <?=$hsn;?>
                                        </td>
                                        <td>
                                            <?=$product_name;?>
                                        </td>
                                        <td>-</td>
                                        <td>
                                            <?=$unit_cost;?>
                                        </td>
                                        <td>
                                            <?=$qty;?>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <?=$product_cgst;?>
                                        </td>
                                        <td class="greybg">
                                            <?=$cgst_value1;?>
                                        </td>
                                        <td>
                                            <?=$product_sgst;?>
                                        </td>
                                        <td class="greybg">
                                            <?=$sgst_value1;?>
                                        </td>
                                    </tr>
                                    <?php 
							}
						    }	                          
                            ?>
                                        <tr>
                                            <td colspan="4"></td>
                                            <th colspan="5">SUBTOTAL 12% GST</th>
                                            <td class="greybg"></td>
                                            <td></td>
                                            <td class="greybg"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4"></td>
                                            <th colspan="5">Total</th>
                                            <td class="greybg"></td>
                                            <td></td>
                                            <td class="greybg"></td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <th colspan="9" class="text-right">Bill Amount</th>
                                            <td class="greybg"></td>
                                            <td></td>
                                            <td class="greybg"></td>
                                            <td></td>
                                        </tr>
                    </tbody>
                </table>
                <div class="clearfix"></div>
                <div class="col-md-5 pull-right">
                    <p class="mb5">SIGNATURE</p>
                    <p class="mb5">(For CHETRAN FOODS PVT. LTD)</p>
                    <p class="mb5">Packing & Forwarding, Freight, Insurance = NIL</p>
                    <p class="mb5">THIS Invoice Is not For IGST Transactions - Inter State Or Export</p>
                    <p class="mb5">All Foods Mentioned in this Invoice are warranted to be of the Nature and Quality they purport to be</p>
                    <p class="mb5">FSSAI Lisense No 11516036000917</p>
                </div>
            </div>
        </div>
    </div>