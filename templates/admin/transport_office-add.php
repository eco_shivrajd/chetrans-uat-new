<!-- BEGIN HEADER -->
<?php include "../includes/header.php"; 
include "../includes/commonManage.php";
include "../includes/transportManage.php";
$tObj 		= 	new transportManager($con,$conmain);

?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageTransport"; $activeMenu = "TransportOffices";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Transport Offices
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">
					 
					<li>
						<i class="fa fa-home"></i>
						<a href="transport_offices.php">Transport Offices</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Add New Transport Office</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Add New Transport Offices
							</div>
							
						</div>
						<div class="portlet-body">
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
<?php
if(isset($_POST['submit']))
{
	$tObj->addTransportOfficeDetails();
	echo '<script>alert("Transport Office added successfully.");location.href="transport_offices.php";</script>';
}
?>  
<form class="form-horizontal" data-parsley-validate="" role="form" method="post" action="transport_office-add.php">         
            <div class="form-group">
              <label class="col-md-3">Transport Office Name:<span class="mandatory">*</span></label>
              <div class="col-md-4">
                <input type="text" 
				placeholder="Enter Transport Office Name"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter Transport Office name"
				data-parsley-maxlength="50"
				data-parsley-maxlength-message="Only 50 characters are allowed"
				name="trans_off_name"class="form-control">
              </div>
            </div><!-- /.form-group -->       
			<div class="form-group">
				  <label class="col-md-3">Transport Type:<span class="mandatory">*</span></label>
				  <div class="col-md-4">								
						<select name="transport_type" id="transport_type" class="form-control"
						data-parsley-trigger="change"				
						data-parsley-required="#true" 
						data-parsley-required-message="Please select Transport Type">
							<option selected disabled>-select-</option>
							<?php
							$sql="SELECT `id`, `transport_name` FROM tbl_transport_type WHERE status = '0' ORDER BY transport_name";
							$result = mysqli_query($con,$sql);
							while($row = mysqli_fetch_array($result))
							{
								$cat_id=$row['id'];
								echo "<option value='$cat_id'>" . $row['transport_name'] . "</option>";
							} ?>
						</select>
				  </div>
				</div><!-- /.form-group -->	
            <div class="form-group">
              <label class="col-md-3">Address:<span class="mandatory">*</span></label>

              <div class="col-md-4">
                <textarea name="address" 
				rows="4"
                placeholder="Enter Address"
                data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please enter address"
				data-parsley-maxlength="200"
				data-parsley-maxlength-message="Only 200 characters are allowed"
				class="form-control"></textarea>
              </div>
            </div><!-- /.form-group -->
            <div class="form-group">
				<label class="col-md-3">State:<span class="mandatory">*</span></label>
				<div class="col-md-4">
				<select name="state_id"              
				data-parsley-trigger="change"				
				data-parsley-required="#true" 
				data-parsley-required-message="Please select state"
				class="form-control" onChange="fnShowCity(this.value)">
				<option selected disabled>-select-</option>
				<?php
				$sql="SELECT id,name FROM tbl_state where country_id=101 order by name";
				$result = mysqli_query($con,$sql);
				while($row = mysqli_fetch_array($result))
				{
					$cat_id=$row['id'];
					echo "<option value='$cat_id'>" . $row['name'] . "</option>";
				} ?>
				</select>
				</div>
				</div>			
				<div class="form-group" id="city_div" style="display:none;">
				  <label class="col-md-3">City:<span class="mandatory">*</span></label>
				  <div class="col-md-4" id="div_select_city">
				  <select name="city_id" id="city_id" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>										
					</select>
				  </div>
				</div><!-- /.form-group -->
				<div class="form-group" id="area_div" style="display:none;">
				  <label class="col-md-3">Region:<span class="mandatory">*</span></label>
				  <div class="col-md-4" id="div_select_area">
				  <select name="suburb_id" id="suburb_id" data-parsley-trigger="change" class="form-control">
					<option selected value="">-Select-</option>									
					</select>
				  </div>
				</div>
				<div class="form-group">
				  <label class="col-md-3">Status:</label>
				  <div class="col-md-4">
				  <div class="input-group">					
						<select name="status" id="status" class="form-control">
							<option value="Active">Active</option>
							<option value="Inactive">Inactive</option>
						</select>
					</div>
				  </div>
				</div><!-- /.form-group -->	
            <div class="form-group">
              <div class="col-md-4 col-md-offset-3">
               <button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
                <a href="transport_offices.php" class="btn btn-primary">Cancel</a>
              </div>
            </div><!-- /.form-group -->
          </form>                                       
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<style>
.form-horizontal{
font-weight:normal;
}
</style>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>  
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();		
	$("#subarea").html('<option value="">-Select-</option>');	
	//var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown&mandatory=mandatory";
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&mandatory=mandatory";
	CallAJAX(url,"div_select_area");
}
</script>            
