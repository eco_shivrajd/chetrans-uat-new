<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);
//$userObj->migration_sp_sstockist(); exit;
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "SalesPerson";
	include "../includes/sidebar.php"
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			
			<h3 class="page-title">
			Manage Sales Person
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Sales Person</a>
					</li>
				</ul>
				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
                
            
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Manage Sales Person
							</div>
                            <a href="sales-add.php" class="btn btn-sm btn-default pull-right mt5">
                                Add Sales Person
                              </a>
                              <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr>
								<th width="15%">
									 Name
								</th>	
								<th width="25%">
									Assigned Region
								</th>								
                                <th width="20%">
									 Email
								</th>
                                <th width="10%">
									 Mobile Number
								</th>
								 <th width="10%">
									Status
								</th>
								<th width="10%">
                                	Action
                                </th>
							</tr>
							</thead>
							<tbody>
							<?php 
							$user_type='SalesPerson';
							$result1 = $userObj->getAllLocalUserDetails($user_type,1);	
							$assigned_regions = $userObj->getAssignedRegion();
							$assigned_regions_array = explode(',',$assigned_regions['assigned_regions']);
							if($result1 > 0 ){							
								while($row = mysqli_fetch_array($result1))
								{ 
									$user_id=$row['id'];
									echo '<tr class="odd gradeX">
									<td>
										 <a href="sales1.php?id='.$row['id'].'">'.fnStringToHTML($row['firstname']).'</a>
									</td>'; 
									$assigned_region='';//getLocalUserWorkingRegionDetails
									$resultarea = $userObj->getLocalUserWorkingRegionDetails($user_id);
									
									if(!empty($resultarea['suburb_ids']) && $resultarea != 0){									
										$suburbs = str_replace(',,',',',$resultarea['suburb_ids']);
										$suburbs = rtrim($suburbs,",");
										$suburbs = ltrim($suburbs,",");
										$sql_s_name="SELECT GROUP_CONCAT(suburbnm) AS all_suburb FROM tbl_surb where id IN(".$suburbs.")";
										$result_s_name = mysqli_query($con,$sql_s_name);
										$suburb_count = mysqli_num_rows($result_s_name);
										$row_suburb = mysqli_fetch_assoc($result_s_name);
										$assigned_region= $row_suburb['all_suburb'];
									}else{
										$assigned_region= '-';
									}
									echo '<td>'.$assigned_region.'</td>
									<td>'.$row['email'].'</td>
									<td>'.$row['mobile'].'</td>';
									?>
								<div id="region_dropdown_<?=$row['id'];?>" style="display:none;"> 
								<select name="assign[]" multiple class="form-control">				 
								<?php									
								$sql_area="SELECT `id`, `cityid`, `stateid`, `suburbnm` FROM tbl_surb WHERE isdeleted != 1 ORDER BY suburbnm";
								$result_area = mysqli_query($con,$sql_area);
								if(mysqli_num_rows($result_area) > 1){
									while($row_area = mysqli_fetch_array($result_area))
									{
										$assign_id=$row_area['id'];
										$suburb_ids = explode(',',$resultarea['suburb_ids']);
										$bg_color = '';
										$disabled = '';
										$title = '';
										$sel="";
										if(in_array($assign_id,$suburb_ids)){
											$sel="SELECTED";
											$title = 'Assigned to selected Sales Person';
										}
										else{
											
											if(in_array($assign_id,$assigned_regions_array)){											
												$bg_color = 'background-color: #5E8C59; color: #ffffff;';
												$disabled = 'disabled';
												$title = 'Region already Assigned';
											}
										}
										echo "<option value='$assign_id' title='$title' $sel $disabled style='$bg_color'>" . fnStringToHTML($row_area['suburbnm']) . "</option>";
									}
								}	?>
								</select>
							</div>
									<?php 
									echo '<td>'.$row['user_status'].'</td>';
									$assign_link = '<a onclick="javascript: assign_region('.$row['id'].')">Assign</a>';
									$delete_link =	'<a href="manageuser.php?utype=SalesPerson&id='.$row['id'].'">Delete</a>';
									echo '<td>'.$assign_link.'/ '.$delete_link.'</td>';
									echo '</tr>';
								}
							}
							?>
							</tbody>
							</table>
						</div>
					</div>
            
				
                    
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>

</div>
<div class="modal fade" id="assignRegionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Assign Region</h4>	   
      </div>		
    <form role="form" class="form-horizontal" onsubmit="return false;" action="sales.php" data-parsley-validate="" name="assign_region_form" id="assign_region_form">
      <div class="modal-body" >	  
	  <div class="clearfix"></div>
			<div class="form-group">
			<label class="col-md-3">Select Region:</label>
			<div class="col-md-4" id="show_region_dropdown">
				
			</div>
			</div><!-- /.form-group --> 			
			<div class="form-group">
				<div class="col-md-4 col-md-offset-3">					
					<button type="submit"  name="btnsubmit"  class="btn btn-primary">Submit</button>
					<a href="sales.php" class="btn btn-primary">Cancel</a>
				</div>
			</div><!-- /.form-group -->
			<input type="hidden" name="sales_p_id" id="sales_p_id" value="0"/>
			<input type="hidden" name="action" id="action" value="assign_region"/>
		</div><!-- /.form-group --> 				
      </div>
	   </form>	   	  
    </div>
  </div>
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script>
function assign_region(sales_p_id){	
	$('#sales_p_id').val(sales_p_id);
	var select_drop = $('#region_dropdown_'+sales_p_id).html();
	$('#show_region_dropdown').html(select_drop);
	$('#assignRegionModal').modal('show');
}
$('form#assign_region_form').submit(function(){
	var formData = new FormData($(this)[0]);	
	$.ajax({
		url:"../includes/assign_region.php",
		type: 'POST',
		data: formData,
		success: function (data) {
			if(data == 1){
				alert('Region assigned successfully');
				$('#assignRegionModal').modal('hide');
				document.forms.assign_region_form.reset();
				window.location.reload(true);
			}else if(data == 'select'){
				alert('Please select Region');
				return false;
			}else{
				alert('Unable to assign Region');
				return false;
			}					
		},
		cache: false,
		contentType: false,
		processData: false
	});
});
</script>
</body>
<!-- END BODY -->
</html>