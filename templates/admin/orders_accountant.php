<!-- BEGIN HEADER -->
<?php include "../includes/grid_header.php";
include_once "../includes/common.php";
include "../includes/userManage.php";	
include "../includes/orderManage.php";
include "../includes/shopManage.php";	
include "../includes/productManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
$shopObj 	= 	new shopManager($con,$conmain);
$prodObj 	= 	new productManage($con,$conmain);
if(isset($_POST['hidbtnsubmit_order_pay']))
{
	//print"<pre>";print_R($_POST);
	
	$result = $orderObj->order_payment_update();
	if($result == true){
		echo '<script>alert("Payment updated successfully.");location.href="orders_accountant.php";</script>';
	}
}
?>
<!-- BEGIN PAGE HEADER-->
</head>
<!-- END HEAD -->

<script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=GOOGLEAPIKEY;?>&callback=initMap" type="text/javascript"></script>
  
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Orders"; $activeMenu = "Orders";
	
	if($recieved=="admin") {
		$activeMenu = "OrderVisibility";
	}
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<div class="page-content-wrapper">
		<div class="page-content">			
			<h3 class="page-title">
			Manage Orders
			</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="#">Orders</a>
					</li>
				</ul>				
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Orders 
							</div>
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">						
							<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post">
							<div class="form-group">
							  <label class="col-md-3">Order Status:</label>
							  <div class="col-md-4">
							  <select name="order_status" id="order_status" class="form-control">
								<option value="">-Select-</option>							
								<option value="4" selected>Delivered</option>
								<option value="6">Payment Received</option>
								<!--<option value="7">Payment Pending</option>-->
								<option value="8">Partial Payment Received</option>									
								</select>
							  </div>
							</div><!-- /.form-group -->														
							<div class="form-group" id="divDaily">
								<label class="col-md-3">Select Order Date:</label>
								<div class="col-md-4">
									<div class="input-group">
										<input type="text" class="form-control  date date-picker1" data-date="<?php echo date('d-m-Y');?>" data-date-format="dd-mm-yyyy" data-date-viewmode="years" name="frmdate" id="frmdate" value="">
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>
									<!-- /input-group -->								 
								</div>
							</div><!-- /.form-group -->							
							<div class="form-group">
								<label class="col-md-3">Sales Person:</label>
								<?php $user_result = $userObj->getAllLocalUser('salesperson'); ?>		
								<div class="col-md-4" id="divsalespersonDropdown">
									<select name="dropdownSalesPerson" id="dropdownSalesPerson" class="form-control">
										<option value="">-Select-</option>
										<?php while($row_user = mysqli_fetch_assoc($user_result))
										{ ?>									
										<option value="<?=$row_user['id'];?>"><?=$row_user['firstname'];?></option>
										<?php } ?>
									</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">State</label>
								<div class="col-md-4">
								<?php
									$sql_state="SELECT * FROM tbl_state where country_id=101";					
									$result_state = mysqli_query($con,$sql_state);		
								?>
									<select id="dropdownState" name="dropdownState" size="1" class="form-control" onChange="fnShowCity(this.value)">
									<option value="">-Select-</option>
									<?php
									while($row_state = mysqli_fetch_array($result_state))
									{?>
										<option value=<?=$row_state['id'];?>><?=fnStringToHTML($row_state['name']);?></option>
									<?php }	?>												
									</select>
								</div>
							</div>
							<div class="form-group" id="city_div">
								<label class="col-md-3">City</label>
								<div class="col-md-4" id="div_select_city">
									<select id="citySelect" size="1" class="form-control">
									<option value="">-Select-</option>									
									</select>
								</div>
							</div>
							 <div class="form-group" id="area_div">
								<label class="col-md-3">Region</label>
								<div class="col-md-4" id="div_select_area">
								 <select class="form-control"  id="subarea">
									<option value="">-Select-</option>									
								 </select>
								 </div>
							</div> 
							<div class="form-group" id="shop_div">
								<label class="col-md-3">Shops:</label>
								<div class="col-md-4">
								<?php $shop_result = $shopObj->getAllShops(); ?>									
								 <select name="divShopdropdown" id="divShopdropdown" class="form-control">
									<option value="">-Select-</option>
									<?php while($row_shop = mysqli_fetch_assoc($shop_result))
									{ ?>									
									<option value="<?=$row_shop['id'];?>"><?=$row_shop['name'];?></option>
									<?php } ?>
								</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">Category:</label>
								<div class="col-md-4" id="divCategoryDropDown">
								<?php $cat_result = $prodObj->getAllCategory(); ?>
								 <select name="dropdownCategory" id="dropdownCategory" class="form-control" onchange="fnShowProducts(this)">
									<option value="">-Select-</option>
									<?php while($row_cat = mysqli_fetch_assoc($cat_result))
									{ ?>									
									<option value="<?=$row_cat['id'];?>"><?=$row_cat['categorynm'];?></option>
									<?php } ?>								
									</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<label class="col-md-3">Product:</label>
								<div class="col-md-4" id="divProductdropdown">
								<?php $prod_result = $prodObj->getAllProducts(); ?>
								 <select name="dropdownProducts" id="dropdownProducts" class="form-control">
									<option value="">-Select-</option>
									<?php while($row_prod = mysqli_fetch_assoc($prod_result))
									{ ?>									
									<option value="<?=$row_prod['id'];?>"><?=$row_prod['productname'];?></option>
									<?php } ?>	
								</select>
								</div>
							</div><!-- /.form-group -->
							<div class="form-group">
								<div class="col-md-4 col-md-offset-3">
									<button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" onclick="ShowReport();">Search</button>									
									<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary" onclick="fnReset();">Reset</button>
								</div>
							</div><!-- /.form-group -->
						</form>	
							<? if($_POST['order_status'] != 6){//6 is payment done?>
							<div>
								<button type="button" class="btn btn-success" name="statusbtn" id="statusbtn" onclick="setPayment()">Set Order Payment</button>
							</div><br>
							<? } ?>
							<form class="form-horizontal" role="form" name="form_list" id="form_list" method="post" action="">	
							<div id="order_list">
							<?php
							$order_status = 4;//Delivered;
							$orders = $orderObj->getOrders($order_status);//print"<pre>";print_R($orders);
							$order_count = count($orders);
							?>
							<table class="table table-striped table-bordered table-hover" id="sample_2">
							<thead>
							<tr id="main_th">
								<?php if($order_count != 0) { ?>
								<th id="select_th">
									<input type="checkbox" name="select_all[]" value=""  id="select_all" onchange="javascript:checktotalchecked(this)">
								</th>
								<?php } ?>
								<th>
									Region
								</th>
								<th>
									Regional Head
								</th>
								<th>
									Shop Name
								</th>
								<th>
									Order Id
								</th>								
                                <th>
									Order Date 
								</th>	
								 <th>
									Category
								</th>	
								<th>
									Product
								</th>	
								<th>
									Quantity
								</th>
								<th>
									Price (₹)
								</th>
								<th>
									GST Price (₹)
								</th>
								<th>
									Action  
								</th>
							</tr>
							</thead>
							<tbody>
							<?php
							
							foreach($orders as $val_order){
								?>
							<tr class="odd gradeX">
							   <td>
									 <input type="checkbox" disabled name="select_all[]" id="select_all" value="ordermain_<?=$val_order['id'];?>" onchange="javascript:checktotalchecked(this)">
								</td>
								<td>
									 <?=$val_order['region_name'];//region?>
								</td>
								<td>
									 <?=$val_order['order_by_name'];?>
								</td>
								<td>
									 <?=$val_order['shop_name'];?>
								</td>
								<td colspan="7">
								</td>
								<td style="display: none;"><!--This display: none code is to remove the datatable
								error, datatable doesnot allow to use colspan-->
								</td>
								<td style="display: none;">
								</td>
								<td style="display: none;">
								</td>
								<td style="display: none;">
								</td>
								<td style="display: none;">
								</td>
								<td style="display: none;">
								</td>
								<td>									
									<a onclick="showInvoice(4,<?=$val_order['id'];?>)" title="View Invoice">view</a> 
								</td>								
							</tr>
							<?php 
									if(count($val_order['order_details']) > 0){
										foreach($val_order['order_details'] as $val){
											$product_variant = $orderObj->getSProductVariant($val['product_variant_id']);
										?>
										<tr class="odd gradeX">
											<td>
												<input type="checkbox" name="select_all[]" id="select_all" value="ordersub_<?=$val_order['id'];?>_<?=$val['id'];?>"  onchange="javascript:checktotalchecked(this)">
											</td>
											<td colspan="3">
											</td>
											<td style="display: none;">
											</td>
											<td style="display: none;">
											</td>
											<td style="display: none;">
											</td>
											<td>
												<?=$val_order['order_no'];?>
											</td> 
											<td>
												<?=$val_order['order_date'];?>
											</td>
											<td>
												 <?=$val['cat_name'];?>
											</td>
											<td>
												 <?=$val['product_name'].' '.$product_variant;?>
											</td>
											<td align="right">
												 <?=$val['product_quantity'];?>
											</td>
											<td align="right">
												<?=$val['product_total_cost'];?>
											</td>
											<td align="right">
												<?=$val['p_cost_cgst_sgst'];?>
											</td>
											<td>						
												<a onclick="showOrderDetails(<?=$val['id'];?>,'Order Details')" title="Order Details">Details</a> 
											</td>									
										</tr>
										
									<?php 
										}
									}						
							} ?>
							
							</tbody>
							</table>
							</form>
							 </div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
				
			</div>
<!-- END PAGE CONTENT-->
<div class="modal fade" id="view_invoice" role="dialog">
	<div class="modal-dialog" style="width: 980px !important;">    
		<!-- Modal content-->
		<div class="modal-content" id="view_invoice_content">      
		</div>      
	</div>
</div>
<div class="modal fade" id="order_details" role="dialog">
    <div class="modal-dialog" style="width: 880px !important;">
      <!-- Modal content-->
      <div class="modal-content" id="order_details_content">
        
        </div>
      </div>
	</div>
</div>
<div class="modal fade" id="googleMapPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog" role="document">
    <div class="modal-content" id="model_content">
      <div class="modal-header">
	  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
      </div>
		<div class="modal-body" style="padding-bottom: 5px !important;"> 
		<div id="map" style="width: 100%; height: 500px;"></div> 
		</div>
	</div>
</div>
</div>

<div class="modal fade" id="set_payment" role="dialog" style="height:auto;">
    <div class="modal-dialog">    
  <!-- Modal content-->
  <div class="modal-content" id="set_payment_content" style="height:auto;">
	
  </div>
  </div>
</div>
<!-- BEGIN FOOTER -->
<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<!-- START JAVASCRIPTS -->
<script>
$(document).ready(function() {
	$("#select_th").removeAttr("class");
	$("#main_th th").removeAttr("class");
	if ( $.fn.dataTable.isDataTable( '#sample_2' ) ) {
    table = $('#sample_2').DataTable();
	table.destroy();
		table = $('#sample_2').DataTable( {
			"aaSorting": [],
		} );
	}
});
$('.date-picker1').datepicker({
	rtl: Metronic.isRTL(),
	orientation: "left",
	endDate: "<?php echo date('d-m-Y');?>",
	autoclose: true
});
function fnReset() {
	location.reload();
}
function fnShowCity(id_value) {	
	$("#city_div").show();	
	$("#area").html('<option value="">-Select-</option>');	
	$("#subarea").html('<option value="">-Select-</option>');	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city&mandatory=mandatory";
	CallAJAX(url,"div_select_city");	
	FnGetShopsDropdown("");
}
function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');		
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetShopsDropdown";
	CallAJAX(url,"div_select_area");
	FnGetShopsDropdown("");
}
function FnGetShopsDropdown(id) {
	$("#shop_div").show(); 	
	$("#divShopdropdown").html('<option value="">-Select-</option>');
	var param = "";
	var state_id = $("#dropdownState").val();
	var city_id = $("#city").val();
	var suburb_id = $("#area").val();
	if(state_id != '')
		param = param + "&state_id="+state_id ;
	if(city_id != '')
		param = param + "&city_id="+city_id ;
	if(suburb_id != ''){
		if(suburb_id != undefined){
			param = param + "&suburb_id="+suburb_id ;
		}
	}
	if(id != ''){
		if(id != undefined)
			param = param + "&suburb_id="+id.value ;
	}
	
	var url = "getShopDropdownByAddress.php?param=param"+param;
	CallAJAX(url,"divShopdropdown");
}
function fnShowProducts(id) {
	var url = "getProductDropdown.php?cat_id="+id.value;
	CallAJAX(url,"divProductdropdown");
}
function showorder(obj){
	window.location = obj.value+".php";
}
$("#select_all").click(function(){
    $('input:checkbox').prop('checked', this.checked);
});
function checktotalchecked(obj){
	$("#select_th").removeAttr("class");
	if($(obj).is(':checked') == true)
	{		
		var checkbox_count = ($('input:checkbox').length) - 1;
		var check_count = ($('input:checkbox:checked').length);
		if(obj.value == ''){
			$('input:checkbox').attr('checked', true);
		}
		else if(checkbox_count == check_count)
			$('input:checkbox').attr('checked', true);
	}else{		
		$('#select_all').attr('checked', false);
		if(obj.value == '')
			$('input:checkbox').attr('checked', false);
	}
}
function ShowReport() {	
	var reportType = $('input[name=reportType]:checked', '#frmsearch').val();
	var url = "ajax_show_orders.php"; 
	
	var data = $('#frmsearch').serialize();
	
	if($('#order_status').val() == 6){
		$('#statusbtn').hide();
	}
	else
		$('#statusbtn').show();
	
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: data,
		async: false
	}).done(function (response) {		 
		$('#order_list').html(response);
		var table = $('#sample_2').dataTable();      
		table.fnFilter('');
		$("#select_th").removeAttr("class");
	}).fail(function () { });
	return false;
}
function showInvoice(order_status, id) {	
	var url = "invoice.php"; 
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_status='+order_status+'&order_id='+id,
		async: false
	}).done(function (response) {
		$('#view_invoice_content').html(response);
		$('#view_invoice').modal('show');
	}).fail(function () { });
	return false;
}
function showOrderDetails(id,order_type) {	
	var url = "order_details_popup.php"; 
    jQuery.ajax({
		url: url,
		method: 'POST',
		data: 'order_details_id='+id+'&order_type='+order_type,
		async: false
	}).done(function (response) {
		$('#order_details_content').html(response);
		$('#order_details').modal('show');
	}).fail(function () { });
	return false;
}
function setPayment() {	
	var check_count = ($('input:checkbox:checked').length);
	if(check_count == 0){
		alert('Please select order for process');
		return false;
	}else{
		var url = "ajax_set_payment.php"; 
		var data = $('#form_list').serialize();
		jQuery.ajax({
			url: url,
			method: 'POST',
			data: data,
			async: false
		}).done(function (response) {
			$('#set_payment_content').html(response);
			$('#set_payment').modal('show');
		}).fail(function () { });
		return false;
	}
}
function takeprint() {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\body {\
		font-size: 12px;}\
	th {text-align: left;}\
	.printHeading { line-height: 18px;  padding: 10px 0;  font-size: 18px; }\
	table { border-collapse: collapse;  \
		font-size: 12px; }\
	table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
	body { font-family: "Open Sans", sans-serif;\
	background-color:#fff;\
	font-size: 15px;\
	direction: ltr;}</style>' + $("#divPrintArea").html();
	if(isIE == true){
		var printWindow = window.open('', '', 'height=400,width=800');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
}
function takeprint_invoice(site_url) {
	var isIE = !!navigator.userAgent.match(/Trident/g) || !!navigator.userAgent.match(/MSIE/g);
	var divContents = '<style>\
	 table { border-collapse: collapse; font-size: 12px; }\
	table, th, td { padding: 5px; font-size: 15px; line-height: 20px; border: 1px solid black; }\
	.text-left {  text-align: left;}\
	.text-right {  text-align: right;}\
	.text-center {  text-align: center;}\
	.darkgreen{	background-color:#364622 !important; color:#fff!important; font-size:24px; font-weight:600;}\
	.fentgreen1{background-color:#b0b29c; color:#4a5036;	font-size:12px;}\
	.fentgreen{	background-color:#b0b29c;	color:#4a5036;}\
	.font-big{	font-size:20px;	font-weight:600;	color:#364622;}\
	.font-big1{	font-size:18px;	font-weight:600;	color:#364622;}\
	.table-bordered-popup {    border: 1px solid #364622;}\
	.table-bordered-popup > tbody > tr > td, .table-bordered-popup > tbody > tr > th, .table-bordered-popup > thead > tr > td, .table-bordered-popup > thead > tr > th {\
		border: 1px solid #364622;	color:#4a5036;}\
	.blue{	color:#010057;}\
	.blue1{	color:#574960;	font-size:16px;}\
	.buyer_section{	color:#574960;	font-size:14px;}\
	.pad-5{padding-left:10px;}\
	.pad-40{padding-left:40px;}\
	.np{padding-left:0px;	padding-right:0px;}\
	.bg{background-image: url("'+site_url+'/assets/global/img/watermark.png"); background-repeat:no-repeat; background-size: 200px 200px;}\
	\
	</style>' + $("#divPrintArea").html();//alert(divContents);return false;
	if(isIE == true){
		var printWindow = window.open('', '', 'height=600,width=900');
		printWindow.document.write(divContents);
		printWindow.focus();
		printWindow.document.execCommand("print", false, null);
	}else{
		$('<iframe>', {
			name: 'myiframe',
			class: 'printFrame'
		}).appendTo('body').contents().find('body').html(divContents);
		window.frames['myiframe'].focus();
		window.frames['myiframe'].print();
		setTimeout(
		function() 
		{
			$(".printFrame").remove();
		}, 1000);
	}
}
var lat=0;
var lng=0;

$('#googleMapPopup').on('shown.bs.modal', function (e) {
  
    var latlng = new google.maps.LatLng(lat,lng);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: latlng,
      zoom: 17
    });
    var marker = new google.maps.Marker({
      map: map,
      position: latlng,
      draggable: false,
      //anchorPoint: new google.maps.Point(0, -29)
   });
});
function showGoogleMap(getlat,getlng) {
   
  lat = getlat; 
  lng = getlng;
  $('#googleMapPopup').modal('show');
}
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
</html>