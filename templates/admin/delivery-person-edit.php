<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";
$userObj 	= 	new userManager($con,$conmain);

?>
<!-- END HEADER -->
<?php

if(isset($_POST['hidbtnsubmit']))
{
	$id=$_POST['id'];
	$user_type="DeliveryPerson";	
	$userObj->updateLocalUserDetails($user_type, $id);
	
	$working_detail = $userObj->getLocalUserWorkingAreaDetails($id);
	if($working_detail == 0)
		$userObj->addLocalUserWorkingAreaDetails($id);
	else
		$userObj->updateLocalUserWorkingAreaDetails($id);	
	
	$userObj->updateCommonUserDetails($id);
	$userObj->updateLocalUserOtherDetails($id);
	echo '<script>location.href="delivery-persons.php";</script>';
}
?>
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "ManageSupplyChain"; $activeMenu = "DeliveryPerson";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- /.modal -->
			<h3 class="page-title">Delivery Person</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li>
						<i class="fa fa-home"></i>
						<a href="delivery-persons.php">Delivery Person</a>
                        <i class="fa fa-angle-right"></i>
					</li>
                    <li>
						<a href="#">Edit Delivery Person</a> 
					</li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet box blue-steel">
						<div class="portlet-title">
							<div class="caption">
								Edit Delivery Person
							</div>
							 <a href="updatepassword.php?id=<?php echo base64_encode($_GET['id']);?>" class="btn btn-sm btn-default pull-right mt5">							
                                Change Password
                              </a>
						</div>
						<div class="portlet-body">						
						<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
					
					    <?php
						$id			= $_GET['id'];
						$userObj 	= 	new userManager($con,$conmain);
						$user_details = $userObj->getLocalUserDetails($id);
						$user_other_details = $userObj->getLocalUserOtherDetails($id);
						$working_area_details = $userObj->getLocalUserWorkingAreaDetails($id);
						if($working_area_details != 0)
						{
							$row1 = array_merge($user_details,$working_area_details);//print"<pre>";print_r($row1);
							if($row1['state_ids'] == '')
								$row1['state_ids'] = $user_details['state'];
							if($row1['city_ids'] == '')
								$row1['city_ids'] = $user_details['city'];
						}
						else{
							$row1 = $user_details;
							$row1['state_ids'] = $user_details['state'];
							$row1['city_ids'] = $user_details['city'];
						}											
					
						?>                       

					<form  name="updateform" id="updateform" class="form-horizontal" role="form" data-parsley-validate="" action="" method="post">
						
						<?php $page_to_update = 'DeliveryPerson'; include "userUpdateCommEle.php";	//form common element file with javascript validation ?>    
						
						<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name <span class="mandatory">*</span></label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"				
								data-parsley-required="#true" 
								data-parsley-required-message="Please enter accouont number"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accname" 
								value="<?=$user_other_details['accname'];?>" 
								class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter accouont number"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"						
							name="accno" class="form-control" value="<?=$user_other_details['accno'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter Bank Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="bank_name" class="form-control" value="<?=$user_other_details['bank_name'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter branch Name"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accbrnm" class="form-control" value="<?=$user_other_details['accbrnm'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter ifsc Code"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="accifsc" class="form-control" value="<?=$user_other_details['accifsc'];?>">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">GSTIN:<span class="mandatory">*</span></label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter GSTIN"
							data-parsley-trigger="change"				
							data-parsley-required="#true" 
							data-parsley-required-message="Please enter GSTIN"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"							
							name="gst_number_sss" class="form-control" value="<?=$user_details['gst_number_sss'];?>">
						  </div>
						</div>
						<!--<div class="form-group">
							<label class="col-md-3">Status</label>
							<div class="col-md-4">                                                   
								<div class="radio-list">
								<label class="radio-inline">
								<input type="radio" name="optionsRadios" id="optionsRadios4" value="option1"> Active </label>
								<label class="radio-inline">
								<input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Inactive </label>
								</div>
							</div>
						</div>-->
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
								<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
								<input type="hidden" name="hidAction" id="hidAction" value="delivery-person-edit.php">
								<input type="hidden" name="id" id="id" value="<?=$row1['id'];?>">
								
								<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
								<a href="delivery-persons.php" class="btn btn-primary">Cancel</a>
							</div>
						</div><!-- /.form-group -->
					</form>  
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<!-- END PAGE LEVEL SCRIPTS -->

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>