<? 
include ("../../includes/config.php");
include "../includes/common.php";
include "../includes/orderManage.php";
$orderObj 	= 	new orderManage($con,$conmain);
$order_details_id = $_POST['order_details_id'];
$order_type = $_POST['order_type'];
$order_details = $orderObj->getOrderDetailsById($order_details_id);
//print"<pre>";print_R($order_details);
$product_variant = $orderObj->getSProductVariant($order_details['product_variant_id']);
?>
<div class="modal-header">
<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
<? if($order_details['lat']!="" && $order_details['long']!="" && $order_details['lat']!="0.0" && $order_details['long']!="0.0") { ?>
&nbsp;&nbsp;
<button type="button" name="btnPrint" id="btnPrint" onclick="showGoogleMap('<?=$order_details['lat'];?>','<?=$order_details['long'];?>')" class="btn btn-primary" style="margin-top: 3px; margin-right: 5px;">Location</button>
<? } ?>

<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<h4 class="modal-title" id="myModalLabel"></h4>	   
</div>
<div class="modal-body" style="padding-bottom: 5px !important;" id="divPrintArea">
<div class="row">
<div class="col-md-12">   
	<div class="portlet box blue-steel">
		<div class="portlet-title ">
			<div class="caption printHeading">
				<?=$order_type;?>
			</div>                          
		</div>
		<div class="portlet-body">
			<table class="table table-striped table-bordered table-hover" id="sample_2" width="100%">
			<tr>
				<td>Order Received By</td>
				<td><?=$order_details['order_by_name'];?></td>				
			</tr>
			<tr>
				<td>Shop Name</td>
				<td><?=$order_details['shop_name'];?></td>				
			</tr>
			<tr>
				<td>Order Id</td>
				<td><?=$order_details['order_no'];?></td>				
			</tr>
			<tr>
				<td>Order Date</td>
				<td><?=fnSiteDateTimeFormat($order_details['order_date']);?></td>				
			</tr>
			<tr>
				<td>Product</td>
				<td><?=$order_details['product_name'].' '.$product_variant;?></td>				
			</tr>
			<tr>
				<td>Quantity</td>
				<td><?=$order_details['product_quantity'];?></td>				
			</tr>
			<tr>
				<td>Unit Price (₹)</td>
				<td><?=$order_details['product_unit_cost'];?></td>				
			</tr>
			<? if($order_details['campaign_applied'] == 0 && $order_details['discounted_totalcost'] != 0 && $order_details['discounted_totalcost'] != ''){ ?>
			<tr>
				<td>After Discount By Weight Unit Price(₹)</td>
				<td>
				<? 
				if($order_details['product_quantity']!=0){
					$discounted_perunitcost = $order_details['discounted_totalcost']/$order_details['product_quantity'];
					echo $discounted_perunitcost;
				}
				?></td>				
			</tr>
			<? } ?>
			<? if($order_details['campaign_applied'] == 0 && $order_details['discount_amount']!=0 && $order_details['discount_amount']!=''){ ?>
			<tr>
				<td>Discount (₹)</td>
				<td>
				<? 
				$discounted_amt = ($order_details['discount_amount'] * $order_details['product_quantity']);
				echo $discounted_amt;
				?></td>				
			</tr>
			<? } ?>
				
			<tr>
				<td>Total Price (₹)</td>
				<td><?=$order_details['product_total_cost'];?></td>				
			</tr>
			<? if($order_details['campaign_applied'] == 0 && $order_details['discounted_totalcost'] != 0 && $order_details['discounted_totalcost'] != ''){ ?>
			<tr>
				<td>After Discount By Weight Total Price(₹)</td>
				<td>
				<? 
				$discounted_totalcost = $order_details['discounted_totalcost'];
				echo $discounted_totalcost;
				?></td>				
			</tr>
			<? } ?>
			<tr>
				<td><b>GST Price(SGST/CGST) (₹)</b></td>
				<td><b><?=$order_details['p_cost_cgst_sgst'];?></b></td>				
			</tr>
			<?php if($order_details['order_status'] == 2){ ?>
			<tr>
				<td>Assigned for Delivery on</td>
				<td><?=fnSiteDateFormat($order_details['delivery_assing_date']);?></td>				
			</tr>
			<? } ?>
			<?php if($order_details['order_status'] == 3){ ?>
			<tr>
				<td>Assigned for Delivery on</td>
				<td><?=fnSiteDateFormat($order_details['delivery_assing_date']);?></td>				
			</tr>
			<tr>
				<td>Assigned to Transport Office on</td>
				<td><?=fnSiteDateFormat($order_details['transport_date']);?></td>				
			</tr>
			<? } ?>
			<?php if($order_details['order_status'] == 4){ ?>
			<tr>
				<td>Assigned for Delivery on</td>
				<td><?=fnSiteDateFormat($order_details['delivery_assing_date']);?></td>				
			</tr>
			<tr>
				<td>Assigned to Transport Office on</td>
				<td><?=fnSiteDateFormat($order_details['transport_date']);?></td>				
			</tr>
			<tr>
				<td>Delivered on</td>
				<td><?=fnSiteDateFormat($order_details['delivery_date']);?></td>				
			</tr>
			<? } ?>
			<?php if($order_details['order_status'] == 6 || $order_details['order_status'] == 8){ ?>
			<tr>
				<td>Assigned for Delivery on</td>
				<td><?=fnSiteDateFormat($order_details['delivery_assing_date']);?></td>				
			</tr>
			<tr>
				<td>Assigned to Transport Office on</td>
				<td><?=fnSiteDateFormat($order_details['transport_date']);?></td>				
			</tr>
			<tr>
				<td>Delivered on</td>
				<td><?=fnSiteDateFormat($order_details['delivery_date']);?></td>				
			</tr>
			<tr>
				<td>Payment Received (₹)</td>
				<td><?=$order_details['amount_paid'];?></td>				
			</tr>
			<tr>
				<td>Payment Date</td>
				<td><?=fnSiteDateFormat($order_details['payment_date']);?></td>				
			</tr>
			<?php } ?>
			</table>
</div>
</div>
</div>
</div>
</div>