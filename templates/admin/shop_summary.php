<!-- BEGIN HEADER -->
<?php header("Cache-Control: no-store, must-revalidate, max-age=0");
header("Pragma: no-cache");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
include "../includes/grid_header.php";
include "../includes/shopManage.php";
include "../includes/userManage.php";
$shopObj 	= 	new shopManager($con,$conmain);
$userObj 	= 	new userManager($con,$conmain);
//echo "<pre>";print_r($_SESSION);
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix"></div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
	<!-- BEGIN SIDEBAR -->
	<?php
	$activeMainMenu = "Reports"; $activeMenu = "ShopReport";
	include "../includes/sidebar.php";
	?>
	<!-- END SIDEBAR -->
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
		
			<h3 class="page-title">Shops</h3>
            <div class="page-bar">
				<ul class="page-breadcrumb">					
					<li><i class="fa fa-home"></i>
					<a href="#">Shops</a></li>
				</ul>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				
					<div class="portlet box blue-steel">
						<div class="portlet-title">
						
							<div class="caption">Shop Listing</div>
							
							
							
                            <div class="clearfix"></div>
						</div>
						<div class="portlet-body">
							<form class="form-horizontal" id="frmsearch" enctype="multipart/form-data" method="post" action="">
							 
								 
								<div class="form-group">
									<label class="col-md-3">State:</label>
									<div class="col-md-4">
									<select name="dropdownState" id="dropdownState" class="form-control" onchange="fnShowCity(this.value)">
										<option value="">-Select-</option>
										<option value="1">Andaman and Nicobar Islands</option><option value="2">Andhra Pradesh</option><option value="3">Arunachal Pradesh</option><option value="4">Assam</option><option value="5">Bihar</option><option value="6">Chandigarh</option><option value="7">Chhattisgarh</option><option value="8">Dadra and Nagar Haveli</option><option value="9">Daman and Diu</option><option value="10">Delhi</option><option value="11">Goa</option><option value="12">Gujarat</option><option value="13">Haryana</option><option value="14">Himachal Pradesh</option><option value="15">Jammu and Kashmir</option><option value="16">Jharkhand</option><option value="17">Karnataka</option><option value="18">Kenmore</option><option value="19">Kerala</option><option value="20">Lakshadweep</option><option value="21">Madhya Pradesh</option><option value="22">Maharashtra</option><option value="23">Manipur</option><option value="24">Meghalaya</option><option value="25">Mizoram</option><option value="26">Nagaland</option><option value="27">Narora</option><option value="28">Natwar</option><option value="29">Odisha</option><option value="30">Paschim Medinipur</option><option value="31">Pondicherry</option><option value="32">Punjab</option><option value="33">Rajasthan</option><option value="34">Sikkim</option><option value="35">Tamil Nadu</option><option value="36">Telangana</option><option value="37">Tripura</option><option value="38">Uttar Pradesh</option><option value="39">Uttarakhand</option><option value="41">West Bengal</option>									</select>
									</div>
								</div>	
								
								<div class="form-group" id="city_div" style="display:none;">
								  <label class="col-md-3">City:</label>
								  <div class="col-md-4" id="div_select_city">
								  <select name="city" id="city" data-parsley-trigger="change" class="form-control" >
									<option  value="">-Select-</option>										
									</select>
								  </div>
								</div><!-- /.form-group -->
								<div class="form-group" id="area_div" style="display:none;">
								  <label class="col-md-3">Taluka:</label>
								  <div class="col-md-4" id="div_select_area">
								  <select name="area" id="area"
								  data-parsley-trigger="change"
								  class="form-control">
									<option  value="">-Select-</option>									
									</select>
								  </div>
								</div><!-- /.form-group --> 						
								<div class="form-group" id="subarea_div" style="display:none;">
								  <label class="col-md-3">Subarea:</label>
								  <div class="col-md-4" id="div_select_subarea">
								  <select name="subarea" id="subarea" data-parsley-trigger="change" class="form-control">
									<option  value="">-Select-</option>									
									</select>
								  </div>
								</div><!-- /.form-group --> 
								
								
								<div class="form-group">
									<div class="col-md-4 col-md-offset-3">
										<button type="button" name="btnsearch" id="btnsearch" onclick="javascript: return getordersearchdata()" class="btn btn-primary">Search</button>
										
										<button type="reset" name="btnreset" id="btnreset" class="btn btn-primary">Reset</button>
									</div>
								</div><!-- /.form-group -->
							
							</form>
							<div class="clearfix"></div>
							<table class="table table-striped table-bordered table-hover" id="sample_2">
								<thead>
									<tr>
										<th>
											 Shop Name
										</th>
										<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Superstockist'){ ?>
										<th>
											 Super Stockist
										</th>
										<?php } ?>
										<?php if($_SESSION[SESSION_PREFIX.'user_type'] != 'Distributor'){ ?>
										<th>
											 Stockist
										</th>
										<?php } ?>
										<th>
											 Contact Person
										</th>
										<th>
											 Mobile Number
										</th>
										<th>
											City
										</th>
										<th>
											Area
										</th>
									</tr>
								</thead>
							<tbody>
							</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
	<!-- END CONTENT -->
	<!-- BEGIN QUICK SIDEBAR -->
	
	<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->

<?php include "../includes/grid_footer.php"?>
<!-- END FOOTER -->
<script language="JavaScript">
//new shop icon code
 /*
 $(document).ready(function(){
	 var userid=<?= $_SESSION[SESSION_PREFIX."user_id"]; ?>;
	
  window.onbeforeunload = function() {
	 
		$.ajax({
		url  : 'newshopicon.php',
		data : {userid1:userid},
		type : 'POST' ,
		success : function( output ) {
					console.log(output);
				  }
		});
   };
});*/
</script>
<script>  

function fnShowCity(id_value) {
	$("#city_div").show();	
	var url = "getCityDropDown.php?cat_id="+id_value+"&select_name_id=city";		
	CallAJAX(url,"div_select_city");	
}

function FnGetSuburbDropDown(id) {
	$("#area_div").show();	
	$("#subarea").html('<option value="">-Select-</option>');		
	var url = "getSuburDropdown.php?cityId="+id.value+"&select_name_id=area&function_name=FnGetSubareaDropDown";
	CallAJAX(url,"div_select_area");
	
}
function FnGetSubareaDropDown(id) {	
	var suburb_str = $("#area").val();
	$("#subarea_div").show();	
	var url = "getSubareaDropdown.php?area_id="+id.value+"&select_name_id=subarea";
	CallAJAX(url,"div_select_subarea");
}
</script>

<script>
$(function() {
		getordersearchdata();
	});
	function getordersearchdata() {
		if($('#dropdownState').val()!=null){var state=$('#dropdownState').val();}else{var state='';}
		
		if($('#city').val()!=null){var city=$('#city').val();}else{var city='';}
		if($('#area').val()!=null){var area=$('#area').val();}else{var area='';}
		if($('#subarea').val()!=null){var subarea=$('#subarea').val();}else{var subarea='';}
		
			var url = 'fetch_shops_reports.php?state='+state+'&city='+city+'&area='+area+'&subarea='+subarea;
			
			 $.ajax({
			 url: url,
			 datatype:"JSON",
			contentType: "application/json",
			   
			error : function(data){console.log("error:" + data)
			},
			success: function(data){
				 var mytable = $('#sample_2').DataTable();
				 
				var resdata=JSON.parse(data);
				if(resdata!=''){
					mytable.clear().draw();
					 for (var arrayIndex in resdata) {
						for (var arrayIndex1 in resdata[arrayIndex]) {
							if(resdata[arrayIndex][arrayIndex1]===undefined || resdata[arrayIndex][arrayIndex1]===null)
							{
								resdata[arrayIndex][arrayIndex1]='';
							}
						}
					}
					for (var i = 0, len = resdata.length; i < len; i++){					
						 mytable.row.add([""+resdata[i]['name']+"", ""+resdata[i]['ssfirstname']+""
						, ""+resdata[i]['sfirstname']+"",""+resdata[i]['contact_person']+"", 
						""+resdata[i]['mobile']+"",""+resdata[i]['ctname']+"",
						""+resdata[i]['arname']+""]).draw(); 
					} 
				}else{
					  mytable.clear().draw();
				}
				  
			}
		});
	}
	
</script>	

</body>
<!-- END BODY -->
</html>