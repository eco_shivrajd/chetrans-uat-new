<!-- BEGIN HEADER -->
<?php include "../includes/header.php";
include "../includes/userManage.php";	
$userObj 	= 	new userManager($con,$conmain);
if(isset($_POST['hidbtnsubmit']))
{	
	//print"<pre>";print_r($_POST);
	$user_type="SalesPerson";	
	/* Local user section */
	$userid_local = $userObj->addLocalUserDetails($user_type);
	$userObj->addLocalUserWorkingAreaDetails($userid_local);
	
	/* Common user section */
	$username	=	fnEncodeString($_POST['username']);
	$common_user_record = $userObj->getCommonUserDetailsByUsername($username);	
	if($common_user_record != 0)
	{
		$common_user_company_record = $userObj->getCommonUserCompanyDetails($userid_local);
		if($common_user_company_record != 0)
		{
			$userObj-addCommonUserCompanyDetails($userid_local);
		}
	}else{
		$userid_common = $userObj->addCommonUserDetails($user_type,$userid_local);
		$userObj->addCommonUserCompanyDetails($userid_local);
	}	
	$other_detail = $userObj->getLocalUserOtherDetails($userid_local);
	if($other_detail == 0)
		$userObj->addLocalUserOtherDetails($userid_local);
	else
		$userObj->updateLocalUserOtherDetails($userid_local);
	$email		=	fnEncodeString($_POST['email']);
	if($email != '')
		$userObj->sendUserCreationEmail();
	
	echo '<script>alert("Sales Person added successfully.");location.href="sales.php";</script>';
}
?>
<!-- END HEADER -->
<body class="page-header-fixed page-quick-sidebar-over-content ">
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->
<?php
$activeMainMenu = "ManageSupplyChain"; $activeMenu = "SalesPerson";
include "../includes/sidebar.php";
?>

<!-- END SIDEBAR -->
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">	 
		<h3 class="page-title">Sales Person</h3>
		<div class="page-bar">
			<ul class="page-breadcrumb">
				 
				<li>
					<i class="fa fa-home"></i>
					<a href="sales.php">Sales Person</a>
					<i class="fa fa-angle-right"></i>
				</li>
				<li>
					<a href="#">Add New Sales Person</a>
				</li>
			</ul>
		</div>
		<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
	<div class="row">
		<div class="col-md-12">
			<!-- Begin: life time stats -->
			<div class="portlet box blue-steel">
				<div class="portlet-title"><div class="caption">Add New Sales Person</div></div>
				<div class="portlet-body">
					<span class="pull-right">Note: <span class="mandatory">*</span> Marked fields are mandatory.</span>
					<form name="addform" id="addform" class="form-horizontal" role="form" data-parsley-validate=""  method="post" action="">          
						
						<?php $page_to_add = 'sales_person';  include "userAddCommEle.php";	//form common element file with javascript validation ?>    
						<div class="form-group">
						  <label class="col-md-3">Office Phone No.:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Office Phone No."
							data-parsley-trigger="change"					
							data-parsley-minlength="10"
							data-parsley-maxlength="15"
							data-parsley-maxlength-message="Only 15 characters are allowed"
							data-parsley-pattern="^(?!\s)[0-9]*$"
							data-parsley-pattern-message="Please enter numbers only"
							name="phone_no" class="form-control">
						  </div>
						</div>
						
						<div class="form-group">
							<label class="col-md-3"><b>Bank Details</b></label>
						</div>						
						 <div class="form-group">
							<label class="col-md-3">Account Name: </label>
							<div class="col-md-4">
								<input type="text" 
								placeholder="Account Name"
								data-parsley-trigger="change"
								data-parsley-maxlength="50"
								data-parsley-maxlength-message="Only 50 characters are allowed"
								name="accname" class="form-control">
							</div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Account Number:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Account Number"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accno" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Bank Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="bank_name" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">Bank Branch Name:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter Branch Name"
							data-parsley-trigger="change"	
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accbrnm" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">IFSC Code:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter IFSC Code"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="accifsc" class="form-control">
						  </div>
						</div>
						<div class="form-group">
						  <label class="col-md-3">GSTIN:</label>
						  <div class="col-md-4">
							<input type="text"
							placeholder="Enter GSTIN"
							data-parsley-trigger="change"
							data-parsley-maxlength="30"
							data-parsley-maxlength-message="Only 30 characters are allowed"
							name="gstnumber" class="form-control">
						  </div>
						</div>
						<div class="form-group">
					  <label class="col-md-3">Status:</label>
					  <div class="col-md-4">
					  <div class="input-group">					
							<select name="user_status" id="status" class="form-control">
								<option value="Active">Active</option>
								<option value="Inactive">Inactive</option>
							</select>
						</div>
					  </div>
					</div><!-- /.form-group -->	
						<div class="form-group">
							<div class="col-md-4 col-md-offset-3">
							<input type="hidden" name="hidbtnsubmit" id="hidbtnsubmit">
							<input type="hidden" name="hidAction" id="hidAction" value="sales-add.php">
							<button type="button"  name="btnsubmit"  onclick="return checkAvailability();" class="btn btn-primary">Submit</button>
							<a href="sales.php" class="btn btn-primary">Cancel</a>
							</div>
						</div><!-- /.form-group -->
					</form>  
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
	<!-- END PAGE CONTENT-->
	</div>
</div>
<!-- END CONTENT -->
<!-- BEGIN QUICK SIDEBAR -->

<!-- END QUICK SIDEBAR -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<?php include "../includes/footer.php"?>
<!-- END FOOTER -->
<style>
.form-horizontal { font-weight:normal; }
</style>
</body>
<!-- END BODY -->
</html>
