<?php
include ("../../includes/config.php");
include "../includes/userManage.php";
include "../includes/orderManage.php";
$userObj 	= 	new userManager($con,$conmain);
$orderObj 	= 	new orderManage($con,$conmain);
extract($_POST);
$start_location11='';$start_time11='';
$end_location22='';$end_time22='';

$record_sp = $userObj->getLocalUserDetails($dropdownSalesPerson);//user details
$result_product = $orderObj->getSalesPOrdersProducts($frmdate,$dropdownSalesPerson);//get pro
$record_product_count = mysqli_num_rows($result_product);

$resultsplocation = $orderObj->getSPLocationPoints($frmdate,$dropdownSalesPerson);//get all point locations
$record_count_sp = mysqli_num_rows($resultsplocation);

$resultsp_startlocation = $orderObj->getSPStartLocationPoints($frmdate,$dropdownSalesPerson);//get start point
$record_count_sp_startlocation = mysqli_num_rows($resultsp_startlocation);



	if($record_count_sp_startlocation > 0)
	{
		while($record11 = mysqli_fetch_array($resultsp_startlocation))
		{ 
			$lattitude11 = $record11['lattitude'];
			$longitude11 = $record11['longitude'];
			$start_date_time = $record11['start_date_time'];
		}	
		$start_location11 = "<br>".$orderObj->getLocation($lattitude11,$longitude11);
		$start_time11 = "<br>(Time: ".date('H:i:s',strtotime($start_date_time)).")";
	}

$resultsp_endlocation = $orderObj->getSPEndLocationPoints($frmdate,$dropdownSalesPerson);
$record_count_sp_endlocation = mysqli_num_rows($resultsp_endlocation);
	if($record_count_sp_endlocation > 0)
	{
		while($record22 = mysqli_fetch_array($resultsp_endlocation))
		{ 
			 $lattitude22 = $record22['lattitude'];
			 $longitude22 = $record22['longitude'];
			 $end_date_time = $record22['end_date_time'];
		}	
		$end_location22 = "<br>".$orderObj->getLocation($lattitude22,$longitude22);
		$end_time22 = "<br>(Time: ".date('H:i:s',strtotime($end_date_time)).")";
	}

$colspan_plus = 0;
if($record_product_count > 7)
	$colspan_plus = $record_product_count -7;

$result = $orderObj->getSalesPOrders($frmdate,$dropdownSalesPerson);
$record_count = mysqli_num_rows($result);


/*Get Sales Person's Start Day Time & End Day Time */
$records_sp_day_time = $orderObj->getSalesPStartEndDay($frmdate,$dropdownSalesPerson);

?>
<? if($_GET["actionType"]=="excel") { ?>
<style>table { border-collapse: collapse; } 
	table, th, td {  border: 1px solid black; } 
	body { font-family: "Open Sans", sans-serif; 
	background-color:#fff;
	font-size: 11px;
	direction: ltr;}
</style>
<? } ?>
<div class="portlet box blue-steel">
	<div class="portlet-title">
		<? if($_GET["actionType"]!="excel") { ?>
		<div class="caption"><i class="icon-puzzle"></i>Daily Status Report</div>
		<?  if($record_count > 0) { ?>
			<button type="button" name="btnExcel" id="btnExcel" onclick="ExportToExcel();" class="btn btn-primary pull-right" style="margin-top: 3px; ">Export to Excel</button> &nbsp;
			&nbsp;
			<button type="button" name="btnPrint" id="btnPrint" onclick="takeprint()" class="btn btn-primary pull-right" style="margin-top: 3px; margin-right: 5px;">Take a Print</button>
		
		<? } } ?>
	</div>
	
	<div class="portlet-body">
		<div class="table-responsive" id="dvtblResonsive">
			<table class="table table-bordered" id="report_table">
				
				<thead>
					<tr>
						<th colspan="<?php echo 14+$colspan_plus;?>" style="text-align:center">
							<b>Chetran Foods Private Limited</b><br>
							<span style="font-size: 12px;">Unit 25 Chintamani Estate, Ramtekdi Industrial Area</span><br>
							<span style="font-size: 12px;">Hadapsar, Pune, 411028</span>
						</th>						
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td>RDS NAME</td>
						<td colspan="2"></td>
						<td>FIELD STAFF</td>						
						<td colspan="<?php echo 9+$colspan_plus;?>" align="left"><?=$record_sp['firstname'];?>
						<input type="hidden" name="date" id="date" value="<?=$frmdate;?>">
						<input type="hidden" name="salespname" id="salespname" value="<?=$record_sp['firstname'];?>">
						</td>						
					</tr>
					<tr>
						<td></td>
						<td valign="top">Region</td>
						<td valign="top" colspan="2" align="left"><?=$record_sp['suburbnm'];?></td>
						<td valign="top">DATE</td>						
						<td valign="top" colspan="3" align="left"><?=$frmdate;?></td>	
						<td valign="top" colspan="<?php echo 6+$colspan_plus;?>"></td>	
					</tr>
					<tr>
						<td valign="top" colspan="<?php echo 14+$colspan_plus;?>">STARTING POINT: <?=$start_location11.$start_time11;?></td>						
					</tr>
					<tr>
						<td valign="top" colspan="2">(IN WALKING ORDER) </td>	
						<td colspan="<?php echo 12+$colspan_plus;?>"> </td>
					</tr>
					<tr>
						<td colspan="14" style="height: 30px;"></td>
					</tr>
					<tr>
						<td colspan="5"></td>
						<td valign="top" colspan="<?php echo 9+$colspan_plus;?>">
 <?php 
					$i = 1;
					$countrec=0;
					if($record_count_sp > 0)
					{						
					while($record = mysqli_fetch_array($resultsplocation))
					{	
						/*echo "<pre>";
						print_r($record);*/
						$c_array_temp[$countrec]['id'] = $record['id'];
						$c_array_temp[$countrec]['lattitude'] = $record['lattitude'];
						$c_array_temp[$countrec]['longitude'] = $record['longitude'];
						$countrec++;
					}
					}
                  /* echo "<pre>";
				   print_r($c_array_temp); */
					$google_distance=0;
					for($i=0;$i<$record_count-1;$i++)
					{
					$google_d= $orderObj->getDistanceBetweenPoints($c_array_temp[$i]['lattitude'],$c_array_temp[$i+1]['lattitude'],$c_array_temp[$i]['longitude'],$c_array_temp[$i+1]['longitude']);
					//echo "<pre>";print_r($google_d);
					$google_distance=$google_distance+$google_d['distance'];
					} 
					$google_distance=bcdiv($google_distance, 1000, 3);	                     
                   ?>
					End Point: <?=$end_location22.$end_time22;?>	
				    Total Distance Travelled By Salesperson  : <?php echo $google_distance ;?> KM

          
							<!-- End Point: <?=$result_end['shopname'].$end_location.$end_time;?>
							
							Total Distance Between Two Address : <?php echo $distance_between_two_address ;?> -->
						</td>
					</tr>
					<?  if($record_count > 0) { ?>
					<tr>
						<td valign="top">S. No.</td>
						<td valign="top" style="text-align:center">OUTLET NAME</td>
						<td valign="top" style="text-align:center">TIME VISITED:  ADDRESS</td>
						<td valign="top" style="text-align:center">PUNCHED LOCATION</td>
						<td valign="top" style="text-align:center">CONTACT NO. OF SHOP</td>	
						<?php 
						$product_array = array();
						$product_cost = array();
						$product_qnty=array();
						$i = 1;
						while($record = mysqli_fetch_array($result_product)){ 
						$product_array[$i] = $record['product_variant_id'];
						
						$dimension = '';
						$dimension = $orderObj->getSProductVariant($record['product_variant_id']);
						if($dimension != '')
							$dimension = " (".$dimension.")";
						?>						
						<td valign="top"><?=$record['brandnm'].">".$record['categorynm'].">".$record['productname'].$dimension."<br>(quantity/₹)";?></td>							
						<?php $i++;} 
						$pro_count = count($product_array);
						if($record_product_count < 6){
							$td_count = 6-$record_product_count;
							for($k=1;$k<=$td_count; $k++){
								echo "<td></td>";
							}
						}
						?>
						<td valign="top" style="text-align:center">Total ₹</td>	
						<td valign="top" style="text-align:center">REMARKS</td>	
					</tr>
					<?php 
					$i = 1;
					$shop_total = 0;
					$totalcost = 0;
					if($record_count > 0){		
						$supertotal=0;$qntytotal=0;
					while($record = mysqli_fetch_array($result)){ 
                       /* echo "<pre>";
						print_r($record);*/
						$shop_total = 0;$shop_qntytotal = 0;
						$order_time = " (Time: ".date('H:i:s',strtotime($record['date'])).")";
						
						$order_location = "";
						$order_location = $orderObj->getLocation($record['oplacelat'],$record['oplacelon']);

					?>
					<tr>
						<td valign="top"><?=$i;?></td>
						<td valign="top"><?=$record['shopname'];?></td>
						<td valign="top" ><?=$order_time."<br>".$record['address']."<br>".$record['cityname']."<br>".$record['statename'];?></td>
						<td valign="top"><?=$order_location;?></td>		
						<td valign="top"><?=($record['mobile'] == 0) ? '-': $record['mobile'];?></td>						
						<?php 
						
						for($j=1; $j<=$pro_count; $j++){ 
							$quantity_row = array();
							$display_q_c = "";
							if($record['shop_id'] != ''){
								$quantity_row = $orderObj->getSalesPOrdersPQuantity($record['shop_id'],$product_array[$j],$record['order_date']);
								
								$cost = '';
								$totalcost = 0 ;$total_qnty=0;
								if($quantity_row['variantunit'] !=''){
									/*$quantity_row['totalcost'] = ($quantity_row['totalcost'] * $quantity_row['variantunit']);*/
									$cost = "/ ".number_format($quantity_row['totalcost'],2, '.', '');
									$shop_total = $shop_total + $quantity_row['totalcost'];
									$shop_qntytotal=$shop_qntytotal+$quantity_row['variantunit'];
									$total_qnty=$total_qnty+$quantity_row['variantunit'];
									$qntytotal=$qntytotal+$quantity_row['variantunit'];
									$totalcost = $quantity_row['totalcost'];
								}
								$product_cost[$i][$j] = $product_cost[$i-1][$j] + $totalcost;
								$product_qnty[$i][$j] = $product_qnty[$i-1][$j] + $total_qnty;
								$display_q_c = $quantity_row['variantunit'].$cost;
							}else{
								$display_q_c = '-';
							}
						?>						
						<td valign="top" align="right"><?=$display_q_c;?></td>							
						<?php 

					} 					
						if($record_product_count < 6){
							$td_count = 6-$record_product_count;
							for($k=1;$k<=$td_count; $k++){
								echo "<td></td>";
							}
						}
						?>
						<td><?=$shop_qntytotal."/".number_format($shop_total,2, '.', '');?></td>
						<td valign="top">
						<?php $remark = '';
						if($record['shop_visit_reason'] != '')
							$remark.= $record['shop_visit_reason']."<br>";
						if($record['shop_close_reason_type'] != '')
							$remark.= $record['shop_close_reason_type']."<br>";
						if($record['shop_close_reason_details'] != '')
							$remark.= $record['shop_close_reason_details']."<br>";
						
						echo $remark;
						?>
						</td>
					</tr>
					<?php $i++;$supertotal=$supertotal+number_format($shop_total,2, '.', '');
						
						}  ?>	
					<tr>
						<td valign="top" colspan="5" style="text-align:right;">Total ₹</td>											
						<?php 
						$total_count = count($product_cost);
						for($j=1; $j<=$pro_count; $j++){ 
						//	print"<pre>";print_r($product_cost);						
						?>						
						<td valign="top" align="right"><?=$product_qnty[$total_count][$j]."/".number_format($product_cost[$total_count][$j],2, '.', '');?></td>							
						<?php } 
						if($record_product_count < 6){
							$td_count = 6-$record_product_count;
							for($k=1;$k<=$td_count; $k++){
								echo "<td></td>";
							}
						}
						?>
						<td><?php echo $qntytotal."/".$supertotal;?></td>
						<td></td>
					</tr>
				<?php } ?>
					<!-- <tr>
					   <td colspan="<?php echo 14+$colspan_plus;?>" style="height: 30px;">
						<b>Total Distance Between Two Address : <?php echo $distance_between_two_address ;?> </b>

						</td>
					</tr>
					<tr>
						<td valign="top" colspan="<?php echo 14+$colspan_plus;?>">SM:              DS:                GM:                   CD:                    ORG:                 POOJA:                     PS:                      MED:                     MOD:          </td>
					</tr>
					<tr>
						<td valign="top" style="text-align:center">AVL O/L</td>
						<td valign="top" style="text-align:center">SERVICE O/L</td>
						<td valign="top" style="text-align:center">ARUDRAZ O/L</td>
						<td valign="top" style="text-align:center">NON SERVICE O/L</td>						
						<td valign="top" style="text-align:center">NEW PLACE O/L</td>	
						<td valign="top" colspan="<?php echo 8+$colspan_plus;?>" style="text-align:center">FOCUS PRODUCT</td>	
						<td></td>							
					</tr>					
					<tr>
						<td colspan="5"></td>	
						<td valign="top">KDS</td>	
						<td valign="top">AKB</td>	
						<td valign="top">9 FLVS</td>	
						<td valign="top">10 RS</td>	
						<td valign="top">20 RS</td>	
						<td valign="top">JAR</td>	
						<td></td>	
						<td colspan="<?php echo $colspan_plus+1;?>"></td>							
					</tr>
					<tr>
						<td colspan="5" style="height: 30px;"></td>	
						<td></td>	
						<td></td>	
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>	
						<td colspan="<?php echo $colspan_plus+1;?>"></td>							
					</tr> -->
				</tbody>
					<?php }else{
					echo "<tr ><td align='center' colspan='14'>No Record available.</td></tr>";
				} ?>
			</table>
		</div>
	</div>
</div> 